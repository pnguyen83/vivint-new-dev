public interface skedIJobManager {

    /**
    * Register a job
    */
    void register(String action, Object param);

    /**
    * Handle job done
    */
    void startJob(skedJob job);

    /**
    * Pre-processing job
    */
    void preExec(skedJob job);

    /**
    * Execute Job
    */
    void executeJob(skedJob job);

    /**
    * Post-processing job
    */
    void postExec(skedJob job);

    /**
    * Handle Batchable.start()
    */
    List<sObject> startBatch(skedJob job);

    /**
    * Handle Batchable.start()
    */
    void executeBatch(skedJob job);

    /**
    * Handle Batchable.finish()
    */
    void finishBatch(skedJob job);
}
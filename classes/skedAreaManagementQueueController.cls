public class skedAreaManagementQueueController {
	@remoteaction
    public static skedRemoteResultModel getAllJobs(skedVivintModel.AreaManagementFilter filter) {
        return skedAreaManagementQueueHandler.getJobsInRegion(filter);
    }

    @remoteaction
    public static skedRemoteResultModel getConfigData() {
        return skedAreaManagementQueueHandler.getConfigData();
    }

    @remoteaction
    public static skedRemoteResultModel cancelJob(skedVivintModel.VivintJob job) {
        return skedAreaManagementQueueHandler.cancelJob(job);
    }

    @remoteaction
    public static skedRemoteResultModel reassignJob(skedVivintModel.JobAvaiModel filter) {
       return skedAreaManagementQueueHandler.getResourcesToReassign(filter);
    }

    @remoteaction
    public static skedRemoteResultModel confirmAssignment(skedVivintModel.Assignment data) {
        return skedAreaManagementQueueHandler.confirmResourceAssignment(data);
    }

    @remoteaction
    public static skedRemoteResultModel getAllResources(skedVivintModel.AreaManagementFilter filter) {
       return  skedAreaManagementQueueHandler.getAllResourcesInRegions(filter);
    }

    @remoteaction
    public static skedRemoteResultModel getResourceAndJobsInRegions(skedVivintModel.AreaManagementFilter filter) {
        return  skedAreaManagementQueueHandler.getResAndJobInRegions(filter);
    }

    @remoteaction
    public static skedRemoteResultModel getResourcesForReschedule (skedVivintModel.AreaManagementFilter filter) {
        return skedAreaManagementQueueHandler.getResourceForReschedule(filter);
    }

    @remoteaction
    public static skedRemoteResultModel rescheduleJob(skedVivintModel.RescheduleJob data) {
        return skedAreaManagementQueueHandler.rescheduleJob(data);
    }

    @remoteaction
    public static skedRemoteResultModel confirmPendingDispatchJob(skedVivintModel.Appointment data) {
        return skedCloserHandler.confirmAppointment(data);
    }

    @remoteaction
    public static skedRemoteResultModel updateAttemptedJob(skedVivintModel.VivintJob job) {
        return skedCloserHandler.updateContactAttempTimeAndStatusOnJob(job);
    }

    @remoteaction
    public static skedRemoteResultModel getUnAllocatedJobs(skedVivintModel.AreaManagementFilter filter) {
        return skedAreaManagementQueueHandler.getUnAllocatedJobsForSchedule(filter);
    }
}
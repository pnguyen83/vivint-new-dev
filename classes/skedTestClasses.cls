@isTest
public with sharing class skedTestClasses {
	public static testmethod void availabilityHandlerTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Availability__c avai = (sked__Availability__c)objFactory.getSObjects(sked__Availability__c.sObjectType).get(0);
        avai.sked__Status__c = skedConstants.AVAILABILITY_STATUS_PENDING;
        avai.sked__Is_Available__c = false;
        update avai;
        avai.sked__Is_Available__c = true;
        update avai;
        test.stopTest();
    }

    public static testmethod void commonServiceTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Holiday__c globalHoliday = (sked__Holiday__c)objFactory.getSObjects(sked__Holiday__c.sObjectType).get(0);

        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__Holiday_Region__c regionHoliday = new sked__Holiday_Region__c(
            sked__Holiday__c = globalHoliday.id,
            sked__Region__c = region.id
        );
        insert regionHoliday;
        skedCommonService.getHolidays();
        skedCommonService.getMapHolidays();
        test.stopTest();
    }

    private static testmethod void jobAllocationHandlerTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        User u = skedSetterControllerTest.createUser();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);

        sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = res.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;

        sked__Job_Allocation__c ja = new sked__Job_Allocation__c(
            sked__Job__c = job.id,
            sked__Resource__c = resource2.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
        );
        insert ja;
        test.stopTest();
    }

    private static testmethod void jobAllocationHandlerTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        User u = skedSetterControllerTest.createUser();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = res.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;

        sked__Job_Allocation__c ja = new sked__Job_Allocation__c(
            sked__Job__c = job.id,
            sked__Resource__c = resource2.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
        );
        insert ja;
        test.stopTest();
    }

    public static testmethod void smsServiceTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
        Set<Id> setJaIds = new Set<Id>{ja.id};
        skedSmsServices.sendSMSToResourceFromJA(setJaIds);
        skedSMS abc = new skedSMS();
        skedSmsServices.sendSMS('job ');
        test.stopTest();
    }

    public static testmethod void ondayRemindScheduleTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        job.sked__Start__c = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0));
        job.sked__Finish__c = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0)).addHours(3);
        job.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;
        update job;

        sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
        ja.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED;
        update ja;
        skedOneDayRemindSchedule oneday = new skedOneDayRemindSchedule();
        oneday.execute(null);
        test.stopTest();
    }

    public static testmethod void setterLocationHandlerTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        location.sked_Region__c = null;
        update location;
        location.sked_Region__c = region.id;
        update location;
        delete location;
        test.stopTest();
    }

    public static testmethod void skedModelsTest() {
        skedModels.activity act = new skedModels.activity();
        act.resourceId = 'yyyy-MM-dd';
        act.notes = 'notes';
        act.scheduleId = 'scheduleId';

        skedModels.availability avai = new skedModels.availability();
        avai.resourceId = 'resourceId';
        avai.notes = 'notes';
        avai.status = 'status';
        avai.scheduleId = 'scheduleId';
        avai.isAllDay = true;

        skedModels.templateEntry entry = new skedModels.templateEntry();
        entry.weekNo  = 1;
        entry.weekday = 'weekday';
        entry.startTime = 800;
        entry.endTime = 1500;
        entry.action = 'action';

        skedModels.template template = new skedModels.template();
        template.startDate = 'startDate';
        template.endDate = 'endDate';
        template.entries = new List<skedModels.templateEntry>();

        skedModels.address add = new skedModels.address();
        add.fullAddress = 'fullAddress';
        add.postalCode = 'postalCode';

        skedModels.job job = new skedModels.job();
        job.accountId = 'accountId';
        job.cancellationReason = 'cancellationReason';
        job.cancellationReasonNotes = 'jobcancellationReasonNotes';
        job.contactId = 'contactId';
        job.contactName = 'contactName';
        job.description = 'description';
        job.jobAllocationCount = 1;
        job.jobStatus = 'jobStatus';
        job.notes = 'notes';
        job.quantity = 1;
        job.rescheduleJobId = 'rescheduleJobId';

        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
        job.loadJobAllocations(new List<sked__Job_Allocation__c>{ja});

        skedModels.jobAllocation joa = new skedModels.jobAllocation();
        joa.resourceId = 'rescheduleJobId';
        joa.resourceName = 'rescheduleJobId';
        joa.travelTimeFrom = 1;
        joa.travelDistanceFrom = 1;
        joa.travelTimeTo = 1;
        joa.travelDistanceTo = 1;
        joa.isQualified = true;

        skedException ex = new skedException('rescheduleJobId');
    }

    public static testmethod void testData() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        Datetime currentDate = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0));
        string DateValue = currentDate.format('yyyy-MM-dd');
        System.debug('11111111 DateValue  ' + DateValue);
        string query = 'select id from sked__Job__c where sked__Job_Status__c = \'Ready\' ANd DAY_ONLY(convertTimezone(sked__Start__c)) = ' + DateValue;
        list<sked__Job__c> jobs = Database.query(query);
        for (sked__Job__c job : jobs) {
            System.debug('11111111 job ' + job);
        }

        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        job.sked__Start__c = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0));
        job.sked__Finish__c = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0)).addHours(3);
        job.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;
        update job;
        System.debug('11111111 a job ' + job);

        test.stopTest();
    }
}
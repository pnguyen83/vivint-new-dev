global class skedSetting {
	private skedAdminSetting adminSetting;

	public skedAdminSetting Admin {
        get {
            if (adminSetting == null) {
                adminSetting = new skedAdminSetting();
            }
            return adminSetting;
        }
    }

    /*********************************************************** Singleton stuffs ***********************************************************/
    private static skedSetting mInstance = null;

    public static skedSetting instance {
        get {
            if (mInstance == null) {
                mInstance = new skedSetting();
            }
            return mInstance;
        }
    }

    /*********************************************************** Private constructor ***********************************************************/
    private skedSetting() {}

    /*********************************************************** Nested Classes ***********************************************************/
    public class skedAdminSetting {
        public integer velocity ;
        public List<skedVivintModel.TimeSlot> lstTimeSlot;
        public Map<String, List<skedVivintModel.TimeSlot>> specifiedWeekDayTimeSlots;
        public integer noOfDaysToDisplay;
        public string skeduloAPIToken ;
        public boolean ignoreTravelTimeFirstJob;
        public boolean enableConfirmationEmail;
        public boolean enableRescheduledEmail;
        public boolean enable1DayNotificationEmail;
        public String avaiNotificationTemplate;
        public integer noOfJobToDisplay;

        public skedAdminSetting() {
            sked_Admin_Setting__c setting = sked_Admin_Setting__c.getOrgDefaults();
        	this.velocity = setting.Velocity__c != null ? (Integer)setting.Velocity__c : 50;
            System.debug('this.velocity = ' + this.velocity);
            this.lstTimeSlot = new List<skedVivintModel.TimeSlot>();
            this.skeduloAPIToken = setting.Skedulo_API_Token__c;
            this.ignoreTravelTimeFirstJob = Test.isRunningTest() ? true : setting.sked_Ignore_Travel_Time_First_Job_Of_Day__c;
            this.enableConfirmationEmail = Test.isRunningTest() ? true : setting.sked_Enable_Confirmation_Email__c;
            this.enableRescheduledEmail = Test.isRunningTest() ? true : setting.sked_Enable_Rescheduled_Email__c;
            this.enable1DayNotificationEmail = Test.isRunningTest() ? true : setting.sked_Enable_1_day_notification_email__c;
            this.specifiedWeekDayTimeSlots = new Map<String, List<skedVivintModel.TimeSlot>>();
            this.avaiNotificationTemplate = Test.isRunningTest() ? 'abc' : setting.sked_Availability_Notification_Template__c;
            this.noOfJobToDisplay = setting.sked_No_of_Jobs__c != null ? (integer)setting.sked_No_of_Jobs__c : 10;
/*
            for (sked_Slot_Interval__mdt slot : [SELECT Id, Label, sked_Slot_End_Time__c, sked_Slot_Start_Time__c, Specified_Week_Day__c
                                                    FROM sked_Slot_Interval__mdt]) {
                skedVivintModel.TimeSlot timeslot = new skedVivintModel.TimeSlot(slot);
                if (slot.Specified_Week_Day__c == null) {
                    this.lstTimeSlot.add(timeslot);
                }
                else {
                    List<skedVivintModel.TimeSlot> timeslots = this.specifiedWeekDayTimeSlots.get(slot.Specified_Week_Day__c);
                    timeslots = timeslots == null ? new List<skedVivintModel.TimeSlot>() : timeslots;
                    timeslots.add(timeslot);
                    this.specifiedWeekDayTimeSlots.put(slot.Specified_Week_Day__c.tolowerCase(), timeslots);
                }
            }
*/
            this.noOfDaysToDisplay = setting.No_of_days__c != null ? Integer.valueOf(setting.No_of_days__c) : 10;
        }
    }
}
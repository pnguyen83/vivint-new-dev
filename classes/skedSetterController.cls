public class skedSetterController {
	@remoteaction
	public static skedRemoteResultModel saveAppointment(skedVivintModel.Appointment appointment) {
		return skedSetterHandler.handleSaveAppointment(appointment);
	}

	@remoteaction
	public static skedRemoteResultModel lookupLocation(string name) {
		return skedSetterHandler.searchLocation(name);
	}

	@remoteaction
	public static skedRemoteResultModel getConfigData() {
		return skedSetterHandler.collectConfigData();
	}

	@remoteaction
	public static skedRemoteResultModel loadBookingGrid(skedVivintModel.SetterBookingData filter) {
		return skedSetterHandler.getBookingGridData(filter);
	}

	@remoteaction
	public static skedRemoteResultModel lookupAccessId(string accessID) {
		return skedSetterHandler.searchAccessId(accessID);
	}

	@remoteaction
	public static skedRemoteResultModel getTimeLabelOfDay(skedVivintModel.TimeSlotFilterModel filter) {
		return skedCommonService.getTimeLabelOfDayForSetter(filter);
	}
}
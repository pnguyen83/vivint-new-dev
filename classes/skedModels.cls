global class skedModels {
    global virtual class modelBase {
        public string id {get; set;}
        public string name {get; set;}
        public boolean selected {get; set;}
        //public string action {get; set;}
    }

    global virtual class geometry {
        public decimal lat {get; set;}
        public decimal lng {get; set;}

        public geometry(Location geoLocation) {
            if (geoLocation != null) {
                this.lat = geoLocation.getLatitude();
                this.lng = geoLocation.getLongitude();
            }
        }

        public geometry(decimal lat, decimal lng) {
            this.lat = lat;
            this.lng = lng;
        }
    }

    global virtual class address {
        public string fullAddress {get; set;}
        public string postalCode {get;set;}
        public geometry geometry {get; set;}
    }

    global virtual class event extends modelBase implements Comparable {
        public string objectType;
        public string eventType;
        public address address;
        public string startDate;
        public string endDate;
        public integer startTime;
        public integer endTime;
        public integer duration;
        public boolean isAvailable;
        public string relatedId; //jobid for job allocation

        public transient string timezoneSidId;
        public transient Date eventDate;
        public transient DateTime start;
        public transient DateTime finish;
        public transient Location geoLocation;
        public transient Decimal distanceToJob;

        public virtual Integer compareTo(Object compareTo) {
            event compareToRecord = (event)compareTo;
            Integer returnValue = 0;

            if (start > compareToRecord.start) {
                returnValue = 1;
            }
            else if (start < compareToRecord.start) {
                returnValue = -1;
            }
            return returnValue;
        }
    }

    /************************************************ SKEDULO MANAGED OBJECT MODELS ************************************************/
    global virtual class activity extends event {
        public string resourceId {get; set;}
        public string notes {get; set;}
        public string scheduleId {get; set;}
    }

    global virtual class availability extends event {
        public string resourceId {get; set;}
        public string notes {get; set;}
        public string status {get; set;}
        public string scheduleId {get; set;}
        public boolean isAllDay {get; set;}
    }

    global virtual class holiday extends modelBase {
        public string startDate {get; set;}
        public string endDate {get; set;}
        public boolean isGlobal {get; set;}
        public string regionId {get; set;}
    }

    global virtual class templateEntry extends modelBase {
        public integer weekNo {get; set;}
        public string weekday {get; set;}
        public integer startTime {get; set;}
        public integer endTime {get; set;}
        public string action {get; set;}
    }



    global virtual class template extends modelBase {
        public string startDate {get; set;}
        public string endDate {get; set;}
        public List<templateEntry> entries {get; set;}
    }

    global virtual class region extends modelBase {
        public string timezoneSidId {get; set;}

        public region(sked__Region__c skedRegion) {
            this.id = skedRegion.id;
            this.name = skedRegion.name;
            this.timezoneSidId = skedRegion.sked__timezone__c;
        }
    }

    global virtual class skedLocation extends modelBase {
        public string regionId {get; set;}
        public string locationType {get ;set;}
        public geometry geoLocation {get; set;}
        public string address {get; set;}
        public string timezone {get; set;}

        public skedLocation (sked_Setter_Location__c loc) {
            this.id = loc.id;
            this.name = loc.Name;
            this.regionId = string.isNotBlank(loc.sked_Region__c) ? loc.sked_Region__c : null;
            this.address = string.isNotBlank(loc.sked_Location_Address__c) ? loc.sked_Location_Address__c : '';
            this.geoLocation = loc.sked_Geolocation__c != null ? new geometry(loc.sked_Geolocation__c) : null;
            this.timezone = string.isNotBlank(loc.sked_Region__c) ? loc.sked_region__r.sked__timezone__c: '';
        }
    }



    global virtual class job extends event {
        public string accountId {get; set;}
        public string cancellationReason {get; set;}
        public string cancellationReasonNotes {get; set;}
        public string contactId {get; set;}
        public string contactName {get; set;}
        public string description {get; set;}
        public integer jobAllocationCount {get; set;}
        public string jobStatus {get; set;}
        public string notes {get; set;}
        public integer quantity {get; set;}
        public string rescheduleJobId {get; set;}
        public string regionId {get; set;}
        public string scheduleId {get; set;}

        public string serviceLocationId {get; set;}

        public List<job> followups {get; set;}
        public List<jobAllocation> allocations {get; set;}
        public List<jobAllocation> possibleAllocations {get; set;}

        public List<Id> tagIds {get; set;}


        public boolean hasError {get; set;}
        public List<string> errorMessages {get; set;}


        public void loadJobAllocations(List<sked__Job_Allocation__c> skedAllocations) {
            this.allocations = new List<jobAllocation>();
            for (sked__Job_Allocation__c skedAllocation : skedAllocations) {
                skedModels.jobAllocation allocation = new skedModels.jobAllocation();
                allocation.id = skedAllocation.Id;
                allocation.jobId = skedAllocation.sked__Job__c;
                allocation.resourceId = skedAllocation.sked__Resource__c;
                allocation.status = skedAllocation.sked__Status__c;
                if (skedAllocation.sked__Resource__r != null) {
                    skedModels.resource resource = new skedModels.resource();
                    resource.id = skedAllocation.sked__Resource__c;
                    resource.name = skedAllocation.sked__Resource__r.Name;
                    if (skedAllocation.sked__Resource__r.sked__User__r != null) {
                        resource.photoUrl = skedAllocation.sked__Resource__r.sked__User__r.SmallPhotoUrl;
                    }
                    allocation.resource = resource;
                }
                if (skedAllocation.sked__Estimated_Travel_Time__c != NULL) {
                    allocation.travelTimeFrom = integer.valueOf(skedAllocation.sked__Estimated_Travel_Time__c);
                }
                //allocation.travelDistanceFrom = skedAllocation.sked_Estimated_Distance__c;
                this.allocations.add(allocation);
            }
        }
    }

    global virtual class jobAllocation extends event {
        public string jobId {get; set;}
        public string resourceId {get; set;}
        public string resourceName {get; set;}
        public geometry startFromLocation {get; set;}
        public integer travelTimeFrom {get; set;}
        public decimal travelDistanceFrom {get; set;}
        public geometry goToLocation {get; set;}
        public integer travelTimeTo {get; set;}
        public decimal travelDistanceTo {get; set;}
        public boolean isQualified {get; set;}
        public string status {get;set;}

        public job job {get; set;}
        public resource resource {get; set;}
    }

    global virtual class resource extends modelBase {
        public string category {get; set;}
        public string photoUrl {get; set;}
        public string regionId {get; set;}
        public string userId {get; set;}
        public string timezoneSidId {get; set;}
        public integer rating {get; set;}
        public address address {get; set;}
        public decimal distanceToJob;
        public decimal timeToJob;
        //public List<tag> tags;

        public string regionName {get; set;}
        public integer noOfAvailableJobs {get; set;}
       // public accountResourceScore accountResourceScore {get; set;}

      //  public transient DateTime firstTagExpiry {get; set;}
       // public transient Set<Id> tagIds {get; set;}
        public transient Location geoLocation {get; set;}

        public resource() {
           // this.tags = new List<tag>();
           // this.tagIds = new Set<id>();
        }

        public resource (sked__Resource__c skedRes) {
            this.id = skedRes.id;
            this.name = skedRes.name;
        }

       /* public void loadResourceTags(List<sked__Resource_Tag__c> resourceTags) {
            loadResourceTags(resourceTags, NULL);
        } */

        public void loadResourceTags(List<sked__Resource_Tag__c> resourceTags, Set<Id> requiredTagIds) {
          //  this.tags = new List<tag>();
           // this.tagIds = new Set<Id>();
        /*    for (sked__Resource_Tag__c resourceTag : resourceTags) {
                if (requiredTagIds == NULL || requiredTagIds.contains(resourceTag.sked__Tag__c)) {
                    this.tagIds.add(resourceTag.sked__Tag__c);

                    tag tagItem = new tag();
                    tagItem.id = resourceTag.sked__Tag__c;
                    tagItem.name = resourceTag.sked__Tag__r.Name;
                    if (resourceTag.sked__Expiry_Date__c != null) {
                        tagItem.expiryDateTime = resourceTag.sked__Expiry_Date__c;
                        tagItem.expiryDate = resourceTag.sked__Expiry_Date__c.format(skedDateTimeUtils.DATE_FORMAT);
                        tagItem.expiryTime = integer.valueOf(resourceTag.sked__Expiry_Date__c.format('Hmm'));
                    }
                    this.tags.add(tagItem);

                    if (resourceTag.sked__Expiry_Date__c != NULL) {
                        if (this.firstTagExpiry == NULL || this.firstTagExpiry < resourceTag.sked__Expiry_Date__c) {
                            this.firstTagExpiry = resourceTag.sked__Expiry_Date__c;
                        }
                    }
                }
            } */
        }

        public void loadResourceScore(List<sked__Account_Resource_Score__c> resourceScores) {
        /*    if (resourceScores != NULL && !resourceScores.isEmpty()) {
                sked__Account_Resource_Score__c resourceScore = resourceScores.get(0);
                this.accountResourceScore = new skedModels.accountResourceScore();
                this.accountResourceScore.whitelisted = resourceScore.sked__Whitelisted__c;
                this.accountResourceScore.blacklisted = resourceScore.sked__Blacklisted__c;
            } */
        }


    }

    /************************************************ SKEDULO CUSTOM MODELS ************************************************/
 /*   global virtual class clientAvailability extends event {
        public string regionId {get; set;}
        public string patientId {get; set;}
        public string scheduleId {get; set;}
        public string serviceLocationId {get; set;}
        public string notes {get; set;}
        public string preferredStartDate {get; set;}
        public integer preferredStartTime {get; set;}
        public string preferredEndDate {get; set;}
        public integer preferredEndTime {get; set;}
    }

    global virtual class displaySetting extends modelBase {
        public integer availableEnd {get; set;}
        public integer availableStart {get; set;}
        public string backgroundColor {get; set;}
        public string color {get; set;}
        public string shiftType {get; set;}
        public boolean showLegend {get; set;}
    }

    global virtual class exceptionLog extends modelBase {
        public string exceptionCode {get; set;}
        public string caseNumber {get; set;}
        public string patientName {get; set;}
        public skedModels.job job {get; set;}
        public skedModels.resource resource {get; set;}
    } */

    global virtual class selectOption {
        public string id {get; set;}
        public string name {get; set;}
        public boolean selected {get; set;}
        public string weekDay {get; set;}

        public selectOption(string value, string text) {
            this.id = value;
            this.name = text;
            this.selected = false;
        }

        public selectOption(string value, string text, string weekDay) {
            this.id = value;
            this.name = text;
            this.weekDay = weekDay;
        }
    }
}
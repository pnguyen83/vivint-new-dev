public with sharing class skedSetterLocationHandler {
	public static void onAfterInsert(List<sked_Setter_Location__c> lstNews) {
		udpateNumberOfLocationOnRegion(lstNews, null);
	}

	public static void onAfterUpdate(List<sked_Setter_Location__c> lstNews, Map<id, sked_Setter_Location__c> mapOlds) {
		udpateNumberOfLocationOnRegion(lstNews, mapOlds);
	}

	public static void onAfterDelete(List<sked_Setter_Location__c> lstOlds) {
		udpateNumberOfLocationOnRegion(lstOlds, null);
	}

	//********************************************Private Functions**********************************************//
	private static void udpateNumberOfLocationOnRegion(List<sked_Setter_Location__c> lstNews, Map<id, sked_Setter_Location__c> mapOlds) {
		Set<String> setRegionIds = new Set<String>();

		for (sked_Setter_Location__c loc : lstNews) {
			if (mapOlds != null) {
				if (mapOlds.containsKey(loc.id)) {
					sked_Setter_Location__c oldLoc = mapOlds.get(loc.id);
					if (oldLoc.sked_Region__c == null || oldLoc.sked_Region__c != loc.sked_Region__c ||
						(oldLoc.sked_Region__c != null && loc.sked_Region__c == null)) {
						if (loc.sked_Region__c != null) {
							setRegionIds.add(loc.sked_Region__c);
							setRegionIds.add(oldLoc.sked_Region__c);			
						}
						else {
							setRegionIds.add(oldLoc.sked_Region__c);
						}
					}
				}
			}
			else {
				setRegionIds.add(loc.sked_Region__c);	
			}
		}

		if (!setRegionIds.isEmpty()) {
			List<sked__Region__c> lstRegions = new List<sked__Region__c>();
			System.debug('setRegionIds = ' + setRegionIds);
			for (sked__Region__c reg : [SELECT Id, (SELECT Id FROM Stores__r)
											FROM sked__Region__c
											WHERE Id IN : setRegionIDs]) {
				sked__Region__c region = new sked__Region__c(
					id = reg.Id,
					sked_Number_of_Locations__c = reg.Stores__r.size()
				);
				lstRegions.add(region);
			}

			if (!lstRegions.isEmpty()) {
				update lstRegions;
			}
		}
	}
}
@isTest
public with sharing class skedCloserControllerTest {
	public static testmethod void getConfigDataTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedRemoteResultModel result = skedCloserController.getConfigData();
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void searchLocationTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        skedRemoteResultModel result = skedCloserController.searchLocation(location.name);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getJobsTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        String timezone = UserInfo.getTimeZone().getId();
        test.startTest();
        skedVivintModel.CloserData data = new skedVivintModel.CloserData();
        data.startDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        data.endDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        resource.sked__User__c = UserInfo.getUserId();
        update resource;

        skedRemoteResultModel result = skedCloserController.getJobs(data);
        test.stopTest();
    }

    public static testmethod void getJobsTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        String timezone = UserInfo.getTimeZone().getId();
        test.startTest();
        skedVivintModel.CloserData data = new skedVivintModel.CloserData();
        data.startDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        data.endDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);

        skedRemoteResultModel result = skedCloserController.getJobs(data);
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void getRescheduleAvailabilityTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        String timezone = UserInfo.getTimeZone().getId();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        skedVivintModel.JobAvaiModel data = new skedVivintModel.JobAvaiModel();
        data.regions = new List<String>{region.id};
        data.jobId = job.id;
        data.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);

        skedRemoteResultModel result = skedCloserController.getRescheduleAvailability(data);
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void getRescheduleAvailabilityTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        String timezone = UserInfo.getTimeZone().getId();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        skedVivintModel.JobAvaiModel data = new skedVivintModel.JobAvaiModel();
        data.regions = new List<String>{region.id};
        data.jobId = job.id;

        skedRemoteResultModel result = skedCloserController.getRescheduleAvailability(data);
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void rescheduleJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.RescheduleJob data = new skedVivintModel.RescheduleJob();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        data.jobId = job.id;
        data.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        data.startTime = 800;
        data.endTime = 1100;
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        resource.sked__User__c = UserInfo.getUserId();
        update resource;

        skedRemoteResultModel result = skedCloserController.rescheduleJob(data);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void confirmJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.Appointment data = new skedVivintModel.Appointment();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        data.jobId = job.id;

        skedRemoteResultModel result = skedCloserController.confirmJob(data);
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void cancelJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.Appointment data = new skedVivintModel.Appointment();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        data.jobId = job.id;
        data.isCancel = true;
        data.reason = 'Canceled: On-Site';

        skedRemoteResultModel result = skedCloserController.cancelJob(data);
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void getMoreJobInfoTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);

        skedRemoteResultModel result = skedCloserController.getMoreJobInfo(job.id);
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void updateJobInfoTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__job__c job = (sked__job__c)objFactory.getSObjects(sked__job__c.sObjectType).get(0);
        skedVivintModel.VivintContact contact = new skedVivintModel.VivintContact(job);

        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        job.sked_Setter_Location__c = location.id;
        update job;
        contact.jobId = job.id;

        skedRemoteResultModel result = skedCloserController.updateJobInfo(contact);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void searchJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());

        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};

        skedRemoteResultModel result = skedCloserController.searchJob(filter);
        System.debug('result ' + result);

        test.stopTest();
    }
}
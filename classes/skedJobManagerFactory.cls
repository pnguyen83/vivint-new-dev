public class skedJobManagerFactory extends skedJobManager{
    public static final String HANDLE_UNALLOCATED_JOB = 'Unallocated Jobs Handler';

    // private static variable referencing the skedJobManagerFactory class
    public static skedJobManagerFactory instance {
        get {
            if (instance == null) instance = new skedJobManagerFactory();
            return instance;
        }
        set;
    }
	//Return action map
    public override Map<String,String> getActionMap(){
        return new Map<String,String>{
            HANDLE_UNALLOCATED_JOB  => 'skedJobManagerFactory.UnallocatedJobHandler'
        };
    }

    //======= EXCUTION  METHODS =======
    public class UnallocatedJobHandler extends skedJobManager{
        public Integer DELAY_INTERVAL = 7200;
        //Start a job
        public override void startJob(skedJob job){
            if (sked_Admin_Setting__c.getOrgDefaults().sked_Interval_Time__c != null) {
                DELAY_INTERVAL = (Integer)(sked_Admin_Setting__c.getOrgDefaults().sked_Interval_Time__c * 60);
            }
            System.debug('DELAY_INTERVAL ' + DELAY_INTERVAL);
            super.startJob(job);
            startAfter(job, DELAY_INTERVAL);
        }

        //Handling the execution of synchronous jobs, Schedulable jobs and serial jobs
        public override void executeJob(skedJob job){
            startBatchJob(job, 1);
        }

        public override List<sObject> startBatch(skedJob job){
            skedHandleUnallocatedJobUtils handler = new skedHandleUnallocatedJobUtils();

            return handler.getListValidSlotForBatch();
        }

        /**
        * Handle Batchable.start()
        */
        public override void executeBatch(skedJob job){
            List<sked__Slot__c> slots = (List<sked__Slot__c>)job.objects;
            skedHandleUnallocatedJobUtils handler = new skedHandleUnallocatedJobUtils();

            for (sked__Slot__c slot : slots) {
                handler.allocateResourceToJob(slot, job.map_id_avaiResAllJobs);
            }
        }

        /**
        * Handle Batchable.finish()
        */
        public override void finishBatch(skedJob job){
            if(!job.exceptions.isEmpty()){
                if (sked_Admin_Setting__c.getOrgDefaults().sked_Administrator_Email__c != null &&
                    skedCommonService.validateEmail(sked_Admin_Setting__c.getOrgDefaults().sked_Administrator_Email__c)) {
                    String emailAddress = sked_Admin_Setting__c.getOrgDefaults().sked_Administrator_Email__c;
                    String token = skedCommonService.getAPIToken();
                    System.debug('emailAddress ' + emailAddress);
                    String subject = 'Error happens when allocate pros automatically on ' + System.now().format(skedConstants.WEEKDAY_DAY_MONTH);
                    String emailBody = 'Error: <br/>';
                    for (Exception ex : job.exceptions) {
                        emailBody += ex.getMessage() + '<br/>';
                        emailBody += ex.getStackTraceString() + '<br/>';
                    }

                    skedHandleUnallocatedJobUtils handler = new skedHandleUnallocatedJobUtils();
                    Messaging.Singleemailmessage email = handler.createNotificationEmail(emailAddress, subject, emailBody, token);
                    Messaging.sendEmail(new List<Messaging.Singleemailmessage>{email});
                }

            }
            skedJobManagerFactory.instance.register( skedJobManagerFactory.HANDLE_UNALLOCATED_JOB, null);
        }
    }


}
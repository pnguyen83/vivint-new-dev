public class skedSObjectFactoryExt extends skedSObjectFactory{

    /**
    * @description override to initialize more objects
    * @param obj
    */
    public override void initMore(){
        this.getObject(sked__Resource__c.sObjectType).fields(new Map<String, Object>{
                'sked__GeoLocation__latitude__s'     => 40.7376731,
                'sked__GeoLocation__longitude__s'    => -73.9897125,
                'sked__Resource_Type__c'    => 'Person',
                'sked__User__c' => UserInfo.getUserId(),
                'sked__Mobile_Phone__c' => '12345678'
                });
        this.getObject(sked__job__c.sObjectType).fields(new Map<String, Object>{
                'sked_Customer_Email__c'     => 'abc@email.com',
                'sked__Contact__c' => this.newRelationship(Contact.sObjectType),
                'sked_accepts_Vivint_Policy__c' => true,
                'ssked_Prospect_Additional_information__c' => 'abc',
                'sked_Prospect_Address__c' => '123 abc',
                'sked_Prospect_has_speed_internet__c' => 'yes',
                'sked_Prospect_owns_Home_or_Business__c' => 'yes',
                'sked_Prospect_interested_in_Smart_Home__c' => '10',
                'sked_Prospect_Rep_ID__c' => 'abc',
                'sked_Vivint_Account_Number__c' => 'abc',
                'sked_Area_Interested_In_Smart_Home__c' => 'Smart Assistant',
                'sked_Customer_Phone__c' => '123456',
                'sked_Setter_Name__c' => 'abc',
                'sked_Will_a_decision_maker_be_present__c' => 'yes',
                'sked_Do_you_have_security_system__c' => 'yes',
                'sked_How_long_have_you_had_your_system__c' => '12',
                'sked_Is_your_system_being_monitored__c' => 'yes'
                });
        this.newObject(sked__Region_Area__c.sObjectType).fields(new Map<String,Object>{
                'sked_Zip_Code_Name__c'                 => '00002',
                'sked__Region__c'           => this.newRelationship(sked__Region__c.sObjectType)
            });

        this.newObject(sked__Holiday__c.sObjectType).fields(new Map<String,Object>{
                'Name' => 'Holiday 1',
                'sked__Start_Date__c' => System.today(),
                'sked__End_Date__c' => System.today().addDays(2),
                'sked__Global__c' => true
            });

        this.newObject(sked_Setter_Location__c.sObjectType).fields(new Map<String,Object>{
                'Name'                 => 'Loc 00001',
                'sked_Region__c '           => this.newRelationship(sked__Region__c.sObjectType),
                'sked_GeoLocation__latitude__s'     => 40.7376731,
                'sked_GeoLocation__longitude__s'    => -73.9897125
            });

        this.newObject(sked_Setter__c.sObjectType).fields(new Map<String,Object>{
                'Name'                 => 'Setter 1',
                'sked_Access_ID__c '           => '1234',
                'sked_Active__c '     => true
            });

        this.newObject(sked_Queue_Management_Access__c.sObjectType).fields(new Map<String,Object>{
                'sked_Region__c'           => this.newRelationship(sked__Region__c.sObjectType),
                'sked_Resource__c'           => this.newRelationship(sked__Resource__c.sObjectType),
                'sked_Role__c'     => 'Regional Manager'
            });

        this.newObject(Skedulo_SMS__c.sObjectType).fields(new Map<String,Object>{
                'api_key__c'           => '357e2907',
                'api_secret__c'           => '6e7bd178',
                'From_Phone_Number__c'     => '123456',
                'PendingDispatchSMS__c'   => 'You have a new appointment! [JOBNAME] for [CONTACTNAME] on [JOBTIME]',
                'Default_SMS__c'  => 'You have a new appointment! [JOBNAME] for [CONTACTNAME] on [JOBTIME] at [HOUR]'
            });

        this.newObject(sked_Admin_Setting__c.sObjectType).fields(new Map<String,Object>{
                'Velocity__c'                 => 50,
                'Skedulo_API_Token__c'           => 'abcdfrdfgdfgdfg',
                'sked_Ignore_Travel_Time_First_Job_Of_Day__c' => true,
                'sked_Enable_Confirmation_Email__c' => true,
                'sked_Enable_Rescheduled_Email__c' => true,
                'sked_Enable_1_day_notification_email__c' => true,
                'sked_Availability_Notification_Template__c' => 'test template',
                'sked_No_of_Jobs__c' => 10,
                'No_of_days__c' => 10
            });



    }

    /**
    * @description override to initialize an instance of SObjectModelExt
    *
    */
    public override SObjectModel initNewObject(Schema.sObjectType sObjectType){
        return new SObjectModelExt(sObjectType);
    }

    /**
    * @description extend SObjectModel format more objects
    */
    public  class SObjectModelExt extends SObjectModel{

        public SObjectModelExt(Schema.sObjectType sObjectType){
            super(sObjectType);
        }

        /**
        * @description This method will be overridden by a subclass to format data before creation
        * @param sObjectType
        * @param obj
        */
        public override void formatMore(Schema.sObjectType sObjectType, List<sObject> objects){

        }
    }
}
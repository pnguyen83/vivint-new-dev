public class skedQueueManageAccessHandler {
	public static void onBeforeInsert(List<sked_Queue_Management_Access__c> news) {
        checkPrimaryField(news, null);
    }

    public static void onBeforeUpdate(List<sked_Queue_Management_Access__c> news, Map<id, sked_Queue_Management_Access__c> map_id_old) {
        checkPrimaryField(news, map_id_old);
    }

    //===================================Private functions=====================================//
    private static void checkPrimaryField(List<sked_Queue_Management_Access__c> news, Map<id, sked_Queue_Management_Access__c> map_id_old) {
        Map<id, List<sked_Queue_Management_Access__c>> map_regionId_qma = new Map<id, List<sked_Queue_Management_Access__c>>();

        for (sked_Queue_Management_Access__c qma : news) {
            if (qma.sked_Primary__c) {
                List<sked_Queue_Management_Access__c> qmas = map_regionId_qma.get(qma.id);
                if (qmas == null) {
                    qmas = new List<sked_Queue_Management_Access__c>();
                }
                if (map_id_old != null && map_id_old.containsKey(qma.id)) {
                    if (map_id_old.get(qma.id).sked_Primary__c == false) {
                        qmas.add(qma);
                    }
                }
                else  {
                    qmas.add(qma);
                }

                map_regionId_qma.put(qma.sked_Region__c, qmas);
            }
        }

        if (!map_regionId_qma.isEmpty()) {
            List<id> regionIds = new List<id>(map_regionId_qma.keySet());
            skedObjectSelector selector = skedObjectSelector.newInstance(sked_Queue_Management_Access__c.sObjectType);
            selector.filter('sked_Region__c IN: regionIds');
            List<sked_Queue_Management_Access__c> qmas = new List<sked_Queue_Management_Access__c>();
            qmas = Database.query(selector.getQuery());

            for (sked_Queue_Management_Access__c qma : qmas) {
                if (qma.sked_Primary__c && map_regionId_qma.containsKey(qma.sked_Region__c)) {
                    List<sked_Queue_Management_Access__c> newQmas = map_regionId_qma.get(qma.sked_Region__c);
                    for (sked_Queue_Management_Access__c newQma : newQmas) {
                        newQma.addError(skedConstants.QMA_DUPLICATE_PRIMARY_ERROR);
                    }
                }
            }
        }
    }
}
public with sharing class skedSMS {
	public string api_key { get; set; }
    public string api_secret { get; set; }
    public string to { get; set; }
    public string tfrom { get; set; }
    public string text { get; set; }

    public skedSMS(){
        Skedulo_SMS__c SMS = Skedulo_SMS__c.getOrgDefaults();
        if(SMS != null){
            api_key = SMS.api_key__c;
            api_secret = SMS.api_secret__c;
            tfrom = SMS.From_Phone_Number__c;
        }
    }

    public skedSMS(string type){
        Skedulo_SMS__c SMS = Skedulo_SMS__c.getOrgDefaults();
        if(SMS != null){
            api_key = SMS.api_key__c;
            api_secret = SMS.api_secret__c;
            tfrom = SMS.From_Phone_Number__c;

            if(type == skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH){
               text = SMS.PendingDispatchSMS__c; //
            }
            else {
            	text = SMS.Default_SMS__c;
            }
        }
    }


    public static string generateSMS(string text, sked__Job_Allocation__c jall){
    	string rtnSMS = text;
        rtnSMS = rtnSMS.replace('[JOBNAME]',jall.sked__Job__r.Name);
        rtnSMS = rtnSMS.replace('[JOBTIME]',jall.sked__Job__r.sked__Start__c.format(skedConstants.WEEKDAY_DAY_MONTH_YEAR,jall.sked__Job__r.sked__TimeZone__c));
        rtnSMS = rtnSMS.replace('[CONTACTNAME]',jall.sked__Job__r.sked_Prospect_Name__c);
        rtnSMS = rtnSMS.replace('[HOUR]',jall.sked__Job__r.sked__Start__c.format(skedConstants.HOUR_MINUTES_ONLY,jall.sked__Job__r.sked__TimeZone__c));

        return rtnSMS;

    }
}
public class skedSkeduloAPI {
	public static final String SKEDULO_END_POINT    = 'https://api.skedulo.com/notifications/';

    public static void sendOneOffMessage(String token, String jsonBody) {
        Http http      = new Http();
        HttpRequest req    = new HttpRequest();
        HttpResponse res  = new HttpResponse();
        string jsonResponse;
        System.debug('token ' + token);
        String endPoint = SKEDULO_END_POINT + 'oneoff';
        req.setEndpoint( EndPoint );
        req.setMethod('POST');
        req.setTimeout(10000);
        req.setHeader('Content-Type', 'application/json');
        System.debug('jsonBody ' + jsonBody);
        String authenKey = 'Bearer ' + token;
        req.setHeader('Authorization', authenKey);
        req.setBody(jsonBody);

        res = http.send(req);
        jsonResponse = res.getBody();
        Integer statusCode = res.getStatusCode();
        System.debug('res ' + res);
        if (statusCode == 400) {
            throw new skedException(jsonResponse);
        }
    }
}
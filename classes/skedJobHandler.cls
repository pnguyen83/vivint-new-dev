public class skedJobHandler {
    public static void onAfterUpdate(List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_oldJobs) {
        updateVivintAccountNumberField(newJobs, map_oldJobs);
        sendNotificationChangeJobTime(newJobs, map_oldJobs);
    }

    public static void onBeforeInsert(List<sked__Job__c> newJobs) {
        setJobStartMinuteAndJobEndMinuteValue(newJobs);
    }

    public static void onBeforeUpdate(List<sked__Job__c> newJobs) {
        setJobStartMinuteAndJobEndMinuteValue(newJobs);
    }

    //===============================================================Private functions=========================================//
    private static void updateVivintAccountNumberField(List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_oldJobs) {
        Map<Id, sked__Job__c> map_contactId_jobs = new Map<id, sked__Job__c>();
        List<sObject> objects = new List<sObject>();

        for (sked__Job__c skedJob : newJobs) {
            if (map_oldJobs.containsKey(skedJob.id)) {
                sked__Job__c skedOldJob = map_oldJobs.get(skedJob.id);
                map_contactId_jobs.put(skedJob.sked__Contact__c, skedJob);
            }
        }

        if (!map_contactId_jobs.isEmpty()) {

            for (Contact cont : [SELECT id, AccountId, FirstName, LastName
                                    FROM Contact
                                    WHERE Id IN :map_contactId_jobs.keySet()]) {
                sked__Job__c skedJob = map_contactId_jobs.get(cont.id);
                Account acc = new Account(id = cont.AccountId);
                boolean isUpdate = false;

                if (skedJob.sked_Customer_Email__c != null) {
                    cont.Email = skedJob.sked_Customer_Email__c;
                    isUpdate = true;
                }

                if (skedJob.sked_Prospect_First_Name__c != null) {
                    cont.FirstName = skedJob.sked_Prospect_First_Name__c;
                    isUpdate = true;
                }

                if (skedJob.sked_Prospect_Last_Name__c != null) {
                    cont.LastName = skedJob.sked_Prospect_Last_Name__c;
                    isUpdate = true;
                }

                acc.Name = cont.FirstName + ' ' + cont.LastName;

                if (skedJob.sked_Customer_Phone__c != null) {
                    //cont.phone = skedJob.sked_Customer_Phone__c;
                    cont.MobilePhone = skedJob.sked_Customer_Phone__c;
                    acc.Phone = skedJob.sked_Customer_Phone__c;
                    isUpdate = true;
                }

                if (isUpdate) {
                    objects.add(cont);
                    objects.add(acc);
                }

            }

            if (!objects.isEmpty()) {
                objects.sort();
                update objects;
            }
        }
    }

    private static void sendNotificationChangeJobTime(List<sked__Job__c> newJobs, Map<id, sked__Job__c> map_oldJobs) {
        List<Messaging.Singleemailmessage> emailTemplates = new List<Messaging.Singleemailmessage>();
        Map<id, sked__Job__c> map_id_selectedjobs = new Map<id, sked__job__c>();
        Id orgWideEmailId = skedCommonService.getOrgWideEmail();

        for (sked__Job__c skedJob : newJobs) {
            if (map_oldJobs.containsKey(skedJob.id)) {
                sked__Job__c oldJob = map_oldJobs.get(skedJob.id);
                if (skedJob.sked__Start__c != oldJob.sked__Start__c ||
                    skedJob.sked__Finish__c != oldJob.sked__Finish__c) {
                    map_id_selectedjobs.put(skedJob.id, skedJob);
                }
            }
        }

        if (!map_id_selectedjobs.isEmpty()) {
            for (sked__Job_Allocation__c skedJA : [SELECT id, sked__Resource__r.Name, sked__Resource__r.sked__Email__c,
                                                            sked__Job__c, sked__Resource__r.sked__Primary_Phone__c,
                                                            sked__Job__r.sked__Contact__r.FirstName,
                                                            sked__Job__r.sked__Contact__r.Email
                                                    FROM sked__Job_Allocation__c
                                                    WHERE sked__Job__c IN :map_id_selectedjobs.keySet()
                                                    AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_COMPLETE
                                                    AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED
                                                    AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED]) {
                if (map_id_selectedjobs.containsKey(skedJA.sked__Job__c)) {
                    sked__Job__c skedJob = map_id_selectedjobs.get(skedJA.sked__Job__c);
                    skedVivintModel.SendOutEmailModel data = new skedVivintModel.SendOutEmailModel(skedJob.sked__Start__c, skedJob.sked__Finish__c,
                                                                    skedJA.sked__Resource__r.Name, skedJA.sked__Resource__r.sked__Primary_Phone__c,
                                                                    skedJob.sked__Timezone__c, skedJA.sked__Job__r.sked__Contact__r.FirstName,
                                                                    skedJob.sked__Address__c, skedJA.sked__Job__r.sked__Contact__r.Email,
                                                                    skedConstants.RESCHEDULED_APPOINTMENT_EMAIL);
                    data.orgWideEmailId = orgWideEmailId;
                    Messaging.Singleemailmessage email = skedCloserHandler.sendOutEmail(data);
                    email.setReplyTo(skedJA.sked__Resource__r.sked__Email__c);

                    emailTemplates.add(email);
                }
            }

            if (!emailTemplates.isEmpty()) {
                Messaging.sendEmail(emailTemplates);
            }
        }
    }

    private static void setJobStartMinuteAndJobEndMinuteValue(List<sked__Job__c> newJobs) {
        Map<id, Set<sked__Job__c>> map_regionid_job = new Map<id, Set<sked__Job__c>>();

        for (sked__Job__c skedJob : newJobs) {
            if ((skedJob.sked_Job_Start_Minutes__c == null && skedJob.sked__Start__c != null) ||
                (skedJob.sked_Job_End_Minutes__c == null && skedJob.sked__Finish__c != null)) {
                Set<sked__Job__c> skedJobs = map_regionid_job.get(skedJob.sked__Region__c);
                if (skedJobs == null) {
                    skedJobs = new Set<sked__Job__c>();
                }
                skedJobs.add(skedJob);
                map_regionid_job.put(skedJob.sked__Region__c, skedJobs);
            }
        }

        if (!map_regionid_job.isEmpty()) {
            for (sked__Region__c skedRegion : [SELECT id, sked__Timezone__c
                                                FROM sked__Region__c
                                                WHERE id IN: map_regionid_job.keySet()
                                                ]) {
                if (map_regionid_job.containsKey(skedRegion.id)) {
                    Set<sked__Job__c> skedJobs = map_regionid_job.get(skedRegion.id);
                    for (sked__Job__c skedJob : skedJobs) {
                        DateTime jobTimeStartDate = skedDateTimeUtils.getStartOfDate(skedJob.sked__Start__c, skedRegion.sked__Timezone__c);
                        Integer startMinutes = skedDateTimeUtils.getDifferenteMinutes(jobTimeStartDate, skedJob.sked__Start__c);
                        skedJob.sked_Job_Start_Minutes__c = startMinutes;
                        Integer endMinutes = skedDateTimeUtils.getDifferenteMinutes(jobTimeStartDate, skedJob.sked__Finish__c);
                        skedJob.sked_Job_End_Minutes__c = endMinutes;
                    }
                }
            }
        }
    }
}
public class skedSetterHandler {
    public static skedRemoteResultModel handleSaveAppointment(skedVivintModel.Appointment appointment) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        //SavePoint sp = Database.setSavePoint();
        try {
            if (String.isBlank(appointment.contactInfo.postalCode)) {
                result.errorMessage = skedConstants.ADDRESS_MISSING_ZIPCODE;
                result.success = false;
                return result;
            }

            List<sked__Region_Area__c> zipcodes = getZipCodesFromName(appointment.contactInfo.postalCode);

            if (zipcodes == null || zipcodes.isEmpty()) {
                result.errorMessage = skedConstants.ZIPCODE_NO_REGION;
                result.success = false;
                return result;
            }

            //get timezone
            string timezone = zipcodes.get(0).sked__Region__r.sked__Timezone__c;
            String regionId = zipcodes.get(0).sked__Region__c;
            Integer velocity = skedSetting.instance.Admin.velocity;

            appointment.jobInfo.regionId = regionId;

            //get suitable resource and assign
            skedResourceAvailabilityBase.resourceModel res = getSuitableResource(appointment.contactInfo.location.geoLocation,
                                                            appointment.lstRes, velocity);

            //create contact
            Contact cont = createContact(appointment.contactInfo);

            if (cont != null) {
                insert cont;
            }

            appointment.contactInfo.id = cont.id;

            //create job
            sked__Job__c job = createJob(appointment.jobInfo, timezone, appointment.contactInfo);

            if (job != null) {
                insert job;

                if (skedConstants.SLOT_RESOURCE_TYPE.equalsIgnoreCase(appointment.slotType)) {
                    //create job allocation
                    sked__Job_Allocation__c ja = createJobAllocation(job.id, res.id);
                    insert ja;
                }
                else {
                    if (!System.isBatch()) {
                        skedCommonService.sendSmsNotificationForUnAllocatedJobs(regionId, job.id);
                    }

                    //send notification email to customer
                    skedVivintModel.SendOutEmailModel data = new skedVivintModel.SendOutEmailModel(job.sked__Start__c, job.sked__Finish__c,
                                                                    '', '',
                                                                    timezone, cont.FirstName,
                                                                    job.sked__Address__c, cont.Email,
                                                                    skedConstants.UNALLOCATED_EMAIL);
                    data.orgWideEmailId = skedCommonService.getOrgWideEmail();
                    data.isUnAllocated = true;
                    Messaging.Singleemailmessage email = skedCloserHandler.sendOutEmail(data);
                    Messaging.sendEmail(new List<Messaging.Singleemailmessage>{email});
                }

                skedVivintModel.VivintJA jaModel = new skedVivintModel.VivintJA(job.sked__Start__c, job.sked__finish__c, job.sked__Address__c,
                            res, timezone, appointment.contactInfo.location.geoLocation);

                result.data = jaModel;
            }
        }
        catch (Exception ex) {
            result.getError(ex);
            //Database.rollback(sp);
        }

        return result;
    }

    public static skedRemoteResultModel searchLocation(string name) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            List<skedModels.selectOption> lstData = skedCommonService.searchLocation(name);
            result.data = lstData;
        }
        catch(Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel collectConfigData() {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = new skedVivintModel.SetterSettingData();
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getBookingGridData (skedVivintModel.SetterBookingData filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {

            if (String.isBlank(filter.postalCode)) {
                result.errorMessage = skedConstants.ADDRESS_MISSING_ZIPCODE;
                result.success = false;
                return result;
            }

            List<sked__Region_Area__c> zipcodes = getZipCodesFromName(filter.postalCode);

            if (zipcodes == null || zipcodes.isEmpty()) {
                result.errorMessage = skedConstants.ZIPCODE_NO_REGION;
                result.success = false;
                return result;
            }

            String regionId =   zipcodes.get(0).sked__Region__c;
            String timezone =   zipcodes.get(0).sked__Region__r.sked__Timezone__c;

            List<skedVivintModel.DaySlot> lstDaySlots = new List<skedVivintModel.DaySlot>();

            //get date range of grid
            Date selectedDate = Date.valueOf(filter.selectedDate);
            DateTime startOfGrid = skedDateTimeUtils.getStartOfDate(selectedDate, timezone);
            DateTime endOfGrid = skedDateTimeUtils.addDays(startOfGrid, 6, timezone);
            endOfGrid = skedDateTimeUtils.getEndOfDate(endOfGrid, timezone);
            DateTime tempTime = startOfGrid;

            //set jobLocation for timeslot
            List<skedVivintModel.TimeSlot> lstTimeSlots = skedCommonService.getListTimeSlot(filter.selectedDate, new List<String>{regionId});
            for (skedVivintModel.TimeSlot slot : lstTimeSlots) {
                slot.jobLocation = Location.newInstance(filter.jobLocation.lat, filter.jobLocation.lng);
            }

            //get available resource in region
            Set<Id> setResIds = skedCommonService.getResourceIdsInRegion(regionId);
            set<Date> setInputDates = new Set<Date>();

            DateTime tempStartDate = startOfGrid;
            Integer index = 0;

            while (index < 7) {
                setInputDates.add(skedDateTimeUtils.getDate(tempStartDate, timezone));
                index++;
                tempStartDate = skedDateTimeUtils.addDays(tempStartDate, 1, timezone);
            }
            skedAvailatorParams params = new skedAvailatorParams();
            params.timezoneSidId = timezone;
            params.startDate = skedDateTimeUtils.getDate(startOfGrid, timezone);
            params.endDate = skedDateTimeUtils.getDate(endOfGrid, timezone);
            params.resourceIds = setResIds;
            params.inputDates = setInputDates;
            params.regionId = regionId;

            skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
            Map<Id, skedResourceAvailabilityBase.resourceModel> mapResource = resourceAvailability.initializeResourceList();

            //get day of week filter
            Set<String> setDaysOfWeek = new Set<String>(filter.dateOfWeek);
            Set<String> setTimeOfDay = new Set<String>(filter.timeOfDate);

            //get config setting data
            Integer velocity = skedSetting.instance.Admin.velocity;
            boolean ignoreTravelTimeFirstJob = skedSetting.instance.Admin.ignoreTravelTimeFirstJob;

            while (tempTime < endOfGrid) {
                Date checkDate = skedDateTimeUtils.getDate(tempTime, timezone);

                if (setInputDates.contains(checkDate)) {
                    skedVivintModel.DaySlot daySlot = new skedVivintModel.DaySlot(tempTime, timezone);

                    skedVivintModel.TimeSlotCheckingModel checkingData = new skedVivintModel.TimeSlotCheckingModel(lstTimeSlots,
                                                                                setTimeOfDay, timezone, tempTime, mapResource,
                                                                                dayslot.dayString, daySlot, velocity, ignoreTravelTimeFirstJob);

                    daySlot = checkTimeSlotOfDateSlot(checkingData);

                    if (!setDaysOfWeek.isEmpty()) {
                        if (setDaysOfWeek.contains(daySlot.weekday)) {
                            lstDaySlots.add(daySlot);
                        }
                    }
                    else {
                        System.debug('dayslot.dayString ' + dayslot.dayString);
                        lstDaySlots.add(daySlot);
                    }
                }
                tempTime = skedDateTimeUtils.addDays(tempTime, 1, timezone);
            }

            result.data = lstDaySlots;
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel searchAccessId(string accessId) {
        skedRemoteResultModel result = new skedRemoteResultModel();
        string filter = '%' + accessId.replace(' ', '%') + '%';
        try {
            List<sked_Setter__c> lstSetters = [SELECT Id, sked_Active__c, sked_Access_ID__c
                                                FROM sked_Setter__c
                                                WHERE sked_Access_ID__c LIKE :filter
                                                AND sked_Active__c = true];
            if (lstSetters == null || lstSetters.isEmpty()) {
                result.success = false;
                result.message = skedConstants.ACCESS_ID_NOT_AVAI;
            }
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static boolean checkResourceMaxTravelRadius(skedResourceAvailabilityBase.resourceModel res, Location jobLocation) {
        boolean result = false;
        if (res.address != null && res.address.geometry != null) {
            Location homeLocation = Location.newInstance(res.address.geometry.lat, res.address.geometry.lng);
            Decimal travelDistance = homeLocation.getDistance(jobLocation, 'mi');
            if (travelDistance > res.maximumTravelRadius) {
                result = false;
            }
            else {
                result = true;
            }
        }

        return result;
    }

    public static boolean checkTravelTimeOfResourceStraightLine(Location jobLocation, DateTime slotStart,
                                                                DateTime slotEnd, List<skedModels.event> events,
                                                                skedResourceAvailabilityBase.resourceModel res,
                                                                Integer velocity) {
        boolean isAvai = true;

        if (res.allowMoreJob || res.ignoreTravelTime) {
            return true;
        }

        skedModels.event previousEvent, nextEvent;
        boolean hasPreviousConfirmed = false;
        boolean hasNextConfirmed = false;

        for (skedModels.event event : events) {

            if (slotEnd < event.start && nextEvent != NULL) {
                continue;
            }
            if (event.start < slotEnd && event.finish > slotStart) {
                isAvai = false;
                break;
            }

            if (event.finish <= slotStart && event.geoLocation != null) {
                    if (event.objectType == 'jobAllocation') {
                        if (previousEvent == null || event.finish > previousEvent.finish || (event.finish == previousEvent.finish && hasPreviousConfirmed == false)) {
                            String status = ((skedModels.jobAllocation)event).status;
                            Decimal travelDistance = calculateDistance(event.geoLocation, jobLocation);
                            if (previousEvent == null || previousEvent.distanceToJob == null) {
                                previousEvent = event;
                                previousEvent.distanceToJob = travelDistance;
                            }
                            else if (status == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED && event.finish == previousEvent.finish) {
                                previousEvent = event;
                                previousEvent.distanceToJob = travelDistance;
                                hasPreviousConfirmed = true;
                            }
                            else if ((travelDistance > previousEvent.distanceToJob && event.finish == previousEvent.finish) ||
                                    (event.finish > previousEvent.finish)){
                                previousEvent = event;
                                previousEvent.distanceToJob = travelDistance;
                            }
                        }


                    }
                    else {
                        previousEvent = event;
                    }


            }
            if (event.start >= slotEnd && event.geoLocation != null) {
                    if (event.objectType == 'jobAllocation') {
                        if (nextEvent == null || nextEvent.start > event.start ||
                        (event.finish == nextEvent.finish && hasNextConfirmed == false)) {
                            String status = ((skedModels.jobAllocation)event).status;
                            Decimal travelDistance = calculateDistance(event.geoLocation, jobLocation);
                            if (nextEvent == null || nextEvent.distanceToJob == null) {
                                nextEvent = event;
                                nextEvent.distanceToJob = travelDistance;
                            }
                            else if (status == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED && event.finish == nextEvent.finish) {
                                nextEvent = event;
                                nextEvent.distanceToJob = travelDistance;
                                hasNextConfirmed = true;
                            }
                            else if ((travelDistance > nextEvent.distanceToJob && event.finish == nextEvent.finish) ||
                                    (event.finish > nextEvent.finish)){
                                nextEvent = event;
                                nextEvent.distanceToJob = travelDistance;
                            }
                        }
                    }
                    else {
                        nextEvent = event;
                    }


            }
        }

        if (isAvai) {
            if (previousEvent != null && previousEvent.geoLocation != null) {
                Integer timeFromPreJobToCurrent = skedDateTimeUtils.getDifferenteMinutes(previousEvent.finish, slotStart);
                Decimal travelDistance = calculateDistance(previousEvent.geoLocation, jobLocation);
                Decimal travelTime = ((travelDistance / velocity) * 60).intValue();
                if (travelTime > timeFromPreJobToCurrent || travelDistance > res.maximumTravelRadius) {

                    isAvai = false;
                }
            }

            if (nextEvent != null && nextEvent.geoLocation != null && isAvai) {
                Integer timeFromCurrentJobToNextJob = skedDateTimeUtils.getDifferenteMinutes(slotEnd, nextEvent.start);
                Decimal travelDistance = calculateDistance(nextEvent.geoLocation, jobLocation);
                Decimal travelTime = ((travelDistance / velocity) * 60).intValue();
                if (travelTime > timeFromCurrentJobToNextJob || travelDistance > res.maximumTravelRadius) {
                    isAvai = false;
                }
            }
        }

        return isAvai;
    }

    public static skedVivintModel.TimeSlot checkAvaiResOfTimeSlot(skedVivintModel.TimeSlotCheckingModel checkingData) {
        skedVivintModel.TimeSlot slot = checkingData.checkingSlot;

        //check resource to get available resource of each timeslot
        for (skedResourceAvailabilityBase.resourceModel baseRes : checkingData.mapResource.values()) {
            boolean isAvai = true;
            skedResourceAvailabilityBase.resourceModel res = baseRes.deepclone();

            if (res.mapDateslot.containsKey(checkingData.dateString)) {
                skedResourceAvailabilityBase.dateslotModel daySlotModel =  res.mapDateslot.get(checkingData.dateString);
                isAvai = skedCommonservice.checkResourceWithTimePeriod(res, daySlotModel,
                                                                        checkingData.ignoreTravelTimeFirstJob, slot.slotStart, slot.slotEnd, slot);
                //check travel time of resource
                if (isAvai) {
                    isAvai = checkResourceMaxTravelRadius(res, slot.jobLocation);

                    if (isAvai) {
                        isAvai = checkTravelTimeOfResourceStraightLine(slot.jobLocation, slot.slotStart, slot.slotEnd, daySlotModel.events,
                                                                res, checkingData.velocity);

                    }
                }

                if (isAvai) {
                    slot.lstRes.add(res);
                }
            }
        }

        if (!slot.lstRes.isEmpty()) {
            slot.isAvai = true;
        }

        return slot;
    }

    public static skedVivintModel.DaySlot checkTimeSlotOfDateSlot(skedVivintModel.TimeSlotCheckingModel checkingData) {
        Date currentDate = System.today();
        Date checkingDate = skedDateTimeUtils.getDate(checkingData.checkingDateTime, checkingData.timezone);
        Integer bookableDays = currentDate.daysBetween(checkingDate);
        DateTime currentTime = System.now();

        for (skedVivintModel.TimeSlot defaultSlot : checkingData.lstTimeSlots) {
            if ((defaultSlot.slotStartDate == null ||defaultSlot.slotStartDate <= checkingDate) &&
                (defaultSlot.slotEndDate == null ||defaultSlot.slotEndDate >= checkingDate)) {
                if (defaultSlot.weekDay.equalsIgnoreCase(checkingData.daySlot.weekDay)) {
                    defaultSlot.slotStart = skedDateTimeUtils.addMinutes(checkingData.checkingDateTime, defaultSlot.startMinutes, checkingData.timezone);
                    defaultSlot.slotEnd = skedDateTimeUtils.addMinutes(checkingData.checkingDateTime, defaultSlot.endMinutes, checkingData.timezone);

                    skedVivintModel.TimeSlot slot = defaultSlot.deepClone();
                    slot.isAvai = true;
                    slot.pendingJobPerSlot = 0;
                    slot.readyJobPerSlot = 0;

                    if (bookableDays > slot.bookableDateInAdvance || bookableDays < 0) {
                        slot.isAvai = false;
                    }

                    if (slot.slotStart < currentTime) {
                        slot.isAvai = false;
                    }

                    for (sked__Job__c skedJob : slot.listOfJobs) {
                        if (skedJob.sked__Start__c <= slot.slotEnd && skedJob.sked__Finish__c >= slot.slotStart) {
                            if (skedJob.sked__Job_Status__c.contains('Pending')) {
                                slot.pendingJobPerSlot++;
                            }
                            else {
                                slot.readyJobPerSlot++;
                            }
                        }
                    }

                    if (slot.pendingJobPerSlot >= slot.maxPendingJobPerSlot) {
                        slot.isAvai = false;
                    }

                    if (slot.readyJobPerSlot >= slot.maxReadyJobPerSlot) {
                        slot.isAvai = false;
                    }

                    if (slot.pendingJobPerSlot + slot.readyJobPerSlot > slot.maxPendingJobPerSlot) {
                        slot.isAvai = false;
                    }

                    if (slot.isAvai) {
                        if (skedConstants.SLOT_RESOURCE_TYPE.equalsIgnoreCase(slot.bookingType)) {
                            checkingData.checkingSlot = slot;
                            slot = checkAvaiResOfTimeSlot(checkingData);

                            if (slot.isAvai != null && slot.isAvai) {
                                checkingData.daySlot.isAvailable = true;
                            }
                        }
                        else {
                            if (slot.isAvai != null && slot.isAvai) {
                                checkingData.daySlot.isAvailable = true;
                            }
                        }
                    }

                    if (!checkingData.timeOfDate.isEmpty()) {
                        if (checkingData.timeOfDate.contains(slot.id)) {
                            checkingData.daySlot.lstTimeSlots.add(slot);
                        }
                    }
                    else {
                        checkingData.daySlot.lstTimeSlots.add(slot);
                    }
                }
            }

        }

        return checkingData.daySlot;
    }

    //****************************************************Private Controller******************************//
    private static sked__Job__c createJob(skedVivintModel.VivintJob jobModel, string timezone,
                                            skedVivintModel.VivintContact contactInfo) {
        Date selectedDate = Date.valueOf(jobModel.jobDate);
        DateTime jobDate = DateTime.newInstance(selectedDate, Time.newInstance(0, 0, 0, 0));
        jobDate = skedDateTimeUtils.switchTimezone(jobDate,UserInfo.getTimeZone().getId(), timezone);
        integer startMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(jobModel.jobStart);
        integer finishMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(jobModel.jobFinish);
        DateTime jobStart = skedDateTimeUtils.addMinutes(jobDate, startMinutes, timezone);
        DateTime jobFinish = skedDateTimeUtils.addMinutes(jobDate, finishMinutes, timezone);
        integer jobDuration = finishMinutes - startMinutes;
        sked__Job__c skedJob = new sked__Job__c(
            sked__region__c = jobModel.regionId,
            sked__start__c = jobStart,
            sked__finish__c = jobFinish,
            sked__duration__c = jobDuration,
            sked_Slot__c = jobModel.slotId,
            sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_DISPATCH,
            sked__Address__c = jobModel.address,
            sked__GeoLocation__latitude__s = contactInfo.location.geoLocation.lat,
            sked__GeoLocation__longitude__s = contactInfo.location.geoLocation.lng,
            sked__Type__c = skedConstants.JOB_TYPE_DEFAULT,
            sked__Description__c = 'For ' + contactInfo.firstname + ' ' + contactInfo.lastname,
            sked_Setter_Location__c = contactInfo.location.id,
            sked__Contact__c = contactInfo.id,
            sked__Account__c = contactInfo.accountId,
            ssked_Prospect_Additional_information__c = contactInfo.additionalInformation,
            sked_Job_Start_Minutes__c = startMinutes,
            sked_Job_End_Minutes__c = finishMinutes,
            sked_Area_Interested_In_Smart_Home__c = contactInfo.areaInterestedIn != null ? string.join(contactInfo.areaInterestedIn,'; ') : null,
            //sked_accepts_Vivint_Policy__c = contactInfo.acceptPolicy != null ? contactInfo.acceptPolicy : false,
            sked_Prospect_Address__c = jobModel.address,
            sked_Customer_Email__c = contactInfo.email,
            sked_Prospect_has_speed_internet__c = contactInfo.haveHighSpeedInternet,
            sked_Prospect_interested_in_Smart_Home__c = contactInfo.howInterestedInSH,
            sked_Prospect_Name__c = contactInfo.firstname + ' ' + contactInfo.lastname,
            sked_Prospect_owns_Home_or_Business__c = contactInfo.ownHomeOrBusiness,
            sked_Customer_Phone__c = contactInfo.phone,
            sked_Prospect_Rep_ID__c = contactInfo.repId,
            sked_Do_you_have_security_system__c = contactInfo.haveSecuritySystem,
            sked_How_long_have_you_had_your_system__c = contactInfo.timeHaveSystem,
            sked_Is_your_system_being_monitored__c = contactInfo.systemMonitored,
            sked_Setter_Name__c = contactInfo.setterName,
            sked_Will_a_decision_maker_be_present__c = contactInfo.decisionMaking,
            sked_Prospect_First_Name__c = contactInfo.firstname,
            sked_Prospect_Last_Name__c = contactInfo.lastname,
            sked_Vivint_Account_Number__c = contactInfo.accountNumber
        );


        return skedJob;
    }

    public static sked__Job_Allocation__c createJobAllocation(string jobId, string resourceId) {
        sked__Job_Allocation__c ja = new sked__Job_Allocation__c (
            sked__resource__c = resourceId,
            sked__Job__c = jobId,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
        );

        return ja;
    }

    private static Contact createContact(skedVivintModel.VivintContact cont) {
        Account newAcc = new Account(
            Name = cont.firstname + ' ' + cont.lastname,
            ShippingStreet = cont.address,
            Phone = cont.phone
        );

        insert newAcc;

        cont.accountId = newAcc.id;

        Contact ct = new Contact(
            AccountId = newAcc.id,
            FirstName = cont.firstname,
            LastName = cont.LastName,
            Email = cont.email,
            //Phone = cont.phone,
            MobilePhone = cont.phone
        );
        return ct;
    }

    public static skedResourceAvailabilityBase.resourceModel getSuitableResource(skedModels.geometry destination,
                                                            List<skedResourceAvailabilityBase.resourceModel> lstRes,
                                                            Integer velocity) {
        skedResourceAvailabilityBase.resourceModel suitableRes;
        List<skedModels.geometry> origins = new List<skedModels.geometry>();
        List<skedModels.geometry> destinations = new List<skedModels.geometry>{destination};

        lstRes.sort();
        for (skedResourceAvailabilityBase.resourceModel res : lstRes) {
            origins.add(res.address.geometry);
        }

        if (!origins.isEmpty() && !destinations.isEmpty()) {
            for (skedResourceAvailabilityBase.resourceModel res : lstRes) {
                    Location origin = Location.newInstance(res.address.geometry.lat, res.address.geometry.lng);
                    Location target = Location.newInstance(destination.lat, destination.lng);
                    res.distanceToJob = calculateDistance(origin, target);
                    res.distanceToJob = res.distanceToJob == null ? 0 : res.distanceToJob;

                if (suitableRes == null) {
                    suitableRes = res;
                }
                else {
                    if (suitableRes.noOfJobsInSlot >= res.noOfJobsInSlot) {
                        if (suitableRes.noOfJobsInSlot == res.noOfJobsInSlot) {
                            if (suitableRes.distanceToJob > res.distanceToJob) {
                                suitableRes = res.deepClone();
                                suitableRes.noOfJobsInSlot = res.noOfJobsInSlot;
                            }
                        }
                        else {
                            suitableRes = res.deepClone();
                            suitableRes.noOfJobsInSlot = res.noOfJobsInSlot;
                        }
                    }
                }
            }
        }

        return suitableRes;
    }

    public static decimal calculateDistance(Location origin, Location target) {
        return Location.getDistance(origin, target, 'mi');
    }

    public static List<sked__Region_Area__c> getZipCodesFromName(String zipCode) {
        return new List<sked__Region_Area__c>([SELECT id, sked_Zip_Code_Name__c, sked__Region__c,
                                                sked__Region__r.sked__Timezone__c
                                                FROM sked__Region_Area__c
                                                WHERE sked_Zip_Code_Name__c =: zipCode
                                                AND sked__Region__c != null]);
    }


}
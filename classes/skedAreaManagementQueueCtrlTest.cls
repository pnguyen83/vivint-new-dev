@isTest
public with sharing class skedAreaManagementQueueCtrlTest {
	public static testmethod void getAllJobsTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        System.debug('job ' + job);
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.searchText = 'JOB';
        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};
        filter.closers = new List<String>();
        System.debug('filter ' + filter);
        skedRemoteResultModel result = skedAreaManagementQueueController.getAllJobs(filter);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void getAllJobsTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__region__c region = (sked__region__c)objFactory.getSObjects(sked__region__c.sObjectType).get(0);
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        System.debug('job ' + job);
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        filter.regions = new List<String>{region.id};
        filter.getMyJobs = true;
        filter.startDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.endDate = System.now().addDays(6).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());

        filter.status = new List<String>{'Pending Allocation', 'Allocated', 'Dispatched' , 'Ready', 'In Progress' , 'Complete', 'Cancelled', 'Pending Dispatch'};

        System.debug('filter ' + filter);
        skedRemoteResultModel result = skedAreaManagementQueueController.getAllJobs(filter);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void getConfigDataTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedRemoteResultModel result = skedAreaManagementQueueController.getConfigData();
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void getConfigDataTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Queue_Management_Access__c regionManager = (sked_Queue_Management_Access__c)objFactory.getSObjects(sked_Queue_Management_Access__c.sObjectType).get(0);
        delete regionManager;
        skedRemoteResultModel result = skedAreaManagementQueueController.getConfigData();
        System.debug('result ' + result);
        test.stopTest();
    }

    public static testmethod void cancelJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.VivintJob data = new skedVivintModel.VivintJob();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        data.Id = job.id;
        data.reason = 'Canceled: On-Site';

        skedRemoteResultModel result = skedAreaManagementQueueController.cancelJob(data);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void reassignJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.JobAvaiModel filter = new skedVivintModel.JobAvaiModel();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);

        filter.jobId = job.id;
        filter.regions = new List<String>{region.id};
        filter.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());

        skedRemoteResultModel result = skedAreaManagementQueueController.reassignJob(filter);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void confirmAssignmentTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        User u = skedSetterControllerTest.createUser();
        skedVivintModel.Assignment data = new skedVivintModel.Assignment();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Resource__c res = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = res.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;

        data.jobId = job.id;
        data.resourceIDs = new List<String>{res.id, resource2.id};

        skedRemoteResultModel result = skedAreaManagementQueueController.confirmAssignment(data);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void getAllResourcesTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        filter.regions = new List<String>{region.id};

        skedRemoteResultModel result = skedAreaManagementQueueController.getAllResources(filter);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void getResourceAndJobsInRegionsTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        filter.regions = new List<String>{region.id};
        filter.resourceList = new List<skedModels.resource>();
        String day1 = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        String day2 = System.now().addDays(1).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        String day3 = System.now().addDays(2).format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.dayList = new List<String>{day1, day2, day3};
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        skedModels.resource res = new skedModels.resource(resource);
        filter.resourceList.add(res);

        skedRemoteResultModel result = skedAreaManagementQueueController.getResourceAndJobsInRegions(filter);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void getResourcesForRescheduleTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.AreaManagementFilter filter = new skedVivintModel.AreaManagementFilter();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        filter.regions = new List<String>{region.id};
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Job_Allocation__c ja = (sked__Job_Allocation__c)objFactory.getSObjects(sked__Job_Allocation__c.sObjectType).get(0);
        String timezone = UserInfo.getTimeZone().getId();

        filter.selectedJob = new skedVivintModel.VivintJA(job, ja);
        String day1 = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        filter.dayList = new List<String>{day1};

        skedRemoteResultModel result = skedAreaManagementQueueController.getResourcesForReschedule(filter);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void rescheduleJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.RescheduleJob data = new skedVivintModel.RescheduleJob();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        data.jobId = job.id;
        data.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, UserInfo.getTimeZone().getId());
        data.startTime = 800;
        data.endTime = 1100;

        skedRemoteResultModel result = skedAreaManagementQueueController.rescheduleJob(data);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void confirmPendingDispatchJobTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.Appointment data = new skedVivintModel.Appointment();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        data.jobId = job.id;
        skedRemoteResultModel result = skedAreaManagementQueueController.confirmPendingDispatchJob(data);
        System.debug('result ' + result);

        test.stopTest();
    }

    public static testmethod void testData() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        System.debug('job ' + job);
        test.stopTest();
    }
}
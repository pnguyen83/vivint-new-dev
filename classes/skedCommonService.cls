global class skedCommonService {
    public static List<skedModels.holiday> getHolidays() {
        List<skedModels.holiday> results = new List<skedModels.holiday>();
        List<sked__Holiday__c> skedGlobalHolidays = [SELECT Id, Name, sked__Start_Date__c, sked__End_Date__c
                                                     FROM sked__Holiday__c
                                                     WHERE sked__Global__c = TRUE];
        for (sked__Holiday__c skedGlobalHoliday : skedGlobalHolidays) {
            skedModels.holiday model = new skedModels.holiday();
            model.id = skedGlobalHoliday.Id;
            model.name = skedGlobalHoliday.Name;
            model.startDate = Json.serialize(skedGlobalHoliday.sked__Start_Date__c).replace('"', '');
            model.endDate = Json.serialize(skedGlobalHoliday.sked__End_Date__c).replace('"', '');
            model.isGlobal = true;
            results.add(model);
        }
        List<sked__Holiday_Region__c> skedRegionHolidays = [SELECT Id, sked__Holiday__c, sked__Holiday__r.Name, sked__Holiday__r.sked__Start_Date__c, sked__Holiday__r.sked__End_Date__c,
                                                            sked__Region__c, sked__Region__r.Name
                                                            FROM sked__Holiday_Region__c];
        for (sked__Holiday_Region__c skedRegionHoliday : skedRegionHolidays) {
            skedModels.holiday model = new skedModels.holiday();
            model.id = skedRegionHoliday.sked__Holiday__c;
            model.name = skedRegionHoliday.sked__Holiday__r.Name;
            model.startDate = Json.serialize(skedRegionHoliday.sked__Holiday__r.sked__Start_Date__c).replace('"', '');
            model.endDate = Json.serialize(skedRegionHoliday.sked__Holiday__r.sked__End_Date__c).replace('"', '');
            model.isGlobal = false;
            model.regionId = skedRegionHoliday.sked__Region__c;
            results.add(model);
        }
        return results;
    }

    public static Map<string, Set<Date>> getMapHolidays() {
        Map<string, Set<Date>> mapHolidays = new Map<string, Set<Date>>();
        Date currentDate = system.now().date().addDays(-1);//buffer for different timezone

        List<sked__Holiday__c> skedGlobalHolidays = [SELECT Id, sked__Start_Date__c, sked__End_Date__c
                                                     FROM sked__Holiday__c
                                                     WHERE sked__Global__c = TRUE
                                                     AND sked__End_Date__c >= :currentDate];
        List<sked__Holiday_Region__c> skedRegionHolidays = [SELECT Id, sked__Holiday__r.sked__Start_Date__c, sked__Holiday__r.sked__End_Date__c,
                                                            sked__Region__c, sked__Region__r.Name
                                                            FROM sked__Holiday_Region__c
                                                            WHERE sked__Holiday__r.sked__End_Date__c >= :currentDate];

        Set<Date> globalHolidays = new Set<Date>();
        for (sked__Holiday__c globalHoliday : skedGlobalHolidays) {
            Date tempDate = globalHoliday.sked__Start_Date__c;
            while (tempDate <= globalHoliday.sked__End_Date__c) {
                globalHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }
        }
        mapHolidays.put(skedConstants.HOLIDAY_GLOBAL, globalHolidays);

        for (sked__Holiday_Region__c regionHoliday : skedRegionHolidays) {
            Set<Date> regionHolidays;
            if (mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                regionHolidays = mapHolidays.get(regionHoliday.sked__Region__r.Name);
            } else {
                regionHolidays = new Set<Date>();
            }

            Date tempDate = regionHoliday.sked__Holiday__r.sked__Start_Date__c;
            while (tempDate <= regionHoliday.sked__Holiday__r.sked__End_Date__c) {
                regionHolidays.add(tempDate);
                tempDate = tempDate.addDays(1);
            }

            if (!mapHolidays.containsKey(regionHoliday.sked__Region__r.Name)) {
                mapHolidays.put(regionHoliday.sked__Region__c, regionHolidays);
            }
        }
        return mapHolidays;
    }

    public static Set<id> getResourceIdsInRegion(string regionId) {
        Set<id> setResIds = new Set<Id>();

        List<sked__Resource__c> skedAvailableResources = [SELECT Id
                                                           FROM sked__Resource__c
                                                           WHERE sked__Is_Active__c = true
                                                           AND sked__Primary_Region__c =:regionId];

        for (sked__Resource__c skedResource : skedAvailableResources) {
            setResIds.add(skedResource.id);
        }

        return setResIds;
    }

    public static List<skedModels.resource> getResourcesInRegion(string regionId) {
        Map<String, List<skedModels.resource>> map_regionId_resource = getResourcesInRegions(new List<String>{regionId});
        return map_regionId_resource.get(regionId);
    }

    public static List<skedModels.resource> getResourcesInRegions(Set<string> regionIds) {
        Map<String, List<skedModels.resource>> map_regionId_resource = getResourcesInRegions(new List<String>(regionIds));
        List<skedModels.resource> result = new List<skedModels.resource>();

        for (String regionid : regionIds) {
            result.addAll(map_regionId_resource.get(regionId));
        }

        return result;
    }

    public static Map<String, List<skedModels.resource>> getResourcesInRegions(List<String> regionIds) {
        Map<String, List<skedModels.resource>> map_regionId_resource = new Map<String, List<skedModels.resource>>();

        List<sked__Resource__c> skedAvailableResources = [SELECT Id, Name, sked__Primary_Region__c,sked__User__r.FullPhotoUrl,
                                                                sked__User__r.SmallPhotoUrl, sked__Primary_Region__r.Name,
                                                                sked__User__c
                                                           FROM sked__Resource__c
                                                           WHERE sked__Is_Active__c = true
                                                           AND sked__Primary_Region__c IN :regionIds];

        for (sked__Resource__c skedRes : skedAvailableResources) {
            List<skedModels.resource> resources = map_regionId_resource.get(skedRes.sked__Primary_Region__c);

            if (resources == null) {
                resources = new List<skedModels.resource>();
            }
            skedModels.resource res = new skedModels.resource(skedRes);
            res.regionId = skedRes.sked__Primary_Region__c;
            res.regionName = skedRes.sked__Primary_Region__r.Name;
            res.photoUrl = skedRes.sked__User__r.SmallPhotoUrl;

            resources.add(res);

            map_regionId_resource.put(skedRes.sked__Primary_Region__c, resources);
        }

        return map_regionId_resource;
    }

    public static skedModels.resource getResourceFromUserId(String userId) {
        List<sked__Resource__c> skedResources = [SELECT Id, Name, sked__Primary_Region__c
                                                    FROM sked__Resource__c
                                                    WHERE sked__user__c =:userId
                                                    AND sked__Is_Active__c = true];

        if (skedResources == null || skedResources.isEmpty()) {
            throw new skedException(skedConstants.USER_NOT_ASSOCIATED_WITH_RES);
        }

        skedModels.resource res = new skedModels.resource(skedResources.get(0));
        res.regionId = skedResources.get(0).sked__Primary_Region__c;
        return res;
    }

    public static List<skedModels.selectOption> searchLocation(String name) {
        List<skedModels.selectOption> lstLocations = new List<skedModels.selectOption>();
        String searchStr = '%' + name.replace(' ', '%') + '%';
        for (sked_Setter_Location__c loc : [SELECT Id, Name
                                        FROM sked_Setter_Location__c
                                        WHERE Name LIKE :searchStr]) {
            skedModels.selectOption locModel = new skedModels.selectOption(loc.id, loc.name);
            lstLocations.add(locModel);
        }

        return lstLocations;
    }

    public static List<skedModels.selectOption> getPickListValues(string objectApiName, string fieldApiName) {
        List<string> picklistValues = new List<string>();

        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(objectApiName);
        DescribeSObjectResult objDescribe = targetType.getDescribe();
        map<String, SObjectField> mapFields = objDescribe.fields.getmap();
        SObjectField fieldType = mapFields.get(fieldApiName);
        DescribeFieldResult fieldResult = fieldType.getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry f : ple) {
            picklistValues.add(f.getValue());
        }
        picklistValues.sort();

        List<skedModels.selectOption> result = new List<skedModels.selectOption>();
        for (string picklistValue : picklistValues) {
            result.add(new skedModels.selectOption(picklistValue, picklistValue));
        }
        return result;
    }

    public static List<skedModels.region> getListRegions() {
        List<skedModels.region> regions = new List<skedModels.region>();

        for (sked__Region__c skedRegion : [SELECT id, Name, sked__Timezone__c
                                            FROM sked__Region__c
                                            ]) {
            skedModels.region region = new skedModels.region(skedRegion);
            regions.add(region);
        }

        return regions;
    }

    public static String getRegionIdFromTimezone(String timezone) {
        List<sked__Region__c> skedRegions = [SELECT Id FROM sked__Region__c WHERE sked__Timezone__c =:timezone];

        if (skedRegions == null || skedRegions.isEmpty()) {
            throw new skedException('There is not any region associated with user timezone');
        }

        return skedRegions.get(0).Id;
    }

    public static Map<Id, skedResourceAvailabilityBase.resourceModel> getAvaiResourcesOfJob(skedVivintModel.JobAvaiModel data) {
        List<sked__job__c> skedJobs = [SELECT Id, sked__Region__c, sked__Timezone__c, sked__Start__c, sked__Finish__c,
                                            sked__GeoLocation__latitude__s, sked__GeoLocation__longitude__s,
                                            (SELECT id, sked__Resource__c
                                                FROM sked__Job_Allocations__r
                                                WHERE sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DECLINED
                                                AND sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED)
                                        FROM sked__Job__c
                                        WHERE id =: data.jobId];

        if (skedJobs == null || skedJobs.isEmpty()) {
            throw new skedException(skedConstants.INVALID_JOB_ID);
        }

        data.job = skedJobs.get(0);

        if (data.regions == null) {
            data.regions = new List<String>{data.job.sked__Region__c};
        }
        data.timezone = data.job.sked__Timezone__c;

        DateTime startOfJobDate = skedDateTimeUtils.getStartOfDate(data.job.sked__Start__c, data.timezone);
        Integer jobStartMinutes = skedDateTimeUtils.getDifferenteMinutes(startOfJobDate, data.job.sked__Start__c);
        Integer jobEndMinutes = skedDateTimeUtils.getDifferenteMinutes(startOfJobDate, data.job.sked__Finish__c);

        List<sked__Resource__c> skedResources = [SELECT Id, sked__User__c, sked__Primary_Region__c
                                                    FROM sked__Resource__c
                                                    WHERE sked__Is_Active__c = true
                                                    AND sked__Primary_Region__c IN :data.regions
                                                ];

        if (skedResources == null || skedResources.isEmpty()) {
            if (data.fromCloserHandler) throw new skedException(skedConstants.USER_NOT_ASSOCIATED_WITH_RES);
            else throw new skedException(skedConstants.NO_ACTIVE_RESOURCE);
        }

        Set<Id> setResIds = new Set<id>();
        String currentUserId = UserInfo.getUserId();

        for (sked__Resource__c skedRes : skedResources) {
            if (data.fromCloserHandler != null && data.fromCloserHandler) {
                if (skedRes.sked__User__c == currentUserId) {
                    setResIds.add(skedRes.id);
                    break;
                }
            }
            else {
                if (data.regions != null && data.regions.contains(skedRes.sked__Primary_Region__c)) {
                    setResIds.add(skedRes.id);
                }
            }
        }

        if (data.startDate == null) {
            if (data.selectedDate != null) {
                data.startDate = data.selectedDate;
            }
            else {
                data.startDate = data.job.sked__Start__c.format(skedConstants.YYYY_MM_DD, data.timezone);
            }
        }

        if (data.endDate == null) {
            if (data.selectedDate != null) {
                data.endDate = data.selectedDate;
            }
            else {
                data.endDate = data.job.sked__Finish__c.format(skedConstants.YYYY_MM_DD, data.timezone);
            }
        }

        data.dateStart = Date.valueOf(data.startDate);
        data.dateEnd = Date.valueOf(data.endDate);

        Date tempDate = data.dateStart;
        set<Date> setInputDates = new Set<Date>();

        while (tempDate <= data.dateEnd) {
            setInputDates.add(tempDate);
            tempDate = tempDate.addDays(1);
        }

        skedAvailatorParams params = new skedAvailatorParams();
        params.timezoneSidId = data.timezone;
        params.startDate = data.dateStart;
        params.endDate = data.dateEnd;
        params.resourceIds = setResIds;
        params.inputDates = setInputDates;
        params.regionId = data.job.sked__Region__c;

        skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
        Map<Id, skedResourceAvailabilityBase.resourceModel> mapResources = resourceAvailability.initializeResourceList();

        return mapResources;
    }

    public static boolean checkResourceWithTimePeriod(skedResourceAvailabilityBase.resourceModel res, skedResourceAvailabilityBase.dateslotModel daySlotModel,
                                                        boolean ignoreTravelTimeFirstJob, DateTime slotStart, DateTime slotEnd,
                                                        skedVivintModel.TimeSlot slot) {
        boolean isAvai = true;
        res.allowMoreJob = false;
        Integer noOfPendingJob = 0;
        Integer noOfReadyJob = 0;
        Integer noOfJob = 0;

        //ignore travel time of first job of day
        if (ignoreTravelTimeFirstJob) {
            if (daySlotModel.firstJobOfDay == null || (daySlotModel.firstJobOfDay != null && daySlotModel.firstJobOfDay.start > slotStart) ||
                (daySlotModel.firstJobOfDay != null && daySlotModel.firstJobOfDay.start == slotStart)) {
                res.ignoreTravelTime = true;
            }
            else if (daySlotModel.firstJobOfDay != null && daySlotModel.firstJobOfDay.start < slotStart){
                res.ignoreTravelTime = false;
            }
        }

        for (skedModels.Event event : daySlotModel.events) {
            if (event.isAvailable == true && isAvai) {
                if (event.start < slotEnd && event.finish > slotStart) {
                    isAvai = false;
                }
            }
            else if (event.isAvailable == false && isAvai) {
                if (event.start < slotEnd  && event.finish > slotStart) {
                    if (slotStart == event.start && slotEnd == event.finish) {
                        if (event.objectType != 'jobAllocation') {
                            isAvai = false;
                            break;
                        }
                        else {
                            skedModels.joballocation ja = (skedModels.joballocation)event;
                            noOfJob++;
                            if (ja.status != null) {
                                if (ja.status.equalsIgnoreCase(skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH)) {
                                    noOfPendingJob++;
                                }
                                else {
                                    noOfReadyJob++;
                                }
                            }

                            if (slot != null &&
                                    (noOfReadyJob >= slot.maxReadyJobPerSlot || noOfPendingJob >= slot.maxPendingJobPerSlot ||
                                        (noOfReadyJob + noOfPendingJob) >= (slot.maxReadyJobPerSlot + slot.maxPendingJobPerSlot))) {
                                isAvai = false;
                                res.allowMoreJob = false;
                                break;
                            }
                            else {
                                res.allowMoreJob = true;
                            }

                            if (ja.relatedId != null && slot != null && slot.jobId != null) {
                                if (ja.relatedId == slot.jobId ) {
                                    slot.isSelected = true;
                                }
                                else {
                                    slot.isSelected = false;
                                }
                            }
                        }
                    }
                    else {
                        isAvai = false;
                    }

                }
            }
        }

        res.noOfJobsInSlot = noOfJob;

        return isAvai;
    }

    public static boolean validateEmail(String email) {
        Boolean res = true;
         if (String.isBlank(email)) {
            return false;
         }
        String emailRegex = '^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$';
        Pattern MyPattern = Pattern.compile(emailRegex);
        Matcher MyMatcher = MyPattern.matcher(email);

        if (!MyMatcher.matches())
            res = false;
        return res;
    }

    public static Set<String> checkUserPermissionSetFromUserId() {
        Set<String> setManagerRegionIds = new Set<String>();
        String resourceId = skedCommonService.getResourceFromUserId(UserInfo.getUserId()).id;

        for (sked_Queue_Management_Access__c skedQMA : [SELECT id, sked_Region__c FROM sked_Queue_Management_Access__c
                                                        WHERE sked_Resource__c =: resourceId]) {
            setManagerRegionIds.add(skedQMA.sked_Region__c);
        }

        return setManagerRegionIds;
    }

    public static Set<String> getRegionIdFromUserId(skedVivintModel.AreaManagementFilter filter) {
        String resourceId = skedCommonService.getResourceFromUserId(UserInfo.getUserId()).id;
        Set<String> setManagerRegionIds = new Set<String>();
        Set<String> selectedRegionIds = new Set<String>();

        List<sked__Resource__c> skedResources = [SELECT id, sked__Primary_Region__c
                                                        FROM sked__Resource__c
                                                        WHERE Id = :resourceId];
        if (!skedResources.isEmpty()) {
            setManagerRegionIds.add(skedResources.get(0).sked__Primary_Region__c);
        }

        //query data from Queue Management Access to specified which user could view Area Manager Queue page
        for (sked_Queue_Management_Access__c skedQMA : [SELECT id, sked_Region__c FROM sked_Queue_Management_Access__c
                                                        WHERE sked_Resource__c =: resourceId]) {
            setManagerRegionIds.add(skedQMA.sked_Region__c);
        }

        if (filter != null && !filter.regions.isEmpty()) {
            for (String regionId : filter.regions) {
                if (setManagerRegionIds.contains(regionId)) {
                    selectedRegionIds.add(regionId);
                }
            }
        }
        else {
            selectedRegionIds.addAll(setManagerRegionIds);
        }

        return selectedRegionIds;
    }

    public static String getTimezoneFromUserId() {
        String resourceId = skedCommonService.getResourceFromUserId(UserInfo.getUserId()).id;
        List<sked__Resource__c> skedResources = [SELECT id, sked__Primary_Region__c, sked__Primary_Region__r.sked__Timezone__c
                                                    FROM sked__Resource__c
                                                    WHERE id = :resourceId];
        if (skedResources == null || skedResources.isEmpty()) {
            throw new skedException(skedConstants.USER_NOT_ASSOCIATED_WITH_RES);
        }

        return skedResources.get(0).sked__Primary_Region__r.sked__Timezone__c;
    }

    public static void initContactInfoFromJob(sked__Job__c skedJob, skedVivintModel.VivintContact ct) {
        ct.id = skedJob.sked__Contact__c;
        //ct.acceptPolicy = skedJob.sked_accepts_Vivint_Policy__c;
        ct.additionalInformation = skedJob.ssked_Prospect_Additional_information__c;
        ct.address = skedJob.sked_Prospect_Address__c;
        ct.haveHighSpeedInternet = skedJob.sked_Prospect_has_speed_internet__c;
        ct.ownHomeOrBusiness = skedJob.sked_Prospect_owns_Home_or_Business__c;
        ct.howInterestedInSH = skedJob.sked_Prospect_interested_in_Smart_Home__c;
        ct.repId = skedJob.sked_Prospect_Rep_ID__c;
        ct.accountNumber = skedJob.sked_Vivint_Account_Number__c != null ? skedJob.sked_Vivint_Account_Number__c : '';
        if (skedJob.sked_Area_Interested_In_Smart_Home__c != null) {
            ct.areaInterestedIn = skedJob.sked_Area_Interested_In_Smart_Home__c.split(';');
        }

        ct.firstname = skedJob.sked__Contact__r.firstname;
        ct.lastname = skedJob.sked__Contact__r.lastname;
        ct.name = skedJob.sked__Contact__r.firstname + ' ' + skedJob.sked__Contact__r.lastname;
        ct.email = skedJob.sked_Customer_Email__c;
        ct.phone = skedJob.sked_Customer_Phone__c;
        ct.setterName = skedJob.sked_Setter_Name__c;
        ct.locationName = skedJob.sked_Setter_Location__r.Name;
        ct.decisionMaking = skedJob.sked_Will_a_decision_maker_be_present__c;
        ct.haveSecuritySystem = skedJob.sked_Do_you_have_security_system__c;
        ct.timeHaveSystem = skedJob.sked_How_long_have_you_had_your_system__c;
        ct.systemMonitored = skedJob.sked_Is_your_system_being_monitored__c;
        if (skedJob.sked_Contact_Attempted_Time__c != null) {
            ct.attemptedTime = 'Last: ' + skedJob.sked_Contact_Attempted_Time__c.format(skedConstants.MM_DD_YY_HH_MM);
        }

    }

    public static skedVivintModel.VivintJA updateContactAttemptedJob(skedVivintModel.VivintJA ja,
                                                                        String timezone, DateTime attemptedTime) {
        if (attemptedTime != null) {
            ja.attemptedTime = 'Last: ' + attemptedTime.format(skedConstants.MM_DD_YY_HH_MM, timezone);
        }

        return ja;
    }

    public static List<skedVivintModel.TimeSlot> getListTimeSlot(String selectedDate, List<String> regionIds) {
        Date startDate = Date.valueOf(selectedDate);
        Date endDate = startDate.addDays(6);

        List<skedVivintModel.TimeSlot> timeslots = new List<skedVivintModel.TimeSlot>();

        timeslots = getListTimeSlotInDefinedTimeRange(startDate, endDate, regionIds);

        return timeslots;
    }

    public static List<skedVivintModel.TimeSlot> getListTimeSlotInListOfDates(List<String> dateStrings, List<String> regionIds) {
        Date startDate;
        Date endDate;
        List<skedVivintModel.TimeSlot> timeslots = new List<skedVivintModel.TimeSlot>();
        for (String dateString : dateStrings) {
            Date selectedDate = Date.valueOf(dateString);
            if (startDate == null || startDate > selectedDate) {
                startDate = selectedDate;
            }

            if (endDate == null || endDate < selectedDate) {
                endDate = selectedDate;
            }
        }

        if (startDate != null && endDate != null) {
            timeslots = getListTimeSlotInDefinedTimeRange(startDate, endDate, regionIds);
        }

        return timeslots;
    }

    public static List<skedVivintModel.TimeSlot> getListTimeSlotInDefinedTimeRange(Date startDate, Date endDate, List<String> regionIds) {
        List<skedVivintModel.TimeSlot> timeslots = new List<skedVivintModel.TimeSlot>();
        String job_status_cancelled = skedConstants.JOB_STATUS_CANCELLED;
        String job_Status_completed = skedConstants.JOB_STATUS_COMPLETE;

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType).filter('sked_Active_End_Date__c = null OR sked_Active_End_Date__c >= :startDate');
        selector.filter('sked_Active_Start_Date__c = null OR sked_Active_Start_Date__c <= :endDate');
        selector.filter('sked_Market__c IN:regionIds');
        selector.subQuery('sked_Jobs__r').filter('sked__Job_Status__c !=: job_status_cancelled')
                                        .filter('sked__Job_Status__c !=: job_Status_completed');


        List<sked__Slot__c> slots = Database.query(selector.getQuery());

        for (sked__Slot__c slot : slots) {
            skedVivintModel.TimeSlot timeslot = new skedVivintModel.TimeSlot(slot);
            timeslots.add(timeslot);
        }

        return timeslots;
    }

    public static skedRemoteResultModel getTimeLabelOfDayForCloser(skedVivintModel.TimeSlotFilterModel filter) {
        List<skedVivintModel.Timeslot> slots = new List<skedVivintModel.Timeslot>();
        List<skedVivintModel.Timeslot> data = new List<skedVivintModel.Timeslot>();
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            filter.regionIds = new List<String>{getResourceFromUserId(UserInfo.getUserId()).regionId};

            slots = getTimeLabelOfDay(filter);

            for (skedVivintModel.Timeslot slot : slots) {
                data.add(slot);
            }

            result.data = data;
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getTimeLabelOfDayForSetter(skedVivintModel.TimeSlotFilterModel filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            List<sked__Region_Area__c> regionAreas = skedSetterHandler.getZipCodesFromName(filter.zipcode);

            if (regionAreas == null || regionAreas.isEmpty()) {
                result.errorMessage = skedConstants.ZIPCODE_NO_REGION;
                result.success = false;
                return result;
            }

            String regionId = regionAreas.get(0).sked__Region__c;
            filter.regionIds = new List<String>{regionId};
            List<skedVivintModel.Timeslot> data = new List<skedVivintModel.Timeslot>();
            data = getTimeLabelOfDay(filter);

            result.data = data;
        }
        catch (Exception ex) {
            result.getError(ex);
        }


        return result;
    }

    public static List<skedVivintModel.Timeslot> getTimeLabelOfDay(skedVivintModel.TimeSlotFilterModel filter) {
        List<skedVivintModel.Timeslot> data = new List<skedVivintModel.Timeslot>();

        List<skedVivintModel.TimeSlot> slots = getListTimeSlot(filter.selectedDate, filter.regionIds);
        DateTime selectedDateTime = DateTime.newInstance(Date.valueOf(filter.selectedDate), Time.newInstance(12, 0, 0, 0));
        String selectedWeekDay = selectedDateTime.format(skedConstants.WEEKDAY);

        for (skedVivintModel.TimeSlot slot : slots) {
            if (filter.onlySelectedDate == null || filter.onlySelectedDate == false) {
                data.add(slot);
            }
            else if (filter.onlySelectedDate){
                if (selectedWeekDay.equalsIgnoreCase(slot.weekDay)) {
                    data.add(slot);
                }
            }

        }

        return data;
    }

    @future(callout=true)
    public static void sendSmsNotificationForUnAllocatedJobs(String regionId, String jobId) {
        if (!test.isRunningTest()) {
            sendNotificationUnallocatedJobToManager(regionId, jobId);
        }
    }

    public static void sendNotificationUnallocatedJobToManager(String regionId, String jobId) {
        string smsContent = Skedulo_SMS__c.getOrgDefaults().sked_Unallocated_Job_Message__c;

        if (String.isNotBlank(smsContent)) {
            sked__Job__c skedJob = [SELECT id, Name, sked__Start__c, sked__Timezone__c FROM sked__Job__c WHERE id =:jobId];
            smsContent = smsContent.replace('[JOB_NUMBER]', skedJob.name);
            smsContent = smsContent.replace('[JOB_DATE]', skedJob.sked__Start__c.format(skedConstants.WEEKDAY_DAY_MONTH_YEAR, skedJob.sked__Timezone__c));
            smsContent = smsContent.replace('[JOB_TIME]', skedJob.sked__Start__c.format(skedConstants.HOUR_MINUTES_ONLY, skedJob.sked__Timezone__c));

            String token = getAPIToken();

            skedObjectSelector selector = skedObjectSelector.newInstance(sked_Queue_Management_Access__c.sObjectType);
            selector.filter('sked_Region__c =:regionID');

            List<sked_Queue_Management_Access__c> qmas = new List<sked_Queue_Management_Access__c>();
            qmas = Database.query(selector.getQuery());

            for (sked_Queue_Management_Access__c qma : qmas) {
                skedVivintModel.NotifyRequest request = new skedVivintModel.NotifyRequest(qma, smsContent, 'push');
                String jsonBody = JSON.serialize(request);
                skedSkeduloAPI.sendOneOffMessage(token, jsonBody);
            }
        }
    }

    public static String getAPIToken() {
        String token = '';
        List<Skedulo_API_Token__mdt> api = [SELECT sked_API_Token__c
                                FROM Skedulo_API_Token__mdt
                                WHERE DeveloperName = 'Skedulo_Long_Live_Token'
                                LIMIT 1];
        if (api != null && !api.isEmpty()) {
            token = api[0].sked_API_Token__c;
        }
        return token;
    }

    public static id getOrgWideEmail() {
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = :skedConstants.ORG_WIDE_EMAIL_ADDRESS];

        return owea.get(0).id;
    }
}
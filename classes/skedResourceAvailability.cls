global class skedResourceAvailability extends skedResourceAvailabilityBase {

    public skedResourceAvailability(skedAvailatorParams params) {
        super(params);
    }

    protected override void initializeResource(sked__Resource__c skedResource, resourceModel resource) {
        resource.geoLocation = skedResource.sked__GeoLocation__c;
        resource.address = new skedModels.address();
        resource.address.geometry = new skedModels.geometry(skedResource.sked__GeoLocation__c);
        resource.geoLocation = skedResource.sked__GeoLocation__c;
        resource.photoUrl = skedResource.sked__User__r.SmallPhotoUrl;
        resource.name = skedResource.name;
        resource.regionId = skedResource.sked__Primary_Region__c;
        resource.regionName = skedResource.sked__Primary_Region__r.name;
        resource.maximumTravelRadius = skedResource.sked_Maximum_Travel_radius__c != null ? skedResource.sked_Maximum_Travel_radius__c : 20;
        resource.primaryPhone = skedResource.sked__Primary_Phone__c;

        Set<Id> tagSet;
        if (this.params.isUsingSameTagSet) {
            tagSet = this.params.tagIds;
        }
        resource.loadResourceTags(skedResource.sked__ResourceTags__r, tagSet);


    }

    protected override void loadResourceEvents(sked__Resource__c skedResource, resourceModel resource) {
        for (sked__Availability__c availableBlock : skedResource.sked__Availabilities1__r) {
            if (availableBlock.sked__Is_Available__c == TRUE) {
	            DateTime availableStart = availableBlock.sked__Start__c;
	            DateTime availableEnd = availableBlock.sked__Finish__c;

	            for (dateslotModel dateslot : resource.mapDateslot.values()) {
	                if (dateslot.finish <= availableStart || dateslot.start >= availableEnd) {
	                    continue;
	                }
	                else {
	                    DateTime tempStart = availableStart < dateslot.start ? dateslot.start : availableStart;
	                    DateTime tempEnd = availableEnd > dateslot.finish ? dateslot.finish : availableEnd;
	                    dateslot.addEvent(availableStart, availableEnd, null, true);
	                }
	            }
	        }
        }
        for (sked__Availability__c unavailableBlock : skedResource.sked__Availabilities1__r) {
            if (unavailableBlock.sked__Is_Available__c == FALSE) {
	            DateTime unavailableStart = unavailableBlock.sked__Start__c;
	            DateTime unavailableEnd = unavailableBlock.sked__Finish__c;
	            for (dateslotModel dateslot : resource.mapDateslot.values()) {
	                if (dateslot.finish <= unavailableStart || dateslot.start >= unavailableEnd) {
	                    continue;
	                }
	                else {
	                    DateTime tempStart = unavailableStart < dateslot.start ? dateslot.start : unavailableStart;
	                    DateTime tempEnd = unavailableEnd > dateslot.finish ? dateslot.finish : unavailableEnd;
	                    skedModels.availability availability = new skedModels.availability();
	                    availability.objectType = 'availability';
	                    availability.id = unavailableBlock.Id;
	                    availability.start = tempStart;
	                    availability.finish = tempEnd;
                        availability.isAvailable = false;
	                    dateslot.events.add(availability);
	                }
	            }
	        }
        }

        for (sked__Job_Allocation__c skedAllocation : skedResource.sked__Job_Allocations__r) {
            string allocationDateString = skedAllocation.sked__Job__r.sked__Start__c.format(skedDateTimeUtils.DATE_ISO_FORMAT, this.params.timezoneSidId);

            if (resource.mapDateslot.containsKey(allocationDateString)) {
                dateslotModel dateslot = resource.mapDateslot.get(allocationDateString);
                skedModels.jobAllocation jobAllocation = new skedModels.jobAllocation();
                jobAllocation.objectType = 'jobAllocation';
                jobAllocation.id = skedAllocation.Id;
                jobAllocation.start = skedAllocation.sked__Job__r.sked__Start__c;
                jobAllocation.finish = skedAllocation.sked__Job__r.sked__Finish__c;
                jobAllocation.geolocation = skedAllocation.sked__Job__r.sked__GeoLocation__c;
                jobAllocation.isAvailable = false;
                jobAllocation.timezoneSidId = skedAllocation.sked__Job__r.sked__timezone__c;
                jobAllocation.status = skedAllocation.sked__Status__c;
                jobAllocation.relatedId = skedAllocation.sked__job__c;

                dateslot.events.add(jobAllocation);
                if (dateslot.firstJobOfDay == null) {
                    dateslot.firstJobOfDay = jobAllocation;
                }
                else {
                    if (dateslot.firstJobOfDay.start > jobAllocation.start) {
                        dateslot.firstJobOfDay = jobAllocation;
                    }
                }
            }
        }
        for (sked__Activity__c skedActivity : skedResource.sked__Activities__r) {
            string activityDateString = skedActivity.sked__Start__c.format(skedDateTimeUtils.DATE_ISO_FORMAT, this.params.timezoneSidId);
            if (resource.mapDateslot.containsKey(activityDateString)) {
                dateslotModel dateslot = resource.mapDateslot.get(activityDateString);
                skedModels.activity activity = new skedModels.activity();
                activity.objectType = 'activity';
                activity.id = skedActivity.Id;
                activity.isAvailable = false;
                activity.start = skedActivity.sked__Start__c;
                activity.finish = skedActivity.sked__End__c;
                activity.geolocation = skedActivity.sked__GeoLocation__c;
                activity.timezoneSidId = skedActivity.sked__timezone__c;
                dateslot.events.add(activity);
            }
        }

        for (dateslotModel dateslot : resource.mapDateslot.values()) {
            dateslot.events.sort();
        }
    }

}
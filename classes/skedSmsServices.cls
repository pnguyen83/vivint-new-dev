public with sharing class skedSmsServices {
	public static string US_COUNTRYCODE = '1';
	public static string FROM_NUMBER = '12134657605';

	public static void sendSMS(string jsonText){
		System.debug('sms6 jsonText ' + jsonText);
		Http http           = new Http();
		HttpRequest req     = new HttpRequest();
		HttpResponse res    = new HttpResponse();
		//Set end point to Authenciate
		string EndPoint = 'https://rest.nexmo.com/sms/json';
		req.setBody(jsonText);
		req.setEndpoint( EndPoint );
		req.setMethod('POST');
		req.setTimeout(10000);
		req.setHeader('Content-Type', 'application/json');
		integer responseCode = 0;
		string jsonResponse = '';
	    if (!test.isRunningTest()) { res = http.send(req);
	    	responseCode = res.getStatusCode();
	    	System.debug('sms6 responseCode = ' + responseCode);
		    System.debug('sms6 response = ' + res);
		    System.debug('sms6 response = ' + res.getBody());
	    }


	}

	private static string generatePhone(string mobileNumber){
		if(String.isBlank(mobileNumber)){
			return '';
		}
		mobileNumber = mobileNumber.replace(' ','');
		mobileNumber = mobileNumber.replace('(','');
		mobileNumber = mobileNumber.replace(')','');
		mobileNumber = mobileNumber.replace('-','');

        if(!Test.isRunningTest()){
        	if(mobileNumber.left(1) == '+') {
        		return mobileNumber.right(mobileNumber.length() - 1);
        	}
        }

		if(mobileNumber.left(1) == '0'){
			mobileNumber = mobileNumber.right(mobileNumber.length() - 1);
		}

		String countryCode = Skedulo_SMS__c.getOrgDefaults().Country_Code__c;

		countryCode = string.IsBlank(countryCode)?US_COUNTRYCODE:countryCode;

		return countryCode+mobileNumber;
	}

	public static void sendSMSToResourceFromJA(sked__Job_Allocation__c jall){
		System.debug('sms5');
		skedSMS sms = new skedSMS(jall.sked__Status__c);
		sms.tfrom = FROM_NUMBER;
		sms.to    = generatePhone(jall.sked__Resource__r.sked__Mobile_Phone__c);
		sms.text = skedSMS.generateSMS(sms.text, jall);
		string jsonformat = JSON.serialize(sms).replace('tfrom','from');
		sendSMS(jsonformat);
	}

	@Future(callout=true)
	public static void sendNotificationToResourceFromJA(Set<Id> jaIds) {
		if (!test.isRunningTest()) {
			sendSMSToResourceFromJA(jaIds);
		}
	}

	public static void sendSMSToResourceFromJA(Set<Id> jaIds) {
		System.debug('sms3');
		List<sked__Job_Allocation__c> errorSkedJAs = new List<sked__Job_Allocation__c>();

		for (sked__Job_Allocation__c skedJA : [SELECT id, sked__Status__c, sked__Resource__r.sked__Mobile_Phone__c,
														sked__Job__r.sked_Prospect_Name__c, sked__Job__r.sked__TimeZone__c,
														sked__Job__r.name, sked__Job__r.sked__Start__c
													FROM sked__Job_Allocation__c
													WHERE id IN : jaIds]) {
			try {
				System.debug('sms4');
				sendSMSToResourceFromJA(skedJA);
			}
			catch (Exception ex) {
				System.debug('sms7');
				skedJA.sked_API_Error__c = true;
				skedJA.sked_API_Error_Message__c = ex.getMessage();
				errorSkedJAs.add(skedJA);
			}
		}

		if (!errorSkedJAs.isEmpty()) {
			System.debug('sms8');
			update errorSkedJAs;
		}

	}
}
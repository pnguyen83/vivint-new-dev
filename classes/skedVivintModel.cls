global class skedVivintModel{
	global class VivintContact{
		public string id;
		public boolean acceptPolicy ;
		public string additionalInformation;
		public string address;
		public string haveHighSpeedInternet;
		public string ownHomeOrBusiness;
		public string howInterestedInSH;
		public skedModels.skedLocation location ;
		public string locationName;
		public string repId;
		public string accountNumber;
		public List<string> areaInterestedIn;
		public string firstname;
		public string name;
		public string dateOfBirth;
		public string email;
		public string phone;
		public string postalCode;
		public string setterName;
		public string decisionMaking;
		public string haveSecuritySystem;
		public string timeHaveSystem;
		public string systemMonitored;
		public string accountId;
		public string lastname;
		public string jobId;
		public string setInfo;
		public string demoResult;
		public string attemptedTime;

		public VivintContact() {

		}

		public VivintContact(sked__Job__c skedJob) {
			skedCommonService.initContactInfoFromJob(skedJob, this);
		}


	}

	global class SetterSettingData {
		public List<skedModels.selectOption> ownBusinessOptions ;
		public List<skedModels.selectOption> interestedLevelOptions ;
		public List<skedModels.selectOption> interestedAreaOptions ;
		public List<skedModels.selectOption> internetOptions ;
		public List<skedModels.selectOption> daysOfWeek ;
		public Map<String, List<TimeSlot>> weekday_timeslots;
		public List<skedModels.selectOption> abortReasons;
		public List<TimeSlot> slotIntervals ;

		public SetterSettingData() {
			this.ownBusinessOptions = skedCommonService.getPickListValues('sked__Job__c', 'sked_Prospect_owns_Home_or_Business__c');
			this.interestedLevelOptions = skedCommonService.getPickListValues('sked__Job__c', 'sked_Prospect_interested_in_Smart_Home__c');
			this.interestedAreaOptions = skedCommonService.getPickListValues('sked__Job__c', 'sked_Area_Interested_In_Smart_Home__c');
			this.internetOptions = skedCommonService.getPickListValues('sked__Job__c', 'sked_Prospect_has_speed_internet__c');

			this.daysOfWeek = new List<skedModels.selectOption>();
			this.daysOfWeek.add(new skedModels.selectOption('Mon', 'Monday'));
			this.daysOfWeek.add(new skedModels.selectOption('Tue', 'Tuesday'));
			this.daysOfWeek.add(new skedModels.selectOption('Wed', 'Wednesday'));
			this.daysOfWeek.add(new skedModels.selectOption('Thu', 'Thursday'));
			this.daysOfWeek.add(new skedModels.selectOption('Fri', 'Friday'));
			this.daysOfWeek.add(new skedModels.selectOption('Sat', 'Saturday'));
			this.daysOfWeek.add(new skedModels.selectOption('Sun', 'Sunday'));

			this.abortReasons = skedCommonService.getPickListValues('sked__Job__c', 'sked__Abort_Reason__c');
			this.slotIntervals = skedSetting.instance.Admin.lstTimeSlot;
		}
	}

	global class SetterBookingData {
		public string selectedDate;
		public List<string> dateOfWeek;
		public List<string> timeOfDate;
		public string timezone;
		public string regionId;
		public skedModels.geometry jobLocation;
		public String postalCode;

		public SetterBookingData() {
			this.dateOfWeek = new List<String>();
			this.timeOfDate = new List<String>();
		}
	}

	global class Setter extends skedModels.modelBase{
		public boolean isActive ;
	}

	global class Appointment {
		public VivintContact contactInfo ;
		public VivintJob jobInfo ; //use in Setter Modal
		public List<skedResourceAvailabilityBase.resourceModel>  lstRes ; //user in Setter Modal
		public string selectedDate ;
		public String jobId; //Use in Closer Modal
		public boolean isCancel;
		public String reason;
		public string slotType;

		public Appointment() {
			this.lstRes = new List<skedResourceAvailabilityBase.resourceModel>();
			this.jobInfo = new VivintJob();
		}
	}

	global class VivintJob extends skedModels.job {
		public string jobDate ;
        public decimal jobStart ;
        public decimal jobFinish ;
        public Setter setter ;
        public string address ;
        public String reason;
        public String slotId;
	}

	global class VivintJA extends skedModels.jobAllocation {
		public string jobDate ;
		public string jobTime ;
		public string jobAddress ;
		public String jobName ;
		public String jobStatus;
		public String regionId;
		public skedModels.geometry geometry ;
		public skedResourceAvailabilityBase.resourceModel res ;
		public VivintContact jobContact ;
		public String color;
		public transient DateTime jobStart;
		public transient DateTime jobFinish;
		public transient Decimal jaCount;
		public String timezone;
		public String jobType;
		public String timeslotId;
		public String reason;
		public String attemptedTime;

		public Integer jobStartMinutes;
		public Integer jobEndMinutes;

		public VivintJA() {

		}

		public VivintJA(DateTime jobStart, DateTime jobEnd, string address,
							skedResourceAvailabilityBase.resourceModel resource, string timezone,
							skedModels.geometry jobLocation) {
			this.jobDate = jobStart.format(skedConstants.WEEKDAY_DAY_MONTH_YEAR, timezone);
			this.jobTime = jobStart.format(skedConstants.HOUR_MINUTES_ONLY, timezone) + ' - ' + jobEnd.format(skedConstants.HOUR_MINUTES_ONLY, timezone);
			this.jobAddress = address;
			this.res = resource;
			this.geometry = jobLocation;
		}

		public VivintJA (sked__Job_Allocation__c skedJA, String timezone) {
			this.id = skedJA.id;
			this.jobId = skedJA.sked__Job__c;
			this.timezone = timezone;
			this.regionId = skedJA.sked__Job__r.sked__Region__c;
			//DateTime startOfJobDate = skedDateTimeUtils.getStartOfDate(skedJA.sked__Job__r.sked__Start__c, timezone);
			this.jobStartMinutes = (Integer)skedJA.sked__Job__r.sked_Job_Start_Minutes__c;
			this.jobEndMinutes = (Integer)skedJA.sked__Job__r.sked_Job_End_Minutes__c;
			this.jobContact = new VivintContact();
			this.jobAddress = skedJA.sked__Job__r.sked__Address__c;
			this.jobDate = skedJA.sked__Job__r.sked__Start__c.format(skedConstants.YYYY_MM_DD, timezone);
			this.jobContact.name = skedJA.sked__Job__r.sked__Contact__r.FirstName + ' ' + skedJA.sked__Job__r.sked__Contact__r.LastName;
			this.jobContact.phone = skedJA.sked__Job__r.sked_Customer_Phone__c;
			this.jobContact.email = skedJA.sked__Job__r.sked_Customer_Email__c;
			if (skedJA.sked__Job__r.sked_Area_Interested_In_Smart_Home__c != null) this.jobContact.areaInterestedIn = skedJA.sked__Job__r.sked_Area_Interested_In_Smart_Home__c.split(';');
			this.jobName = skedJA.sked__Job__r.name;
			this.jobStatus = skedJA.sked__Job__r.sked__Job_Status__c;
			this.status = skedJA.sked__Status__c;

			this.jobTime = skedJA.sked__Job__r.sked__Start__c.format(skedConstants.MM_DD_YY_HH_MM_TIMEZONE, timezone);
			this.jobStart = skedJA.sked__Job__r.sked__Start__c;
			this.jobFinish = skedJA.sked__Job__r.sked__Finish__c;
			this.res = new skedResourceAvailabilityBase.resourceModel();
			this.res.id = skedJA.sked__Resource__c;
			this.res.name = skedJA.sked__Resource__r.Name;
			this.timeslotId = skedJA.sked__Job__r.sked_Slot__c;
		}

		public VivintJA (sked__Job__c skedJob, sked__Job_Allocation__c skedJA) {
			this.jobId = skedJob.id;
			this.timezone = skedJob.sked__Timezone__c != null ? skedJob.sked__Timezone__c : UserInfo.getTimeZone().getId();
			this.jobStartMinutes = (Integer)skedJob.sked_Job_Start_Minutes__c;
			this.jobEndMinutes = (Integer)skedJob.sked_Job_End_Minutes__c;
			this.jobContact = new VivintContact();
			this.jobAddress = skedJob.sked__Address__c;
			this.jobDate = skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, this.timezone);
			this.jobContact.name = skedJob.sked__Contact__r.FirstName + ' ' + skedJob.sked__Contact__r.LastName;
			this.jobContact.phone = skedJob.sked_Customer_Phone__c;
			this.jobContact.email = skedJob.sked_Customer_Email__c;
			if (skedJob.sked_Area_Interested_In_Smart_Home__c != null) {
				this.jobContact.areaInterestedIn = skedJob.sked_Area_Interested_In_Smart_Home__c.split(';');
			}
			this.jobName = skedJob.name;
			this.jobStatus = skedJob.sked__Job_Status__c;
			this.jobTime = skedJob.sked__Start__c.format(skedConstants.MM_DD_YY_HH_MM_TIMEZONE, this.timezone);
			this.jobStart = skedJob.sked__Start__c;
			if (skedJA != null) {
				this.res = new skedResourceAvailabilityBase.resourceModel();
				this.res.id = skedJA.sked__Resource__c;
				this.res.name = skedJA.sked__Resource__r.Name;
				this.id = skedJA.id;
				this.status = skedJA.sked__Status__c;
			}
			this.jobFinish = skedJob.sked__Finish__c;
	        this.timezone = skedJob.sked__Timezone__c;
	        this.regionId = skedJob.sked__Region__c;
	        this.geometry = new skedModels.geometry(skedJob.sked__GeoLocation__latitude__s,skedJob.sked__GeoLocation__longitude__s);
	        this.jobType = skedJob.sked__Type__c;
	        this.jobContact = new skedVivintModel.VivintContact(skedJob);
	        this.jobContact.setInfo = skedJob.Job_Created_Desc__c;
	        this.jobContact.demoResult = skedJob.sked_Demo_Result__c;
	        this.reason = skedJob.sked__Abort_Reason__c;
	        this.timeslotId = skedJob.sked_Slot__c;
		}

	}

	global class DaySlot {
		public string dayString ;
		public string dayLabel ;
		public List<TimeSlot> lstTimeSlots ;
		public integer noOfTimeslot ;
		public string weekday ;
		public boolean isAvailable;

		public Dayslot(DateTime selectedDateTime, String timezone) {
			this.dayLabel = selectedDateTime.format(skedConstants.WEEKDAY_DAY_MONTH, timezone);
			this.weekday = selectedDateTime.format(skedConstants.WEEKDAY, timezone);
			this.dayString = selectedDateTime.format(skedDateTimeUtils.DATE_ISO_FORMAT, timezone);
			this.lstTimeSlots = new List<TimeSlot>();
			this.isAvailable = false;
		}
	}

	global class TimeSlot implements Comparable{
		public string id ;
		public decimal startTime ;
		public decimal endTime ;
		public string name ;
		public string startLabel ;
		public string endLabel ;
		public boolean isAvai ;
		public integer startMinutes;
		public integer endMinutes;
		public Location jobLocation;
		public string dateString;
		public decimal jobsPerSlot;
		public boolean isSelected;
		public Date slotStartDate;
		public Date slotEndDate;
		public Decimal bookableDateInAdvance;
		public string bookingType;
		public string weekDay;
		public skedModels.region market;

		public transient Decimal readyJobPerSlot;
		public  Decimal pendingJobPerSlot;
		public transient Decimal unAllocatedJobPerSlot;
		public transient Decimal maxPendingJobPerSlot;
		public transient Decimal maxReadyJobPerSlot;
		public transient Decimal maxUnallocatedJobPerSlot;
		public transient String jobId;
		public transient List<sked__Job__c> listOfJobs;
		public transient DateTime slotStart;
		public transient DateTime slotEnd;

		public List<skedResourceAvailabilityBase.resourceModel> lstRes ;
		public List<VivintJA> jobAllocations ;

		public TimeSlot () {
			this.lstRes = new List<skedResourceAvailabilityBase.resourceModel>();
			this.isAvai = false;
			this.jobAllocations = new List<VivintJA>();
			this.isSelected = false;
			this.listOfJobs = new List<sked__Job__c>();
		}

		public TimeSlot(sked__Slot__c slot) {
			this.id = slot.id;
			this.name = slot.name;
			this.readyJobPerSlot = 0;
			this.pendingJobPerSlot = 0;
			this.unAllocatedJobPerSlot = 0;

			this.listOfJobs = new List<sked__Job__c>();
			this.jobAllocations = new List<VivintJA>();
			this.lstRes = new List<skedResourceAvailabilityBase.resourceModel>();
			this.slotEndDate = slot.sked_Active_End_Date__c;
			this.slotStartDate = slot.sked_Active_Start_Date__c;
			this.bookableDateInAdvance = slot.sked_Bookable_days_in_advance__c;
			this.bookingType = slot.sked_Booking_Type__c;
			this.weekDay = slot.sked_Day__c;
			this.startTime = slot.sked_Start_Time__c;
			this.endTime = slot.sked_End_Time__c;
			this.market = new skedModels.region(slot.sked_Market__r);
			this.maxPendingJobPerSlot = slot.sked_Number_of_Parallel_Bookings_Pending__c;
			this.maxReadyJobPerSlot = slot.sked_Number_of_Parallel_bookings_Ready__c;
			this.maxUnallocatedJobPerSlot = slot.sked_Number_of_Parallel_Bookings_Pending__c;
			this.bookingType = slot.sked_Booking_Type__c;

			this.startMinutes = slot.sked_Start_Time__c != null ? skedDateTimeUtils.convertTimeNumberToMinutes(slot.sked_Start_Time__c) : null;
			this.endMinutes = slot.sked_End_Time__c != null ? skedDateTimeUtils.convertTimeNumberToMinutes(slot.sked_End_Time__c) : null;
			if (slot.sked_Number_of_Parallel_Bookings_Pending__c != null && slot.sked_Number_of_Parallel_bookings_Ready__c == null) {
				this.jobsPerSlot = slot.sked_Number_of_Parallel_Bookings_Pending__c;
			}

			if (slot.sked_Number_of_Parallel_Bookings_Pending__c == null && slot.sked_Number_of_Parallel_bookings_Ready__c != null) {
				this.jobsPerSlot = slot.sked_Number_of_Parallel_bookings_Ready__c;
			}

			if (slot.sked_Number_of_Parallel_Bookings_Pending__c != null && slot.sked_Number_of_Parallel_bookings_Ready__c != null) {
				this.jobsPerSlot = slot.sked_Number_of_Parallel_bookings_Ready__c + slot.sked_Number_of_Parallel_Bookings_Pending__c;
			}

			if (slot.sked_Jobs__r != null && !slot.sked_Jobs__r.isEmpty()) {
				for (sked__Job__c skedJob : slot.sked_Jobs__r) {
					this.listOfJobs.add(skedJob);
				}
			}

		}

		public TimeSlot deepClone() {
			TimeSlot newSlot = new TimeSlot();
			this.jobAllocations = new List<VivintJA>();
			newSlot.lstRes = new List<skedResourceAvailabilityBase.resourceModel>();
			newSlot.id = this.id;
			newSlot.name = this.name;
			newSlot.startTime = this.startTime;
			newSlot.endTime = this.endTime;
			newSlot.isAvai = this.isAvai;
			newSlot.startMinutes = this.startMinutes;
			newSlot.endMinutes = this.endMinutes;
			newSlot.startLabel = this.startLabel;
			newSlot.endLabel = this.endLabel;
			newSlot.jobLocation = this.jobLocation;
			newSlot.jobId = this.jobId != null ? this.jobId : null;
			newSlot.bookingType = this.bookingType;
			newSlot.maxReadyJobPerSlot = this.maxReadyJobPerSlot;
			newSlot.maxPendingJobPerSlot = this.maxPendingJobPerSlot;
			newSlot.maxUnallocatedJobPerSlot = this.maxUnallocatedJobPerSlot;
			newSlot.market = this.market;
			newSlot.weekDay = this.weekDay;
			newSlot.bookableDateInAdvance = this.bookableDateInAdvance;
			newSlot.isSelected = false;
			newSlot.slotStartDate = this.slotStartDate;
			newSlot.slotEndDate = this.slotEndDate;
			newSlot.jobsPerSlot = this.jobsPerSlot;
			newSlot.listOfJobs = new List<sked__Job__c>(this.listOfJobs);
			newSlot.readyJobPerSlot = this.readyJobPerSlot;
			newSlot.pendingJobPerSlot = this.pendingJobPerSlot;
			newSlot.unAllocatedJobPerSlot = this.unAllocatedJobPerSlot;
			newSlot.slotStart = this.slotStart;
			newSlot.slotEnd = this.slotEnd;

			return newSlot;
		}

		public virtual Integer compareTo(Object compareTo) {
            TimeSlot compareToRecord = (TimeSlot)compareTo;
            Integer returnValue = 0;

            if (startMinutes > compareToRecord.startMinutes) {
                returnValue = 1;
            }
            else if (startMinutes < compareToRecord.startMinutes) {
                returnValue = -1;
            }
            return returnValue;
        }
	}

	public class CloserConfigData {
		public List<TimeSlot> slotIntervals ;
		public Map<String, List<TimeSlot>> weekday_timeslots;
		public List<skedModels.selectOption> abortReasons;

		public CloserConfigData() {
			this.slotIntervals = skedSetting.instance.Admin.lstTimeSlot;
			Map<String, List<TimeSlot>> specifiedWeekDayTimeslot = skedSetting.instance.Admin.specifiedWeekDayTimeSlots;
			this.weekday_timeslots = new Map<String, List<TimeSlot>>();
			List<String> weekDays = new List<String>{'mon', 'tue', 'wed', 'thu', 'fri', 'sat'};

			for (String weekDayName : weekDays) {
				List<TimeSlot> dayTimeSlots = new List<TimeSlot>(this.slotIntervals);

				if (specifiedWeekDayTimeslot.containsKey(weekDayName)) {
					dayTimeSlots = new List<TimeSlot>(specifiedWeekDayTimeslot.get(weekDayName));
				}
				this.weekday_timeslots.put(weekDayName, dayTimeSlots);
			}

			this.abortReasons = skedCommonService.getPickListValues('sked__Job__c', 'sked__Abort_Reason__c');

		}
	}

	public class JobAvaiModel {
		public String startDate ;
		public String endDate ;
		public String jobId ;
		public String sortBy;
		public String timezone;
		public transient List<String> regions;
		public transient Date dateStart;
		public transient Date dateEnd;
		public transient sked__Job__c job;
		public transient boolean fromCloserHandler;
		public transient String selectedDate;
	}

	public class CloserData {
		public String resourceId;
		public String resourceName;
		public String startDate;
		public String endDate ;
		public String timezone ;
		public List<VivintJA> jobAllocations;
		private Map<id, VivintJA> mapJAs;

		public CloserData () {
			this.timezone = UserInfo.getTimeZone().getId();
			this.jobAllocations = new List<VivintJA>();
			this.mapJAs = new Map<id, VivintJA>();
		}

		public CloserData(sked__Resource__c skedRes, DateTime startTime, DateTime endTime, String timezone) {
			this.resourceId = skedRes.id;
			this.resourceName = skedRes.name;
			this.jobAllocations = new List<VivintJA>();
			this.mapJAs = new Map<id, VivintJA>();
			Date startDate = skedDateTimeUtils.getDate(startTime, timezone);
			Date endDate = skedDateTimeUtils.getDate(endTime, timezone);

			List<TimeSlot> timeSlots = skedCommonService.getListTimeSlotInDefinedTimeRange(startDate, endDate, new List<String>{skedRes.sked__Primary_Region__c});

			if (skedRes.sked__Job_Allocations__r != null) {
				for (TimeSlot slot : TimeSlots) {
					for (sked__Job_Allocation__c skedJA : skedRes.sked__Job_Allocations__r) {
						VivintJA ja = new VivintJA(skedJA, timezone);
						ja = skedCommonService.updateContactAttemptedJob(ja, timezone, skedJA.sked__Job__r.sked_Contact_Attempted_Time__c);
						if (ja.jobStartMinutes >= slot.startMinutes && ja.jobEndMinutes <= slot.endMinutes) {
							this.mapJAs.put(ja.id, ja);
							ja.geometry = new skedModels.geometry(skedJA.sked__Job__r.sked__GeoLocation__latitude__s, skedJA.sked__Job__r.sked__GeoLocation__longitude__s);
						}
					}
				}

				if (!this.mapJAs.isEmpty()) {
					this.jobAllocations = new List<VivintJA>(this.mapJAs.values());
				}
			}
		}
	}

	public class RescheduleJob {
		public String jobId;
		public String selectedDate;
		public Decimal startTime;
		public Decimal endTime;
		public String resourceId;
	}

	public class CloserFilterData {
		public String startDate;
		public String endDate;
		public String searchText;
		public skedModels.geometry jobLocation;
		public String postalCode;
	}

	public class AreaManagementFilter {
		public skedModels.region region;
		public List<String> regions;
		public String startDate;
		public String endDate;
		public List<String> closers;
		public List<String> status;
		public boolean getMyJobs;
		public String searchText;
		public List<String> dayList;
		public List<skedModels.resource> resourceList;
		public VivintJA selectedJob;
		public transient boolean isFromAppointmentQueue;
		public transient string timezone;
		public transient boolean fromJobTab;
		public transient DateTime startOfTime;
		public transient DateTime endOfTime;
	}

	public class AreaManagementConfig {
		public List<skedModels.region> regions;
		public List<skedModels.resource> closers;
		public List<skedModels.selectOption> status;
		public List<skedModels.selectOption> cancelReasons;
		public skedModels.resource currentUser;

		public AreaManagementConfig() {
			this.cancelReasons = skedCommonService.getPickListValues('sked__Job__c', 'sked__Abort_Reason__c');
			this.status = skedCommonService.getPickListValues('sked__Job__c', 'sked__Job_Status__c');

			List<skedModels.region> allRegions = skedCommonService.getListRegions();
			Set<String> regionIDs = skedCommonService.getRegionIdFromUserId(null);
			this.regions = new List<skedModels.region>();
			for (skedModels.region region : allRegions) {
				if (regionIDs.contains(region.id)) {
					this.regions.add(region);
				}
			}

			this.closers = skedCommonService.getResourcesInRegions(regionIDs);
			this.currentUser = skedCommonService.getResourceFromUserId(UserInfo.getUserId());
		}
	}

	public class AssignResourceData {
		public List<skedResourceAvailabilityBase.resourceModel> avaiResources;
		public List<skedResourceAvailabilityBase.resourceModel> unAvaiResources;
		public List<skedResourceAvailabilityBase.resourceModel> selectedResources;

		public AssignResourceData() {
			this.avaiResources = new List<skedResourceAvailabilityBase.resourceModel>();
			this.unAvaiResources = new List<skedResourceAvailabilityBase.resourceModel>();
			this.selectedResources = new List<skedResourceAvailabilityBase.resourceModel>();
		}
	}

	public class Assignment {
		public String jobId;
		public List<String> resourceIDs;
	}

	public class AreaManagerRescheduleData extends skedModels.modelBase{
		public skedResourceAvailabilityBase.resourceModel resource;
		public List<TimeSlot> timeSlots;

		public AreaManagerRescheduleData(skedModels.resource res) {
			this.timeSlots = new List<TimeSlot>();
			this.id = res.id;
			this.name = res.name;
		}

		public AreaManagerRescheduleData() {
			this.timeslots = new List<Timeslot>();
		}
	}

	public class TimeSlotCheckingModel {
		public List<TimeSlot> lstTimeSlots;
		public Set<string> timeOfDate;
		public string timezone;
		public DateTime checkingDateTime;
		public Map<id, skedResourceAvailabilityBase.resourceModel> mapResource;
		public string dateString;
		public skedVivintModel.DaySlot daySlot;
		public Integer velocity;
		public boolean ignoreTravelTimeFirstJob;
		public TimeSlot checkingSlot;

		public TimeSlotCheckingModel (List<TimeSlot> lstTimeSlots,
										Set<string> timeOfDate, string timezone, DateTime tempTime,
										Map<id, skedResourceAvailabilityBase.resourceModel> mapResource,
										string dateString, skedVivintModel.DaySlot daySlot, integer velocity,
										boolean ignoreTravelTimeFirstJob) {
			this.lstTimeSlots = new List<TimeSlot>(lstTimeSlots);
			this.timeOfDate = new Set<String>(timeOfDate);
			this.timezone = timezone;
			this.checkingDateTime = tempTime;
			this.mapResource = new Map<id, skedResourceAvailabilityBase.resourceModel>(mapResource);
			this.dateString = dateString;
			this.daySlot = daySlot;
			this.velocity = velocity;
			this.ignoreTravelTimeFirstJob = ignoreTravelTimeFirstJob;
		}
	}

	public class TimeSlotFilterModel {
		public string selectedDate;
		public boolean onlySelectedDate;
		public string zipcode;
		public transient List<String> regionIds;
	}

	public class SendOutEmailModel {
		public DateTime jobStart;
		public DateTime jobFinish;
		public string resName;
		public string resPhone;
		public string timezone;
		public string prospectName;
		public string address;
		public string prospectEmail;
		public string emailType;
		public id orgWideEmailId;
		public boolean isUnAllocated;

		public SendOutEmailModel(DateTime jobStart, DateTime jobFinish, String resName, String resPhone, String timezone,
									String prospectName, String address, String prospectEmail, String emailType) {
			this.jobStart = jobStart;
			this.jobFinish = jobFinish;
			this.resName = resName;
			this.resPhone = resPhone;
			this.timezone = timezone;
			this.prospectName = prospectName;
			this.address = address;
			this.prospectEmail = prospectEmail;
			this.emailType = emailType;

		}
	}

	public class NotifyRequest {
		public String resourceId;
		public String message;
		public String protocol;

		public NotifyRequest(sked_Queue_Management_Access__c qma, string message, string protocol) {
			this.resourceId = qma.sked_Resource__c;
			this.message = message;
			this.protocol = protocol;
		}
	}

}
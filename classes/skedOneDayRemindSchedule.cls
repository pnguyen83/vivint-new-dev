global class skedOneDayRemindSchedule {
	global void execute(SchedulableContext sc) {
		skedSetting.skedAdminSetting setting = skedSetting.instance.admin;

		if (setting.enable1DayNotificationEmail) {
			Datetime currentDate = Datetime.newInstance(system.today().addDays(1), Time.newInstance(12, 0, 0, 0));
			string DateValue = currentDate.format('yyyy-MM-dd');
			string query = 'select id from sked__Job__c where sked__Job_Status__c = \'Ready\' ANd DAY_ONLY(convertTimezone(sked__Start__c)) = ' + DateValue;
			list<sked__Job__c> jobs = Database.query(query);
			set<id> runJobIds = new set<id>();
			for(sked__Job__c job : jobs){
				runJobIds.add(job.id);
			}

			sendNotification(runJobIds);
		}
	}

	private static void sendNotification(Set<id> setJobIds) {
		Id orgWideEmailId = skedCommonService.getOrgWideEmail();
		List<Messaging.Singleemailmessage> emailTemplates = new List<Messaging.Singleemailmessage>();
		for (sked__Job__c skedJob : [SELECT id, sked__Start__c, sked__Finish__c, sked__Timezone__c,
											sked__Address__c, sked__Contact__r.FirstName, sked_Customer_Email__c, sked__Contact__r.LastName,
											(SELECT Id, sked__Resource__r.Name, sked__Resource__r.sked__Primary_Phone__c,
											sked__Resource__c, sked__Resource__r.sked__User__c
											FROM sked__Job_Allocations__r
												WHERE sked__Status__c = :skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED)
										FROM sked__Job__c
										WHERE Id IN : setJobIds]) {
			for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
				skedVivintModel.SendOutEmailModel data = new skedVivintModel.SendOutEmailModel(skedJob.sked__Start__c, skedJob.sked__Finish__c,
									skedJA.sked__Resource__r.Name, skedJA.sked__Resource__r.sked__Primary_Phone__c,
									skedJob.sked__Timezone__c, skedJob.sked__Contact__r.FirstName,
									skedJob.sked__Address__c, skedJob.sked_Customer_Email__c,
									skedConstants.REMINDER_APPOINTMENT_EMAIL);
				data.orgWideEmailId = orgWideEmailId;
				Messaging.Singleemailmessage email = skedCloserHandler.sendOutEmail(data);
				emailTemplates.add(email);
			}
		}

		if (!emailTemplates.isEmpty()) {
			Messaging.sendEmail(emailTemplates);
		}

	}
}
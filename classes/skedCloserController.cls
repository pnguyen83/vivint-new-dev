public class skedCloserController {
	@remoteAction
	public static skedRemoteResultModel getConfigData() {
		return skedCloserHandler.getConfigData();
	}

	@remoteAction
	public static skedRemoteResultModel searchLocation(String name) {
		return skedSetterHandler.searchLocation(name);
	}

	@remoteAction
	public static skedRemoteResultModel getJobs(skedVivintModel.CloserData data) {
		return skedCloserHandler.getJobOfCloser(data);
	}

	@remoteAction
	public static skedRemoteResultModel getRescheduleAvailability(skedVivintModel.JobAvaiModel data) {
		return skedCloserHandler.getAvailableOfJobInTimeFrame(data);
	}

	@remoteAction
	public static skedRemoteResultModel rescheduleJob(skedVivintModel.RescheduleJob data) {
		return skedCloserHandler.reScheduleJob(data);
	}

	@remoteAction
	public static skedRemoteResultModel confirmJob(skedVivintModel.Appointment data) {
		return skedCloserHandler.confirmAppointment(data);
	}

	@remoteAction
	public static skedRemoteResultModel cancelJob(skedVivintModel.Appointment data) {
		return skedCloserHandler.cancelAppointment(data);
	}

	@remoteAction
	public static skedRemoteResultModel getMoreJobInfo(String jobId) {
		return skedCloserHandler.getMoreJobInformation(jobId);
	}

	@remoteAction
	public static skedRemoteResultModel updateJobInfo (skedVivintModel.VivintContact contact) {
		return skedCloserHandler.updateJobInformation(contact);
	}

	@remoteAction
	public static skedRemoteResultModel searchJob(skedVivintModel.AreaManagementFilter filter) {
		return skedCloserHandler.searchJob(filter);
	}

	@remoteAction
	public static skedRemoteResultModel updateAttemptedJob(skedVivintModel.VivintJob job) {
		return skedCloserHandler.updateContactAttempTimeAndStatusOnJob(job);
	}

	@remoteAction
	public static skedRemoteResultModel getTimeLabelOfDay(skedVivintModel.TimeSlotFilterModel filter) {
		return skedCommonService.getTimeLabelOfDayForCloser(filter);
	}
}
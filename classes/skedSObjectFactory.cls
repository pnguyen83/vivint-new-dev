/**
* @description base class SObjectModel
*/
public virtual class skedSObjectFactory{

    //=============================================================================================
    // CONSTANTS
    //=============================================================================================
    public final String US = 'US';
    public final String AU = 'AU';
    public final Map<String,String> MAP_DEFAULT_ADDR = new Map<String,String>{
        AU  => '79 Mclachlan Fortitude Valley st Brisbane QLD',
        US  => '228 Park Ave S, New York'
    };

    public final Map<String, List<Object>> MAP_REGIONS{
        get{
            if(MAP_REGIONS == null){
                MAP_REGIONS = new Map<String, List<Object>>{
                    US => new List<Object>{'US/Central' ,'US/Mountain', 'US/Michigan'},
                    AU => new List<Object>{'Australia/Queensland','Australia/NSW','Australia/Victoria', 'Australia/Sydney'}
                };
            }
            return MAP_REGIONS;
        }
        set;
    }

    public final list<String> RESOURCE_TYPES = new List<String>{ 'Person', 'Asset' };

    //=============================================================================================
    // VARIABLES
    //=============================================================================================
    // SObjects (in order of dependency) to be generated
    public list<SObjectModel> sObjectList;
    private Map<Schema.sObjectType, list<sObject>> mapSObjectType2ObjectList;

    public skedSObjectFactory(){
        sObjectList                 = new list<SObjectModel>();
        mapSObjectType2ObjectList   = new Map<Schema.sObjectType, list<sObject>>();
    }

    //=============================================================================================
    // MAIN METHODS
    //=============================================================================================
    /**
    * @description create all registered objects
    *
    */
    public Map<Schema.sObjectType, list<sObject>> create(){
        sObjectList.sort();

        for(SObjectModel obj : sObjectList){
            //Fill relationships
            fillRelationships(obj);
            //Generate data
            list<sObject> records = obj.create(true);
            mapSObjectType2ObjectList.put( obj.sObjectType, records );
        }
        return mapSObjectType2ObjectList;
    }

    /**
    * @description Initialize a sample set of test data
    *
    */
    public skedSObjectFactory init(){
        //Account - 10
        this.newObject(Account.sObjectType).field('Name', 'Test Account');

        //Region - 20
        this.newObject(sked__Region__c.sObjectType).fields(new Map<String, Object>{
                'sked__Country_Code__c'     => US,
                'Name'                      => MAP_REGIONS.get(US).get(0),
                'sked__Timezone__c'         => MAP_REGIONS.get(US).get(0)
            });

        //Contact - 30
        this.newObject(Contact.sObjectType).fields(new Map<String,Object>{
                'FirstName'                 => 'Test Contact',
                'LastName'                 => 'Test Contact',
                'AccountId'                 => this.newRelationship(Account.sObjectType),
                'sked__Region__c'           => this.newRelationship(sked__Region__c.sObjectType),
                'Email'                     => 'contact@email.com'
            });

        //Job - 40
        this.newObject(sked__Job__c.sObjectType).fields(new Map<String, Object>{
                'sked__Duration__c'         => 180,
                'sked__Start__c'            => getDTList( DateTime.newInstance(System.now().date(), Time.newInstance(8, 0,0,0)).addDays(-7), 14 ),
                'sked__Job_Status__c'       => new List<Object> { 'Pending Dispatch' },
                'sked__Contact__c'          => this.newRelationship(Contact.sObjectType),
                'sked__Region__c'           => this.newRelationship(sked__Region__c.sObjectType),
                'sked__Address__c'          => MAP_DEFAULT_ADDR.get(US),
                'sked__GeoLocation__latitude__s' => 40.7376731,
                'sked__GeoLocation__longitude__s' => -73.9897125
            });

        //Resource - 50
        this.newObject(sked__Resource__c.sObjectType).fields(new Map<String, Object>{
                'Name'                      => 'Test Resource',
                'sked__Resource_Type__c'    => 'Person',
                'sked__Primary_Region__c'   => this.newRelationship(sked__Region__c.sObjectType),
                'sked__Home_Address__c'     => MAP_DEFAULT_ADDR.get(US),
                'sked__Category__c'         => getPicklistValue(sked__Resource__c.sked__Category__c).get(0),
                'sked__Email__c'            => 'resource@email.com',
                'sked_Maximum_Travel_radius__c' => 10
            });

        //Tag - 60
        this.newObject(sked__Tag__c.sObjectType).fields(new Map<String, Object>{
                'Name'                      => 'Communication'
            });

        //Resource Tag - 70
        this.newObject(sked__Resource_Tag__c.sObjectType).fields(new Map<String, Object>{
                'sked__Resource__c'         => this.newRelationship(sked__Resource__c.sObjectType),
                'sked__Tag__c'              => this.newRelationship(sked__Tag__c.sObjectType)
            });

        //Job Allocation - 80
        this.newObject(sked__Job_Allocation__c.sObjectType).fields(new Map<String, Object>{
                'sked__Resource__c'         => this.newRelationship(sked__Resource__c.sObjectType),
                'sked__Job__c'              => this.newRelationship(sked__Job__c.sObjectType),
                'sked__Status__c'              => 'Pending Dispatch'
            });

        //Availability Template - 90
        this.newObject(sked__Availability_Template__c.sObjectType);

        //Availability Template Entry - 100
        this.newObject(sked__Availability_Template_Entry__c.sObjectType).fields(new Map<String, Object>{
                'sked__Start_Time__c'       => 800,
                'sked__Finish_Time__c'      => 2300,
                'sked__Is_Available__c'     => true,
                'sked__Weekday__c'          => getPicklistValue(sked__Availability_Template_Entry__c.sked__Weekday__c),
                'sked__Availability_Template__c'    => this.newRelationship(sked__Availability_Template__c.sObjectType),
                'sked_Number_of_Parallel_Bookings__c' => 2
            });

        //Availability Template Resource - 110
        this.newObject(sked__Availability_Template_Resource__c.sObjectType).fields(new Map<String, Object>{
                'sked__Availability_Template__c'    => this.newRelationship(sked__Availability_Template__c.sObjectType),
                'sked__Resource__c'                 => this.newRelationship(sked__Resource__c.sObjectType).filter('sked__Resource_Type__c','Person')
            });

        //Availability - 120
        this.newObject(sked__Availability__c.sObjectType).fields(new Map<String, Object>{
                'sked__Resource__c'         => this.newRelationship(sked__Resource__c.sObjectType).filter('sked__Resource_Type__c','Person'),
                'sked__Is_Available__c'     => new list<Object>{FALSE, TRUE},
                'sked__Status__c'           => 'Approved',
                'sked__Start__c'            => DateTime.newInstance(System.now().date(), Time.newInstance(0,0,0,0)),
                'sked__Finish__c'           => DateTime.newInstance(System.now().addDays(1).date(), Time.newInstance(0,0,0,0))
            });

        //Activity - 130
        this.newObject(sked__Activity__c.sObjectType).fields(new Map<String, Object>{
                'sked__Resource__c'         => this.newRelationship(sked__Resource__c.sObjectType).filter('sked__Resource_Type__c','Person'),
                'sked__Start__c'            => DateTime.newInstance(System.now().date(), Time.newInstance(0,0,0,0)),
                'sked__End__c'              => DateTime.newInstance(System.now().addDays(1).date(), Time.newInstance(0,0,0,0)),
                'sked__Type__c'             => getPicklistValue(sked__Activity__c.sked__Type__c).get(0)
            });

        initMore();

        return this;
    }

    /**
    * @description
    * @param obj
    */
    public void fillRelationships(SObjectModel obj){
        for(String fieldName : obj.mapFieldname2Values.keySet()){
            Object value = obj.mapFieldname2Values.get(fieldName);
            if(value instanceof List<Object>){
                 Object firstValue = new List<Object>( (List<Object>)value ).get(0);
                 if(firstValue instanceof RelationshipModel){
                    RelationshipModel relationship = (RelationshipModel)firstValue;
                    if(!mapSObjectType2ObjectList.containsKey( relationship.sObjectType )) continue;
                    List<Object> recordIDs = new List<Object>();
                    Integer count = 0;
                    for(sObject rec : mapSObjectType2ObjectList.get( relationship.sObjectType )){
                        //Apply filter
                        boolean matchFilter = true;
                        for(String filteredFieldName : relationship.mapFilter.keySet()){
                            if(rec.get(filteredFieldName) != relationship.mapFilter.get(filteredFieldName)){
                                matchFilter = false;
                                break;
                            }
                        }
                        if(!matchFilter) continue;
                        recordIDs.add( String.valueOf(rec.get('Id')) );
                        count++;
                        if(count >= relationship.noOfRecords) break;//Limit the number of records
                    }
                    obj.field(fieldName, recordIDs);
                 }
            }
        }
    }

    /**
    * @description
    * @param sObjectType
    */
    public skedSObjectFactory removeObject(Schema.sObjectType sObjectType){
        for(Integer j = 0; j < sObjectList.size(); j++){
           if(sObjectList.get(j).sObjectType == sObjectType){
                sObjectList.remove(j);
                break;
           }
        }
        return this;
    }

    /**
    * @description
    * @param sObjectType
    */
    public skedSObjectFactory removeObjects(List<Schema.sObjectType> sObjectTypes){
        for(Schema.sObjectType objType : sObjectTypes){
           removeObject(objType);
        }
        return this;
    }

    /**
    * @description
    * @param sObjectType
    */
    public SObjectModel getObject(Schema.sObjectType sObjectType){
        for(SObjectModel obj : sObjectList){
            if(obj.sObjectType == sObjectType) return obj;
        }
        return null;
    }

    /**
    * @description
    * @param sObjectType
    */
    public List<sObject> getSObjects(Schema.sObjectType sObjectType){
        return mapSObjectType2ObjectList.get(sObjectType);
    }

    /**
    * @description
    * @param sObjectType
    */
    public SObjectModel newObject(Schema.sObjectType sObjectType){
        Integer greatestCreationOrder = 10;
        if(!sObjectList.isEmpty()){
            sObjectList.sort();
            greatestCreationOrder += sObjectList.get( sObjectList.size()-1 ).creationOrder;
        }
        sObjectModel newObject = initNewObject(sObjectType).creationOrder(greatestCreationOrder);
        sObjectList.add(newObject);
        return newObject;
    }

    /**
    * @description
    * @param sObjectType
    * @param creationOrder
    */
    public RelationshipModel newRelationship(Schema.sObjectType sObjectType){
        return new RelationshipModel(sObjectType);
    }

    //=============================================================================================
    // INNER CLASSES
    //=============================================================================================
    /**
    * @description base class SObjectModel
    */
    public virtual class SObjectModel implements Comparable{

        public Schema.sObjectType sObjectType;
        public Map<String, List<Object>> mapFieldname2Values;
        public Map<String, Object> mapFilter;
        public Integer multiplyBy;
        public Integer creationOrder;

        public SObjectModel(Schema.sObjectType ot){
            mapFieldname2Values     = new Map<String, List<Object>>();
            this.sObjectType        = ot;
            this.multiplyBy         = 1;
            this.mapFilter          = new Map<String, Object>();
        }

        /**
        * @description
        * @param doInsert
        */
        public List<sObject> create(boolean doInsert){
            String sObjectName = sObjectType.getDescribe().getName();
            list<sObject> result = new List<sObject>();
            Schema.SObjectType sObjectType = Schema.getGlobalDescribe().get(sObjectName);
            if(mapFieldname2Values == null || mapFieldname2Values.keySet().isEmpty()){
                result.addAll( skedTestDataFactoryBase.createSObjectList( sObjectName, false, multiplyBy));
            }else{
                List<Map<String, Object>> listMapFieldname2Values = combine( mapFieldname2Values );
                for(Map<String, Object> mapFieldname2Value : listMapFieldname2Values){
                    result.addAll( skedTestDataFactoryBase.createSObjectList( sObjectName, mapFieldname2Value, false, multiplyBy) );
                }
            }
            //format data before insert
            format(sObjectType, result);
            if(doInsert) insert result;
            return result;
        }

        /**
        * @description Format data to meed per-defined rules
        * @param sObjectType
        * @param objects
        */
        public void format(Schema.sObjectType sObjectType, list<sObject> objects){
            if(sObjectType == sked__Job__c.sObjectType){
                for(sObject obj : objects){
                    if(obj.get('sked__Start__c') != null && obj.get('sked__Duration__c')!=null){
                        obj.put( 'sked__Finish__c', ((DateTime)obj.get('sked__Start__c')).addMinutes( Integer.valueOf(obj.get('sked__Duration__c')) ) );
                    }
                }
            }
            formatMore(sObjectType, objects);
        }

        /**
        * @description To be overridden
        * @param sObjectType
        * @param objects
        */
        public virtual void formatMore(Schema.sObjectType sObjectType, list<sObject> objects){

        }

        /**
        * @description
        * @param mapFieldname2Values
        */
        public SObjectModel fields(Map<String, object> mapFieldname2Values){
            for(String fieldName : mapFieldname2Values.keySet()){
                Object value = mapFieldname2Values.get(fieldName);
                if(value instanceof List<Object>) this.mapFieldname2Values.put( fieldName, (List<Object>)value );
                else if(value instanceOf RelationshipModel){
                    this.mapFieldname2Values.put( fieldName, new List<Object>{ value });
                }
                else this.mapFieldname2Values.put( fieldName, new List<Object>{ value });
            }

            return this;
        }

        /**
        * @description
        * @param fieldName
        * @param values
        */
        public SObjectModel field(String fieldName, Object value){
            this.fields(new Map<String, Object>{ fieldName => value });
            return this;
        }

        /**
        * @description
        * @param multiplyBy
        */
        public SObjectModel x(Integer multiplyBy){
            this.multiplyBy     = multiplyBy;
            return this;
        }

        /**
        * @description
        * @param creationOrder
        */
        public SObjectModel creationOrder(Integer creationOrder){
            this.creationOrder = creationOrder;
            return this;
        }

        /**
        * @description
        * @param compareTo
        */
        public Integer compareTo(Object compareTo) {
            SObjectModel obj = (SObjectModel)compareTo;
            return (this.creationOrder > obj.creationOrder ? 1 :-1);
        }
    }

    /**
    * @description base class SObjectModel
    */
    public class RelationshipModel{
        public Schema.sObjectType sObjectType;
        public Integer noOfRecords;
        public Map<String,Object> mapFilter;

        public RelationshipModel(Schema.sObjectType ot){
            this.sObjectType        = ot;
            this.noOfRecords        = 100;
            this.mapFilter          = new Map<String,Object>();
        }

        /**
        * @description
        * @param filter
        */
        public RelationshipModel filter(String fieldName, Object value){
            this.mapFilter.put( fieldName, value );
            return this;
        }

        /**
        * @description
        * @param size
        */
        public RelationshipModel size(Integer noOfRecords){
            this.noOfRecords        = noOfRecords;
            return this;
        }
    }
    //=============================================================================================
    // VIRTUAL METHODS
    //=============================================================================================
    /**
    * @description Initialize an instance of SObjectModel
    *
    */
    public virtual SObjectModel initNewObject(Schema.sObjectType sObjectType){
        return new SObjectModel(sObjectType);
    }
    /**
    * @description Initialize more objects
    *
    */
    public virtual void initMore(){

    }
    //=============================================================================================
    // UTILITY METHODS
    //=============================================================================================
    /**
    * @description: get a list of active values of a picklist field
    * @param: a picklist field which is a sObjectField type, for example Opportunity.StageName, sked__Job__c.sked__Job_Status__c
    * @return: a list of active values of the picklist field
    */
    public static list<String> getPicklistValue(sObjectField field){
        Schema.DescribeFieldResult fieldResult = field.getDescribe();
        list<String> result = new list<String>();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

       for( Schema.PicklistEntry f : ple){
          if(f.isActive()) result.add( f.getValue() );
       }
       return result;
    }

    /**
    * @description: transform a Map<K,Set<V>> into the List<Map<K,V>> getting all possible combinations
    *
    */
    public static List<Map<String, Object>> combine(Map<String, List<Object>> originalMap) {
        List<Map<String, Object>> finalList = new List<Map<String, Object>>();
        Map<String, Object> current = new Map<String, Object>();
        Integer index = 0;

        combine(index, current, originalMap, finalList);

        return finalList;
    }

    /**
     * @description transform a Map<K,Set<V>> into the List<Map<K,V>> getting all possible combinations
     * @param <K> The type of the key
     * @param <V> The type of the value
     * @param index The index of the current key to inspect
     * @param current The current map being built by recursion
     * @param map The original input
     * @param list The result
     */
    public static void combine(Integer index, Map<String, Object> current, Map<String, List<Object>> originalMap, List<Map<String, Object>> finalList) {

        if(index == originalMap.size()) { // if we have gone through all keys in the map
            Map<String, Object> newMap = new Map<String, Object>();

            for(String key: current.keySet()) {          // copy contents to new map.
                newMap.put(key, current.get((String)key));
            }
            finalList.add(newMap); // add to result.
        } else {
            String currentKey = new list<String>(originalMap.keySet()).get(index); // take the current key
            for(Object value: originalMap.get(currentKey)) {
                current.put((String)currentKey, value); // put each value into the temporary map
                Combine(index + 1, current, originalMap, finalList); // recursive call
                current.remove(currentKey); // discard and try a new value
            }
        }
    }

    /**
    * @description: generate a list of continuous DateTime
    * @param start
    * @param noOfDays
    */
    public static List<Object> getDTList(DateTime start, Integer noOfDays) {
        list<Object> result = new List<Object>();
        DateTime temp;
        for(Integer i = 0; i<= noOfDays; i++){
            temp = start.addDays(i);
            result.add(temp);
        }
        return result;
    }
}
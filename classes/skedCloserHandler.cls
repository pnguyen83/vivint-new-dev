public class skedCloserHandler {
	public static skedRemoteResultModel getConfigData() {
		skedRemoteResultModel result = new skedRemoteResultModel();

		try {
			result.data = new skedVivintModel.SetterSettingData();
		}
		catch(Exception ex) {
			result.getError(ex);
		}

		return result;
	}

	public static skedRemoteResultModel getJobOfCloser(skedVivintModel.CloserData data) {
		skedRemoteResultModel result = new skedRemoteResultModel();

		try {
			result.data = getJobOfCloserFromUserId(data);
		}
		catch (Exception ex) {
			result.getError(ex);
		}

		return result;
	}

	public static skedRemoteResultModel getAvailableOfJobInTimeFrame(skedVivintModel.JobAvaiModel data) {
		skedRemoteResultModel result = new skedRemoteResultModel();

		try {
			result.data = getAvailableResOfJob (data);
		}
		catch (Exception ex) {
			result.getError(ex);
		}

		return result;
	}

	public static skedRemoteResultModel updateContactAttempTimeAndStatusOnJob(skedVivintModel.VivintJob job) {
		skedRemoteResultModel result = new skedRemoteResultModel();
		SavePoint sp = Database.setSavePoint();

		try {
			result.data = updateContactAttempedJob(job);
		}
		catch (Exception ex) {
			Database.rollback(sp);
			result.getError(ex);

		}

		return result;
	}

	public static skedRemoteResultModel cancelAppointment(skedVivintModel.Appointment data) {
		skedRemoteResultModel result = new skedRemoteResultModel();
		SavePoint sp = Database.setSavePoint();
		try {
			data.isCancel = true;
			cancelOrConfirmJobAndJobAllocation (data);
		}
		catch (Exception ex) {
			result.getError(ex);
			Database.rollback(sp);
		}

		return result;
	}

	public static skedRemoteResultModel confirmAppointment(skedVivintModel.Appointment data) {
		skedRemoteResultModel result = new skedRemoteResultModel();
		SavePoint sp = Database.setSavePoint();
		try {
			data.isCancel = false;
			cancelOrConfirmJobAndJobAllocation (data);
		}
		catch (Exception ex) {
			result.getError(ex);
			Database.rollback(sp);
		}

		return result;
	}

	public static skedRemoteResultModel getMoreJobInformation(String jobId) {
		skedRemoteResultModel result = new skedRemoteResultModel();
		List<skedVivintModel.VivintContact> contactInfos = new List<skedVivintModel.VivintContact>();

		try {
			skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job__c.sObjectType).filter('id =: jobId');
            List<sked__job__c> skedJobs = Database.query(selector.getQuery());

			if (skedJobs == null || skedJobs.isEmpty()) {
				result.success = false;
				result.errorMessage = skedConstants.INVALID_JOB_ID;
				return result;
			}

			for (sked__Job__c skedJob : skedJobs) {
				skedVivintModel.VivintContact contactInfo = new skedVivintModel.VivintContact(skedJob);
				contactInfos.add(contactInfo);
			}

			result.data = contactInfos;
		}
		catch (Exception ex) {
			result.getError(ex);
		}

		return  result;
	}

	public static skedRemoteResultModel updateJobInformation(skedVivintModel.VivintContact contact) {
		skedRemoteResultModel result = new skedRemoteResultModel();
		SavePoint sp = Database.setSavePoint();

		try {
			result.data = updateProspectInfo(contact);
		}
		catch(Exception ex) {
			Database.rollback(sp);
			result.getError(ex);
		}

		return result;
	}


	public static skedRemoteResultModel reScheduleJob(skedVivintModel.RescheduleJob data) {
		skedRemoteResultModel result = new skedRemoteResultModel();
		SavePoint sp = Database.setSavePoint();
		try {
			implementRescheduleJob(data);
		}
		catch (Exception ex) {
			result.getError(ex);
			Database.rollback(sp);
		}

		return result;
	}

	public static skedRemoteResultModel searchJob(skedVivintModel.AreaManagementFilter filter) {
		skedRemoteResultModel result = new skedRemoteResultModel();
		try {
			result = searchJobOfCurrentUser(filter);
		}
		catch(Exception ex) {
			result.getError(ex);
		}

		return result;
	}

	public static Messaging.Singleemailmessage sendOutEmail(skedVivintModel.SendOutEmailModel data) {
		String startTime = data.jobStart.format(skedConstants.HOUR_MINUTES_ONLY, data.timezone);
		String endTime = data.jobFinish.format(skedConstants.HOUR_MINUTES_ONLY, data.timezone);
		String arrivalWindow = startTime + ' - ' + endTime;
		String confirmationDate = data.jobStart.format(skedConstants.WEEKDAY_DAY_MONTH_YEAR, data.timezone);
		String emailType = data.emailType;
		boolean enableOrgwide = sked_Admin_Setting__c.getOrgDefaults().sked_Enable_Org_Wide_Email__c;
		enableOrgwide = enableOrgwide == null ? false : enableOrgwide;

		skedObjectSelector selector = skedObjectSelector.newInstance(EmailTemplate.sObjectType).filter('DeveloperName = :emailType');
        List<EmailTemplate> emailTemplates = Database.query(selector.getQuery());

		if (emailTemplates == null || emailTemplates.isEmpty()) {
			throw new skedException(skedConstants.MISSING_EMAIL_TEMPLATE);
		}

		EmailTemplate template =emailTemplates.get(0);

		String subject = template.Subject;

		String prospectName = String.isNotBlank(data.prospectName) ? data.prospectName : '';
		String resName = String.isNotBlank(data.resName) ? data.resName : '';
		confirmationDate = String.isNotBlank(confirmationDate) ? confirmationDate : '';
		arrivalWindow = String.isNotBlank(arrivalWindow) ? arrivalWindow : '';
		String address = String.isNotBlank(data.address) ? data.address : '';
		String resPhone = String.isNotBlank(data.resPhone) ? data.resPhone : '';

		String htmlBody = template.HtmlValue;
		htmlBody = htmlBody.replace('[First Name]', prospectName);
		htmlBody = htmlBody.replace('[SHP NAME]', resName);
		htmlBody = htmlBody.replace('[DATE]', confirmationDate);
		htmlBody = htmlBody.replace('[ARRIVAL TIME]', arrivalWindow);
		htmlBody = htmlBody.replace('[ADDRESS]', address);
		if (data.isUnAllocated == null && data.isUnAllocated == false) {
			htmlBody = htmlBody.replace('[XXX.XXX.XXXX]', resPhone);
			htmlBody = htmlBody.replace('[SHP NUMBER]', resPhone);
		}

		Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
		email.setToAddresses(new List<String>{data.prospectEmail});
		if (enableOrgwide) {
			email.setOrgWideEmailAddressId(data.orgWideEmailId);
		}
		else {
			email.setSenderDisplayName(resName);
		}

	    email.setSaveAsActivity(false);


	    email.setSubject(subject);
	    email.setHtmlBody(htmlBody);

	    return email;
	}

	public static skedRemoteResultModel getCloserJobCreationGridData(skedVivintModel.CloserFilterData filter) {
		skedRemoteResultModel result = new skedRemoteResultModel();

		result = getCloserGridData(filter);

		return result;
	}
	//======================================================Private functions==========================================//
	private static skedVivintModel.CloserData getJobOfCloserFromUserId(skedVivintModel.CloserData data) {
		Id userId = UserInfo.getUserId();
		String timezone = UserInfo.getTimeZone().getId();

		DateTime startTime = skedDateTimeUtils.getStartOfDate(Date.valueOf(data.startDate), timezone);
		DateTime endTime = skedDateTimeUtils.getEndOfDate(Date.valueOf(data.endDate), timezone);
		String pendingDispatchStatus = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH;

		skedObjectSelector selector = skedObjectSelector.newInstance(sked__Resource__c.sObjectType).filter('sked__User__c =: userId AND sked__Is_Active__c = true');
        selector.subQuery('sked__Job_Allocations__r').filter('sked__Status__c = :pendingDispatchStatus AND sked__Job__r.sked__Start__c < :endTime AND sked__Job__r.sked__Finish__c > :startTime');

		List<sked__Resource__c> skedResources = Database.query(selector.getQuery());
		if (skedResources == null || skedResources.isEmpty()) {
			throw new skedException(skedConstants.USER_NOT_ASSOCIATED_WITH_RES);
		}

		sked__Resource__c skedRes = skedResources.get(0);

		if (skedRes.sked__Is_Active__c == false) {
			throw new skedException(skedConstants.ASSOCIATED_RESOURCE_NOT_ACTIVE);
		}

		skedVivintModel.CloserData closerData = new skedVivintModel.CloserData(skedRes, startTime, endTime, timezone);
		return closerData;
	}

	private static String getAvailableResOfJob (skedVivintModel.JobAvaiModel data) {
		data.fromCloserHandler = true;//only check availability of current login user
		Map<Id, skedResourceAvailabilityBase.resourceModel> mapResource = skedCommonService.getAvaiResourcesOfJob(data);
		String timezone = data.timezone;
		sked__Job__c skedJob = data.job;
        DateTime startTime = skedDateTimeUtils.getStartOfDate(data.dateStart, timezone);
        DateTime endTime = skedDateTimeUtils.getEndOfDate(data.dateEnd, timezone);

        DateTime tempTime = startTime;
        String regionId = skedCommonservice.getRegionIdFromTimezone(timezone);

        List<skedVivintModel.DaySlot> lstDaySlots = new List<skedVivintModel.DaySlot>();
        List<skedVivintModel.TimeSlot> lstTimeSlots = skedCommonService.getListTimeSlotInDefinedTimeRange(data.dateStart, data.dateEnd, new List<String>{regionId});

        //set job location to timeslot
        for (skedVivintModel.TimeSlot slot : lstTimeSlots) {
        	slot.jobLocation = Location.newInstance(skedJob.sked__GeoLocation__latitude__s, skedJob.sked__GeoLocation__longitude__s);
        }

        while (tempTime < endTime) {
        	skedVivintModel.DaySlot daySlot = new skedVivintModel.DaySlot(tempTime, timezone);
			List<skedVivintModel.TimeSlot> timeslots = new List<skedVivintModel.TimeSlot>();

			for (skedVivintModel.TimeSlot slot : lstTimeSlots) {
				if (slot.bookingType.equalsIgnoreCase(skedConstants.SLOT_RESOURCE_TYPE)) {
					timeslots.add(slot);
				}
			}

			//get config setting data
            Integer velocity = skedSetting.instance.Admin.velocity;
            boolean ignoreTravelTimeFirstJob = skedSetting.instance.Admin.ignoreTravelTimeFirstJob;

			skedVivintModel.TimeSlotCheckingModel checkingData = new skedVivintModel.TimeSlotCheckingModel(timeslots, new Set<string>(), timezone, tempTime, mapResource,
																		daySlot.dayString, daySlot, velocity, ignoreTravelTimeFirstJob);

			daySlot = skedSetterHandler.checkTimeSlotOfDateSlot(checkingData);

			for (skedVivintModel.TimeSlot slot : daySlot.lstTimeSlots) {
				if (slot.isAvai != null && slot.isAvai) {
					daySlot.isAvailable = true;
					break;
				}
			}

			lstDaySlots.add(daySlot);
			tempTime = skedDateTimeUtils.addDays(tempTime, 1, timezone);
        }

        return JSON.serialize(lstDaySlots);
	}

	private static void cancelOrConfirmJobAndJobAllocation (skedVivintModel.Appointment data) {
		if (String.isNotBlank(data.jobId)) {
			List<sObject> sObjects = new List<sObject>();
			String userId = UserInfo.getUserId();

			for (sked__Job__c skedJob : [SELECT id, sked__Start__c, sked__Finish__c, sked__Timezone__c,
											sked__Address__c, sked__Contact__r.Name, sked_Customer_Email__c,
											(SELECT Id, sked__Resource__r.Name, sked__Resource__r.sked__Primary_Phone__c,
											sked__Resource__c, sked__Resource__r.sked__User__c
											FROM sked__Job_Allocations__r
												WHERE sked__Status__c = :skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH)
										FROM sked__Job__c
										WHERE Id =: data.jobId]) {
				if (data.isCancel != null && data.isCancel) {
					skedJob.sked__Abort_Reason__c = data.reason;
					sObjects = updateJobAndJobAllocationStatus(sObjects, skedJob, skedConstants.JOB_STATUS_CANCELLED,
													skedConstants.JOB_ALLOCATION_STATUS_DELETED);
				}
				else {
					sObjects = updateJobAndJobAllocationStatus(sObjects, skedJob, skedConstants.JOB_STATUS_READY,
													skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED);
					String resName = '';
					String resPhone = '';

					for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
						if (skedJA.sked__Resource__r.sked__User__c == userId) {
							resName = skedJA.sked__Resource__r.Name;
							resPhone = skedJA.sked__Resource__r.sked__Primary_Phone__c;
							break;
						}
					}
				}
			}

			if (!sObjects.isEmpty()) {
				update sObjects;
			}
		}
	}

	private static List<sObject> updateJobAndJobAllocationStatus(List<sObject> sObjects, sked__Job__c skedJob,
																	String jobStatus, String jaStatus) {
		skedJob.sked__Job_Status__c = jobStatus;
		sObjects.add(skedJob);

		if (!skedJob.sked__Job_Allocations__r.isEmpty()) {
			for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
				skedJA.sked__Status__c = jaStatus;
				sObjects.add(skedJA);
			}
		}

		return sObjects;
	}

	private static void implementRescheduleJob(skedVivintModel.RescheduleJob data) {
		List<sked__Job__c> skedJobs = [SELECT id, sked__Start__c, sked__Timezone__c, sked__Duration__c, sked__Address__c,
										sked__Finish__c, sked__Contact__r.Name, sked_Customer_Email__c,
										sked__Description__c, sked__GeoLocation__c, sked__Job_Status__c, sked_Setter_Location__c,
										Opportunity__c, sked__Contact__c, ssked_Prospect_Additional_information__c, sked__Quantity__c,
										sked__Region__c, sked_Setter__c, sked__Type__c, sked__GeoLocation__latitude__s, sked__GeoLocation__longitude__s,
										(SELECT Id FROM sked__Job_Allocations__r WHERE sked__Status__c !=: skedConstants.JOB_ALLOCATION_STATUS_COMPLETE
										AND sked__Status__c !=: skedConstants.JOB_ALLOCATION_STATUS_DECLINED
										AND sked__Status__c !=: skedConstants.JOB_ALLOCATION_STATUS_DELETED)
										FROM sked__Job__c
										WHERE Id = :data.jobId];

		if (skedJobs == null || skedJobs.isEmpty()) {
			throw new skedException(skedConstants.INVALID_JOB_ID);
		}

		sked__Job__c skedJob = skedJobs.get(0);
		String timezone = skedJob.sked__Timezone__c;

		if (skedJob.sked__Job_Status__c == skedConstants.JOB_STATUS_COMPLETE) {
			throw new skedException(skedConstants.ERROR_RESCHEDULE_COMPLETE_JOB);
		}

		if (String.isBlank(skedJob.sked_Customer_Email__c)) {
			throw new skedException(skedConstants.PROSPECT_MISSING_EMAIL);
		}

		Id currentUserId = UserInfo.getUserId();

		List<sked__Resource__c> skedResources = [SELECT id, Name, sked__Primary_Phone__c
													FROM sked__Resource__c
													WHERE sked__User__c =: currentUserId
													AND sked__Is_Active__c = true];

		if (skedResources == null || skedResources.isEmpty()) {
			throw new skedException(skedConstants.USER_NOT_ASSOCIATED_WITH_RES + ' or ' + skedConstants.ASSOCIATED_RESOURCE_NOT_ACTIVE);
		}

		sked__Resource__c skedRes = skedResources.get(0);

		if (String.isBlank(data.selectedDate)) {
			throw new skedException(skedConstants.MISSING_RESCHEDULE_DAY);
		}

		DateTime startOfSelectedDay = DateTime.newInstance(Date.valueOf(data.selectedDate), Time.newInstance(0, 0, 0, 0));
		startOfSelectedDay = skedDateTimeUtils.getStartOfDate(startOfSelectedDay, timezone);
		Integer startMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(data.startTime);
		Integer endMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(data.endTime);
		DateTime newJobStart = skedDateTimeUtils.addMinutes(startOfSelectedDay, startMinutes, timezone);
		DateTime newJobEnd = skedDateTimeUtils.addMinutes(startOfSelectedDay, endMinutes, timezone);

		skedJob.sked__Start__c = newJobStart;
		skedJob.sked__Finish__c = newJobEnd;
		skedJob.sked__Duration__c = skedDateTimeUtils.getDifferenteMinutes(newJobStart, newJobEnd);
		skedJob.sked_Job_Start_Minutes__c = startMinutes;
		skedJob.sked_Job_End_Minutes__c = endMinutes;

		List<sObject> updateJobandJas = new List<sObject>{skedJob};
		for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
			skedJA.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
			updateJobandJas.add(skedJA);
		}

		update updateJobandJas;

		sked__Job_Allocation__c newSkedJA = new sked__Job_Allocation__c(
			sked__Job__c = skedJob.id,
			sked__Resource__c = skedRes.id,
			sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
		);

		insert newSkedJA;


	}

	private static skedVivintModel.VivintContact updateProspectInfo (skedVivintModel.VivintContact contact) {
		sked__Job__c newPros = new sked__Job__c();
		Account newAcc = new Account();

		List<sObject> objects = new List<sObject>();

		newPros.id = contact.jobId;
		if (String.isNotBlank(contact.accountNumber))	{
			newPros.sked_Vivint_Account_Number__c = contact.accountNumber;
		}

		if (String.isNotBlank(contact.address))	{
			newPros.sked_Prospect_Address__c = contact.address;
			newAcc.ShippingStreet = contact.address;
		}
		if (String.isNotBlank(contact.phone)) {
			newPros.sked_Customer_Phone__c = contact.phone;
			newAcc.Phone = contact.phone;
		}

		newPros.sked_Prospect_Name__c = contact.firstname + ' ' + contact.lastname;
		newPros.sked_Prospect_First_Name__c = contact.firstname;
		newPros.sked_Prospect_Last_Name__c = contact.lastname;
		newAcc.Name = contact.firstname + ' ' + contact.lastname;

		newPros.sked_Customer_Email__c = contact.email;


		if (contact.location != null && contact.location.id != null) {
			newPros.sked_Setter_Location__c = contact.location.id;
		}

		if (contact.setterName != null) {
			newPros.sked_Setter_Name__c = contact.setterName;
		}

		if (contact.repId != null) {
			newPros.sked_Prospect_Rep_ID__c = contact.repId;
		}

		if (contact.ownHomeOrBusiness != null) {
			newPros.sked_Prospect_owns_Home_or_Business__c = contact.ownHomeOrBusiness;
		}
		if (contact.howInterestedInSH != null) {
			newPros.sked_Prospect_interested_in_Smart_Home__c = contact.howInterestedInSH;
		}
		if (contact.areaInterestedIn != null) {
			newPros.sked_Area_Interested_In_Smart_Home__c = string.join(contact.areaInterestedIn,'; ');
		}
		if (contact.haveHighSpeedInternet != null) {
			newPros.sked_Prospect_has_speed_internet__c = contact.haveHighSpeedInternet;
		}
		if (contact.haveSecuritySystem != null) {
			newPros.sked_Do_you_have_security_system__c = contact.haveSecuritySystem;
		}
		if (contact.timeHaveSystem != null) {
			newPros.sked_How_long_have_you_had_your_system__c = contact.timeHaveSystem;
		}
		if (contact.systemMonitored != null) {
			newPros.sked_Is_your_system_being_monitored__c = contact.systemMonitored;
		}
		if (contact.decisionMaking != null) {
			newPros.sked_Will_a_decision_maker_be_present__c = contact.decisionMaking;
		}
		if (contact.additionalInformation != null) {
			newPros.ssked_Prospect_Additional_information__c = contact.additionalInformation;
		}

		if (contact.accountId != null) {
			newAcc.id = contact.accountId;
			objects.add(newAcc);
		}

		objects.add(newPros);

		objects.sort();

		update objects;
		return contact;
	}

	private static skedRemoteResultModel searchJobOfCurrentUser(skedVivintModel.AreaManagementFilter filter) {
		filter.regions = new List<String>(skedCommonService.getRegionIdFromUserId(filter));
		filter.closers = new List<String>{skedCommonService.getResourceFromUserId(UserInfo.getUserId()).id};
		filter.isFromAppointmentQueue = true;
		skedRemoteResultModel data = skedAreaManagementQueueHandler.getJobsInRegion(filter);

		return data;
	}

	private static sked__Job__c updateContactAttempedJob(skedVivintModel.VivintJob job) {
		DateTime currentTime = System.now();
		sked__Job__c skedJob = new sked__Job__c(
			id = job.id,
			sked_Contact_Attempted_Time__c = currentTime
		);

		update skedJob;
		return skedJob;
	}

	private static skedRemoteResultModel getCloserGridData(skedVivintModel.CloserFilterData filter) {
		skedRemoteResultModel result = new skedRemoteResultModel();

		try {

			if (String.isBlank(filter.postalCode)) {
				result.errorMessage = skedConstants.ADDRESS_MISSING_ZIPCODE;
				result.success = false;
				return result;
			}

			List<sked__Region_Area__c> zipcodes = skedSetterHandler.getZipCodesFromName(filter.postalCode);

			if (zipcodes == null || zipcodes.isEmpty()) {
				result.errorMessage = skedConstants.ZIPCODE_NO_REGION;
				result.success = false;
				return result;
			}

			String regionId = 	zipcodes.get(0).sked__Region__c;
			String timezone = 	zipcodes.get(0).sked__Region__r.sked__Timezone__c;

			List<skedVivintModel.DaySlot> lstDaySlots = new List<skedVivintModel.DaySlot>();

            //get date range of grid
            Date startDate = Date.valueOf(filter.startDate);
            Date endDate = Date.valueOf(filter.endDate);
            DateTime startOfGrid = skedDateTimeUtils.getStartOfDate(startDate, timezone);
			DateTime endOfGrid = skedDateTimeUtils.getEndOfDate(endDate, timezone);

			DateTime tempTime = startOfGrid;

			//set jobLocation for timeslot
			List<skedVivintModel.TimeSlot> lstTimeSlots = skedCommonService.getListTimeSlotInDefinedTimeRange(startDate, endDate, new List<String>{regionId});
            for (skedVivintModel.TimeSlot slot : lstTimeSlots) {
				slot.jobLocation = Location.newInstance(filter.jobLocation.lat, filter.jobLocation.lng);
			}

			//get available resource in region
			skedModels.resource selectedRes = skedCommonservice.getResourceFromUserId(UserInfo.getUserId());
			Set<Id> setResIds = new Set<Id>{selectedRes.id};
			set<Date> setInputDates = new Set<Date>();

			DateTime tempStartDate = startOfGrid;
			Date tempDate = startDate;

			while (tempDate <= endDate) {
                setInputDates.add(tempDate);
                tempDate = tempDate.addDays(1);
            }

            System.debug('setInputDates ' + setInputDates);

			skedAvailatorParams params = new skedAvailatorParams();
            params.timezoneSidId = timezone;
            params.startDate = startDate;
            params.endDate = endDate;
            params.resourceIds = setResIds;
            params.inputDates = setInputDates;
            params.regionId = regionId;

            skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
            Map<Id, skedResourceAvailabilityBase.resourceModel> mapResource = resourceAvailability.initializeResourceList();

            //get day of week filter
            Set<String> setDaysOfWeek = new Set<String>();
            Set<String> setTimeOfDay = new Set<String>();

            //get config setting data
            Integer velocity = skedSetting.instance.Admin.velocity;
            boolean ignoreTravelTimeFirstJob = skedSetting.instance.Admin.ignoreTravelTimeFirstJob;

           	while (tempTime < endOfGrid) {
           		Date checkDate = skedDateTimeUtils.getDate(tempTime, timezone);

           		if (setInputDates.contains(checkDate)) {
                	skedVivintModel.DaySlot daySlot = new skedVivintModel.DaySlot(tempTime, timezone);
					List<skedVivintModel.TimeSlot> timeslots = new List<skedVivintModel.TimeSlot>(lstTimeSlots);

                    skedVivintModel.TimeSlotCheckingModel checkingData = new skedVivintModel.TimeSlotCheckingModel(timeslots,
                                                                                setTimeOfDay, timezone, tempTime, mapResource,
                                                                                daySlot.dayString, daySlot, velocity, ignoreTravelTimeFirstJob);
                    daySlot = skedSetterHandler.checkTimeSlotOfDateSlot(checkingData);

					if (!setDaysOfWeek.isEmpty()) {
						if (setDaysOfWeek.contains(daySlot.weekday)) {
                            lstDaySlots.add(daySlot);
						}
					}
					else {
                        System.debug('daySlot.dayString ' + daySlot.dayString);
						lstDaySlots.add(daySlot);
					}
                }
           		tempTime = skedDateTimeUtils.addDays(tempTime, 1, timezone);
			}

			result.data = lstDaySlots;
		}
		catch (Exception ex) {
			result.getError(ex);
		}

		return result;
	}
}
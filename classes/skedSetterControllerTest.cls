@isTest
public class skedSetterControllerTest {
    public static testmethod void  saveAppointmentMissingContactTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        Test.startTest();
        skedVivintModel.Appointment appointment = new skedVivintModel.Appointment();
        appointment.contactInfo = new skedVivintModel.VivintContact();
        skedRemoteResultModel result = skedSetterController.saveAppointment(appointment);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void  saveAppointmentMissingZipCodeTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();
        Test.startTest();
        skedVivintModel.Appointment appointment = new skedVivintModel.Appointment();
        appointment.contactInfo = new skedVivintModel.VivintContact();
        appointment.contactInfo.postalCode = '111111';
        skedRemoteResultModel result = skedSetterController.saveAppointment(appointment);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void  saveAppointmentTest1() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        Test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        String timezone = region.sked__Timezone__c;

        skedAvailatorParams params = new skedAvailatorParams();
        params.timezoneSidId = region.sked__Timezone__c;
        params.startDate = skedDateTimeUtils.getDate(System.now(), timezone);
        params.endDate = skedDateTimeUtils.getDate(System.now(), timezone);
        params.resourceIds = new Set<Id>{resource.id};
        params.inputDates = new Set<Date>{System.today(), System.today().addDays(1)};
        params.regionId = region.Id;

        skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
        Map<Id, skedResourceAvailabilityBase.resourceModel> mapResource = resourceAvailability.initializeResourceList();

        skedVivintModel.Appointment appointment = new skedVivintModel.Appointment();
        appointment.contactInfo = new skedVivintModel.VivintContact();
        sked__Region_Area__c regionArea = (sked__Region_Area__c)objFactory.getSObjects(sked__Region_Area__c.sObjectType).get(0);
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        appointment.contactInfo.postalCode = regionArea.sked_Zip_Code_Name__c;
        appointment.contactInfo.location = new skedModels.skedLocation(location);
        appointment.contactInfo.firstname = 'contact first name1';
        appointment.contactInfo.lastname = 'last name';
        appointment.contactInfo.areaInterestedIn = new List<String>{'Smart Assistant'};
        appointment.contactInfo.email = 'test@email.com';
        appointment.contactInfo.haveHighSpeedInternet = 'Yes';
        appointment.contactInfo.howInterestedInSH = '10';
        appointment.contactInfo.ownHomeOrBusiness = 'Yes';
        appointment.contactInfo.phone = '10';
        appointment.contactInfo.repId = '10';
        appointment.contactInfo.haveSecuritySystem = 'Yes';
        appointment.contactInfo.timeHaveSystem = '10';
        appointment.contactInfo.systemMonitored = 'Yes';
        appointment.contactInfo.setterName = 'Setter';
        appointment.contactInfo.decisionMaking = 'Yes';
        appointment.contactInfo.accountNumber = '10';

        appointment.jobInfo.jobDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        appointment.jobInfo.jobStart = 1200;
        appointment.jobInfo.jobFinish = 1500;
        appointment.jobInfo.address = '228 Park Ave S, New York';
        for (skedResourceAvailabilityBase.resourceModel res : mapResource.values()) {
            if (appointment.lstRes == null) {
                appointment.lstRes = new List<skedResourceAvailabilityBase.resourceModel>();
            }
            appointment.lstRes.add(res);
        }
        appointment.contactInfo.location.geoLocation = new skedModels.geometry(location.sked_GeoLocation__Latitude__s, location.sked_GeoLocation__Longitude__s);
        skedRemoteResultModel result = skedSetterController.saveAppointment(appointment);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void  saveAppointmentTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        Test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        String timezone = region.sked__Timezone__c;

        skedAvailatorParams params = new skedAvailatorParams();
        params.timezoneSidId = region.sked__Timezone__c;
        params.startDate = skedDateTimeUtils.getDate(System.now(), timezone);
        params.endDate = skedDateTimeUtils.getDate(System.now(), timezone);
        params.resourceIds = new Set<Id>{resource.id};
        params.inputDates = new Set<Date>{System.today(), System.today().addDays(1)};
        params.regionId = region.Id;

        skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
        Map<Id, skedResourceAvailabilityBase.resourceModel> mapResource = resourceAvailability.initializeResourceList();

        skedVivintModel.Appointment appointment = new skedVivintModel.Appointment();
        appointment.contactInfo = new skedVivintModel.VivintContact();
        sked__Region_Area__c regionArea = (sked__Region_Area__c)objFactory.getSObjects(sked__Region_Area__c.sObjectType).get(0);
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        appointment.contactInfo.postalCode = regionArea.sked_Zip_Code_Name__c;
        appointment.contactInfo.location = new skedModels.skedLocation(location);

        for (skedResourceAvailabilityBase.resourceModel res : mapResource.values()) {
            if (appointment.lstRes == null) {
                appointment.lstRes = new List<skedResourceAvailabilityBase.resourceModel>();
            }
            appointment.lstRes.add(res);
        }
        appointment.contactInfo.location.geoLocation = new skedModels.geometry(location.sked_GeoLocation__Latitude__s, location.sked_GeoLocation__Longitude__s);
        skedRemoteResultModel result = skedSetterController.saveAppointment(appointment);
        System.debug('result ' + result);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void lookupLocationTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        skedRemoteResultModel result = skedSetterController.lookupLocation(location.Name);
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void lookupLocationMissingNameTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        skedRemoteResultModel result = skedSetterController.lookupLocation(null);
        System.debug('result ' + result);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void getConfigDataTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedRemoteResultModel result = skedSetterController.getConfigData();
        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void loadBookingGridMissingPostalCodeTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.SetterBookingData filter = new skedVivintModel.SetterBookingData();

        skedRemoteResultModel result = skedSetterController.loadBookingGrid(filter);
        System.debug('result ' + result);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void loadBookingGridWrongPostalCodeTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        skedVivintModel.SetterBookingData filter = new skedVivintModel.SetterBookingData();
        filter.postalCode = 'sdfdfgfgdfg';
        skedRemoteResultModel result = skedSetterController.loadBookingGrid(filter);
        System.debug('result ' + result);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void loadBookingGridTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        String timezone = region.sked__Timezone__c;
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        sked__Region_Area__c regionArea = (sked__Region_Area__c)objFactory.getSObjects(sked__Region_Area__c.sObjectType).get(0);
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        job.sked__Start__c = skedDateTimeUtils.getStartOfDate(System.now(), timezone).addMinutes(480);
        job.sked__Finish__c = skedDateTimeUtils.getStartOfDate(System.now(), timezone).addMinutes(660);
        update job;

        sked__Job__c job2 = job.clone(false, false, false, false);
        job2.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;

        sked__Job__c job3 = job.clone(false, false, false, false);
        job3.sked__Start__c = job.sked__Start__c.addMinutes(480);
        job3.sked__Finish__c = job.sked__Finish__c.addMinutes(480);
        job3.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;

        insert new List<sked__job__c>{job2, job3};
        System.debug('job ' + job);
        System.debug('job2 ' + job2);
        System.debug('job3 ' + job3);

        sked__Job_Allocation__c ja2 = new sked__Job_Allocation__c(
            sked__Job__c = job2.id,
            sked__Resource__c = resource.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
        );

        sked__Job_Allocation__c ja3 = new sked__Job_Allocation__c(
            sked__Job__c = job3.id,
            sked__Resource__c = resource.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
        );

        insert new List<sked__Job_Allocation__c>{ja2, ja3};




        skedVivintModel.SetterBookingData filter = new skedVivintModel.SetterBookingData();

        filter.postalCode = regionArea.sked_Zip_Code_Name__c;
        filter.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        filter.jobLocation = new skedModels.geometry(location.sked_GeoLocation__Latitude__s, location.sked_GeoLocation__Longitude__s);

        skedRemoteResultModel result = skedSetterController.loadBookingGrid(filter);

        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void loadBookingGridTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        User u = createUser();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked__Resource__c resource2 = resource.clone(false, false, false, false);
        resource2.sked__UniqueKey__c = 'mail@mail.com';
        resource2.name = 'res 2';
        resource2.sked__User__c = u.id;
        resource2.Vivint_Employee_Id__c = 1238;
        insert resource2;
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        sked__Region_Area__c regionArea = (sked__Region_Area__c)objFactory.getSObjects(sked__Region_Area__c.sObjectType).get(0);
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        sked__Job__c job2 = job.clone(false, false, false, false);
        job2.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;
        sked__Job__c job3 = job.clone(false, false, false, false);
        job3.sked__Start__c = job.sked__Start__c.addMinutes(480);
        job3.sked__Finish__c = job.sked__Finish__c.addMinutes(480);
        job3.sked__Job_Status__c = skedConstants.JOB_STATUS_READY;

        insert new List<sked__job__c>{job2, job3};

        sked__Job_Allocation__c ja2 = new sked__Job_Allocation__c(
            sked__Job__c = job2.id,
            sked__Resource__c = resource.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
        );

        sked__Job_Allocation__c ja3 = new sked__Job_Allocation__c(
            sked__Job__c = job3.id,
            sked__Resource__c = resource2.id,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED
        );

        insert new List<sked__Job_Allocation__c>{ja2, ja3};



        String timezone = region.sked__Timezone__c;
        skedVivintModel.SetterBookingData filter = new skedVivintModel.SetterBookingData();

        filter.postalCode = regionArea.sked_Zip_Code_Name__c;
        filter.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        filter.jobLocation = new skedModels.geometry(location.sked_GeoLocation__Latitude__s, location.sked_GeoLocation__Longitude__s);
        filter.dateOfWeek = new List<String>{'Tue'};

        skedRemoteResultModel result = skedSetterController.loadBookingGrid(filter);

        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void loadBookingGridTest3() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked__Region__c region = (sked__Region__c)objFactory.getSObjects(sked__Region__c.sObjectType).get(0);
        sked__Resource__c resource = (sked__Resource__c)objFactory.getSObjects(sked__Resource__c.sObjectType).get(0);
        sked_Setter_Location__c location = (sked_Setter_Location__c)objFactory.getSObjects(sked_Setter_Location__c.sObjectType).get(0);
        sked__Region_Area__c regionArea = (sked__Region_Area__c)objFactory.getSObjects(sked__Region_Area__c.sObjectType).get(0);
        sked__Job__c job = (sked__Job__c)objFactory.getSObjects(sked__Job__c.sObjectType).get(0);
        String timezone = region.sked__Timezone__c;
        skedVivintModel.SetterBookingData filter = new skedVivintModel.SetterBookingData();
        List<skedVivintModel.TimeSlot> lstTimeSlots = skedSetting.instance.Admin.lstTimeSlot;
        filter.postalCode = regionArea.sked_Zip_Code_Name__c;
        filter.selectedDate = System.now().format(skedConstants.YYYY_MM_DD, timezone);
        filter.jobLocation = new skedModels.geometry(location.sked_GeoLocation__Latitude__s, location.sked_GeoLocation__Longitude__s);
        filter.timeOfDate = new List<String>{lstTimeSlots.get(0).id};

        skedRemoteResultModel result = skedSetterController.loadBookingGrid(filter);

        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void lookupAccessIdTest() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Setter__c setter = (sked_Setter__c)objFactory.getSObjects(sked_Setter__c.sObjectType).get(0);

        skedRemoteResultModel result = skedSetterController.lookupAccessId(setter.sked_Access_ID__c);

        System.debug('result ' + result);
        System.assert(result.success == true);
        test.stopTest();
    }

    public static testmethod void lookupAccessIdTest2() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

        test.startTest();
        sked_Setter__c setter = (sked_Setter__c)objFactory.getSObjects(sked_Setter__c.sObjectType).get(0);

        skedRemoteResultModel result = skedSetterController.lookupAccessId('result');

        System.debug('result ' + result);
        System.assert(result.success == false);
        test.stopTest();
    }

    public static testmethod void checkTestData() {
        skedSObjectFactoryExt objFactory = new skedSObjectFactoryExt();
        objFactory.init().create();

    }

    public static User createUser() {
        User u = new User(
             ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
             LastName = 'last',
             Email = 'puser000@amamama.com',
             Username = 'puser000@amamama.com' + System.currentTimeMillis(),
             CompanyName = 'TEST',
             Title = 'title',
             Alias = 'alias',
             TimeZoneSidKey = 'America/Los_Angeles',
             EmailEncodingKey = 'UTF-8',
             LanguageLocaleKey = 'en_US',
             LocaleSidKey = 'en_US'
        );
        insert u;
        return u;
    }
}
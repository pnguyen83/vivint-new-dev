public class skedJobAllocationHandler {
	public static void onAfterInsert(List<sked__Job_Allocation__c> skedJAs) {
		sendOutEmailNotification(skedJAs);
	}

	//====================================================Private Functions================================================//
	private static void sendOutEmailNotification(List<sked__Job_Allocation__c> newSkedJAs) {
		Map<id, sked__Job_Allocation__c> pendingDispatchJas = new Map<Id, sked__Job_Allocation__c>();
		Map<id, sked__Job_Allocation__c> confirmedJas = new Map<Id, sked__Job_Allocation__c>();
		Set<id> pendingDispatchJaIdsForSMS = new Set<Id>();
		List<sked__Job__c> updateSkedJobs = new List<sked__Job__c>();
		List<Messaging.Singleemailmessage> emailTemplates = new List<Messaging.Singleemailmessage>();
		Id orgWideEmailId = skedCommonService.getOrgWideEmail();
		skedSetting.skedAdminSetting setting = skedSetting.instance.admin;

		for (sked__Job_Allocation__c skedJA : newSkedJAs) {
			if (skedJA.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH) {
				if (skedJA.sked_Is_Job_Notification_sent_out__c == false) {
					pendingDispatchJas.put(skedJA.id, null);
					updateJobNotificationFlag(skedJA, updateSkedJobs);
				}
				pendingDispatchJaIdsForSMS.add(skedJA.id);
			}
			else if (skedJA.sked__Status__c == skedConstants.JOB_ALLOCATION_STATUS_CONFIRMED) {
				if (skedJA.sked_Is_Job_Notification_sent_out__c == false) {
					confirmedJas.put(skedJA.id, null);
					updateJobNotificationFlag(skedJA, updateSkedJobs);
				}
			}
		}

		for (sked__Job_Allocation__c skedJA : [SELECT Id, Name, sked__Job__c, sked__Job__r.sked__Contact__c, sked__Job__r.name,
												sked__Job__r.sked__Address__c, sked__Job__r.sked_Customer_Phone__c,
												sked__Job__r.sked__Start__c, sked__Job__r.sked__Finish__c, sked__Job__r.sked_Customer_Email__c,
												sked__Job__r.sked_Job_Start_Minutes__c, sked__Job__r.sked_Job_End_Minutes__c,
												sked__Job__r.sked__Contact__r.FirstName, sked__Status__c, sked__Job__r.sked__Timezone__c,
												sked__Job__r.sked__Contact__r.LastName, sked__Resource__r.sked__Email__c,
												sked__Job__r.sked__Contact__r.Email,
												sked__Resource__r.Name, sked__Resource__r.sked__Primary_Phone__c, sked__Resource__r.sked__User__c
												FROM sked__Job_Allocation__c
												WHERE Id IN : pendingDispatchJas.keySet()
												OR Id IN : confirmedJas.keySet()]) {
			if (pendingDispatchJas.containsKey(skedJA.id)) {
				pendingDispatchJas.put(skedJA.id, skedJA);
			}
			else if (confirmedJas.containsKey(skedJA.id)) {
				confirmedJas.put(skedJA.id, skedJA);
			}

		}

		if (setting.enableConfirmationEmail) {
			for (sked__Job_Allocation__c skedJA : pendingDispatchJas.values()) {
				if (skedJA.sked__Job__r.sked__Start__c != null && skedJA.sked__Job__r.sked__Finish__c != null &&
					skedJA.sked__Job__r.sked__Contact__c != null &&
					skedCommonService.validateEmail(skedJA.sked__Job__r.sked__Contact__r.Email) &&
					skedCommonService.validateEmail(skedJA.sked__Resource__r.sked__Email__c)) {
					skedVivintModel.SendOutEmailModel data = new skedVivintModel.SendOutEmailModel(skedJA.sked__Job__r.sked__Start__c, skedJA.sked__Job__r.sked__Finish__c,
																	skedJA.sked__Resource__r.Name, skedJA.sked__Resource__r.sked__Primary_Phone__c,
																	skedJA.sked__Job__r.sked__Timezone__c, skedJA.sked__Job__r.sked__Contact__r.FirstName,
																	skedJA.sked__Job__r.sked__Address__c, skedJA.sked__Job__r.sked__Contact__r.Email,
																	skedConstants.CONFIRMATION_APPOINTMENT_EMAIL);
					data.orgWideEmailId = orgWideEmailId;
					Messaging.Singleemailmessage email = skedCloserHandler.sendOutEmail(data);
					email.setReplyTo(skedJA.sked__Resource__r.sked__Email__c);

					emailTemplates.add(email);
				}

			}

			//send notification to resources

		}

		//send rescheduled notification email
		if (setting.enableRescheduledEmail) {
			for (sked__Job_Allocation__c skedJA : confirmedJas.values()) {
				if (skedJA.sked__Job__r.sked__Start__c != null && skedJA.sked__Job__r.sked__Finish__c != null &&
					skedJA.sked__Job__r.sked__Contact__c != null &&
					skedCommonService.validateEmail(skedJA.sked__Job__r.sked__Contact__r.Email) &&
					skedCommonService.validateEmail(skedJA.sked__Resource__r.sked__Email__c)) {
					skedVivintModel.SendOutEmailModel data = new skedVivintModel.SendOutEmailModel(skedJA.sked__Job__r.sked__Start__c, skedJA.sked__Job__r.sked__Finish__c,
																	skedJA.sked__Resource__r.Name, skedJA.sked__Resource__r.sked__Primary_Phone__c,
																	skedJA.sked__Job__r.sked__Timezone__c, skedJA.sked__Job__r.sked__Contact__r.FirstName,
																	skedJA.sked__Job__r.sked__Address__c, skedJA.sked__Job__r.sked__Contact__r.Email,
																	skedConstants.RESCHEDULED_APPOINTMENT_EMAIL);
					data.orgWideEmailId = orgWideEmailId;
					Messaging.Singleemailmessage email = skedCloserHandler.sendOutEmail(data);
					email.setReplyTo(skedJA.sked__Resource__r.sked__Email__c);
					emailTemplates.add(email);
				}

			}
		}

		if (!emailTemplates.isEmpty()) {
			Messaging.sendEmail(emailTemplates);
			update updateSkedJobs;
		}

		if (!pendingDispatchJaIdsForSMS.isEmpty() && !System.isBatch() && !System.isFuture() && !System.isScheduled()) {
			skedSmsServices.sendNotificationToResourceFromJA(pendingDispatchJaIdsForSMS);
		}
	}

	private static void updateJobNotificationFlag(sked__Job_Allocation__c skedJA, List<sked__Job__c> updateSkedJobs) {
		sked__Job__c skedJob = new sked__Job__c(
						id = skedJA.sked__Job__c,
						sked_Is_notification_sent_out__c = true
					);
		updateSkedJobs.add(skedJob);
	}
}
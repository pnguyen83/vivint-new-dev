public class skedHandleUnallocatedJobUtils {
    public List<sked__Slot__c> getListValidSlotForBatch() {
        Date today = System.today();
        String job_status_pending_allocation = skedConstants.JOB_STATUS_PENDING_ALLOCATION;
        String job_status_cancelled = skedConstants.JOB_STATUS_CANCELLED;
        String job_result_sold_scheduled = skedConstants.JOB_RESULT_SOLD_SCHEDULED;
        String job_result_no_sales = 'Demo - No Sale' + '%';

        List<sked__Slot__c> result = new List<sked__Slot__c>();

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Slot__c.sObjectType).filter('sked_Active_End_Date__c = null OR sked_Active_End_Date__c >= :today');
        selector.filter('sked_Active_Start_Date__c = null OR sked_Active_Start_Date__c <= :today');
        selector.subQuery('sked_Jobs__r')
                .filter('sked__Job_Status__c =: job_status_pending_allocation OR sked__Job_Status__c =:job_status_cancelled OR (sked__Job_Allocation_Count__c = 0 AND (sked_Demo_Result__c =:job_result_sold_scheduled OR sked_Demo_Result__c LIKE :job_result_no_sales))')
                .sort('Name');

        List<sked__Slot__c> skedSlots = new List<sked__Slot__c>();
        skedSlots = Database.query(selector.getQuery());

         Map<id, skedVivintModel.TimeSlot> map_id_slot = checkSlotAvailableWithParallelJob(skedSlots);

        for (sked__Slot__c skedSlot : skedSlots) {
            if (skedSlot.sked_Market__c == null) {
                continue;
            }
            if (map_id_slot.containsKey(skedSlot.id)) {
                skedVivintModel.TimeSlot slot = map_id_slot.get(skedSlot.id);
                string timezone = skedSlot.sked_Market__r.sked__Timezone__c;
                String weekday = system.now().format(skedConstants.WEEKDAY, timezone);
                System.debug('4 slot ' + slot.name + ' isAvai ' + slot.isAvai);
                if (weekday.equalsIgnoreCase(skedSlot.sked_Day__c) && slot.isAvai) {
                    System.debug('5 slot ' + slot.name + ' slot.id ' + slot.id);
                    result.add(skedSlot);
                }
            }
        }

        return result;
    }

    public void allocateResourceToJob(sked__Slot__c skedSlot, Map<Id, skedResourceAvailabilityBase.resourceModel> map_id_avaiResAllJobs) {
        if (skedSlot.sked_Jobs__r != null) {
            Map<id,skedVivintModel.Timeslot> map_id_slot = getTimeSlotInformation(new List<sked__Slot__c>{skedSlot});
            skedVivintModel.Timeslot slot = map_id_slot.get(skedSlot.id);
            List<sked__Job_Allocation__c> skedJAs = new List<sked__Job_Allocation__c>();
            List<sked__Job__c> skedJobs = new List<sked__Job__c>();
            string regionId = skedSlot.sked_Market__c;
            Set<id> resIds = skedCommonService.getResourceIdsInRegion(regionId);
            Integer velocity = skedSetting.instance.Admin.velocity;
            Integer maxNoOfJob = skedSlot.sked_Number_of_Parallel_Bookings_Pending__c != null ? (Integer)skedSlot.sked_Number_of_Parallel_Bookings_Pending__c : 0;

            skedAvailatorParams params = new skedAvailatorParams();
            params.timezoneSidId = skedSlot.sked_Market__r.sked__Timezone__c;
            params.startDate = System.today();
            params.endDate = System.today();
            params.resourceIds = resIds;
            params.inputDates = new Set<Date>{System.today()};
            params.regionId = regionId;

            skedResourceAvailability resourceAvailability = new skedResourceAvailability(params);
            Map<Id, skedResourceAvailabilityBase.resourceModel> mapResources = resourceAvailability.initializeResourceList();

            Map<id, Allocation> map_id_cancelledJob = new Map<id, Allocation>();
            Map<id, Allocation> map_id_completedJob = new Map<id, Allocation>();
            Map<id, Allocation> map_id_pendingJob = new Map<id, Allocation>();

            Date today = System.today();

            for (sked__job__c skedJob : skedSlot.sked_Jobs__r) {
                DateTime startOfDate = skedDateTimeUtils.getStartOfDate(today, skedJob.sked__Timezone__c);
                DateTime endOfDate = skedDateTimeUtils.getEndOfDate(today, skedJob.sked__Timezone__c);
                if (skedJob.sked__Start__c <= endOfDate && skedJob.sked__Finish__c >= startOfDate) {
                    Allocation job = new Allocation(skedJob, mapResources, velocity);
                    if (skedConstants.JOB_STATUS_CANCELLED.equalsIgnoreCase(job.jobStatus)) {
                        map_id_cancelledJob.put(skedJob.id, job);
                    }
                    else if (job.jobStatus.containsIgnoreCase(skedConstants.JOB_DEMO_NO_SALE)) {
                        map_id_completedJob.put(skedJob.id, job);
                    }
                    else {
                        map_id_pendingJob.put(skedJob.id, job);
                    }
                }
            }

            Map<id, Allocation> map_id_readyToAllocateJob = new Map<id, Allocation>();
            Map<id, Allocation> map_id_unAllocatedJob = new Map<id, Allocation>();

            //get suitable resource for cancelled job
            checkAndAllocateJob(map_id_cancelledJob.values(), map_id_readyToAllocateJob, map_id_unAllocatedJob, map_id_avaiResAllJobs, slot);
            //get suitable resource for complete No sales or Sold:Scheduled job
            checkAndAllocateJob(map_id_completedJob.values(), map_id_readyToAllocateJob, map_id_unAllocatedJob, map_id_avaiResAllJobs, slot);
            //get suitable resource for pending allocation job
            checkAndAllocateJob(map_id_pendingJob.values(), map_id_readyToAllocateJob, map_id_unAllocatedJob, map_id_avaiResAllJobs, slot);

            if (!map_id_readyToAllocateJob.isEmpty()) {
                for (Allocation job : map_id_readyToAllocateJob.values()) {
                    sked__Job_Allocation__c skedJA = skedSetterHandler.createJobAllocation(job.jobId, job.resourceId);
                    if (skedJA != null) {
                        skedJAs.add(skedJA);
                    }

                    sked__Job__c skedJob = new sked__Job__c(
                        id = job.jobId,
                        sked__Job_Status__c = skedConstants.JOB_STATUS_PENDING_DISPATCH,
                        sked_Is_notification_sent_out__c = false
                    );
                    skedJobs.add(skedJob);
                }
            }

            if (!map_id_unAllocatedJob.isEmpty()) {
                skedObjectSelector selector = skedObjectSelector.newInstance(sked_Queue_Management_Access__c.sObjectType).filter('sked_Region__c =:regionId').filter('sked_Primary__c = true');
                List<sked_Queue_Management_Access__c> qmas = new List<sked_Queue_Management_Access__c>();
                String token = skedCommonService.getAPIToken();
                qmas = Database.query(selector.getQuery());
                String slotStartTime = skedSlot.sked_Start_Time__c != null ? skedDateTimeUtils.convertTimeNumberToTimeLabel((Integer)skedSlot.sked_Start_Time__c) : '';
                String slotEndTime = skedSlot.sked_End_Time__c != null ? ' - ' + skedDateTimeUtils.convertTimeNumberToTimeLabel((Integer)skedSlot.sked_End_Time__c) : '';
                String slotName = slotStartTime + slotEndTime;
                for (sked_Queue_Management_Access__c qma : qmas) {
                    sendNotificationSMS(qma, map_id_unAllocatedJob.size(), slotName, token);
                }
            }

            if (!skedJobs.isEmpty()) {
                update skedJobs;
            }

            if (!skedJAs.isEmpty()) {
                insert skedJAs;
            }
        }
    }

    public Messaging.Singleemailmessage createNotificationEmail(String emailAddress, String subject, String emailBody, String token) {
        Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();
        email.setToAddresses(new List<String>{emailAddress});
        email.setSaveAsActivity(false);
        email.setSubject(subject);
        email.setHtmlBody(emailBody);

        return email;
    }

    public void sendNotificationSMS(sked_Queue_Management_Access__c qma, Integer quantity, string slotName, String token) {
        string smsContent = Skedulo_SMS__c.getOrgDefaults().sked_Auto_Allocation_Notification_SMS__c;
        smsContent = smsContent.replace('[QUANTITY]', String.valueOf(quantity));
        smsContent = smsContent.replace('[SLOT_NAME]', slotName);
        System.debug('sms ' + slotName);
        if (String.isNotBlank(smsContent)) {

            skedVivintModel.NotifyRequest request = new skedVivintModel.NotifyRequest(qma, smsContent, 'sms');
            String jsonBody = JSON.serialize(request);
            skedSkeduloAPI.sendOneOffMessage(token, jsonBody);
        }
        else {
            throw new skedException('Missing Auto Allocation Notification SMS field value in Skedulo SMS custom setting');
        }
    }

    public void checkAndAllocateJob(List<Allocation> skedJobs, Map<id,Allocation> map_id_job, Map<id, Allocation> map_id_unAllocatedJobs,
                                        Map<Id, skedResourceAvailabilityBase.resourceModel> map_id_avaiResAllJobs,
                                        skedVivintModel.Timeslot slot) {
        for (Allocation job : skedJobs) {
            for (skedResourceAvailabilityBase.resourceModel resource : job.map_id_waitingRes.values()) {
                checkResourceAvailabilityWithJob(job, resource, map_id_avaiResAllJobs, slot);
            }

            if (getSuitableResource(job, map_id_avaiResAllJobs, slot)) {
                map_id_job.put(job.jobId, job);
            }
            else {
                map_id_unAllocatedJobs.put(job.jobId, job);
            }

        }
    }

    public boolean getSuitableResource(Allocation job, Map<Id, skedResourceAvailabilityBase.resourceModel> map_id_avaiResAllJobs,
                                        skedVivintModel.Timeslot slot) {
        boolean result = false;
        skedResourceAvailabilityBase.resourceModel suitableRes;
        List<skedResourceAvailabilityBase.resourceModel> lstRes = new List<skedResourceAvailabilityBase.resourceModel>(job.map_id_AvailableRes.values());
        lstRes.sort();

        for (skedResourceAvailabilityBase.resourceModel resource : lstRes) {
            skedResourceAvailabilityBase.resourceModel res = map_id_avaiResAllJobs.get(resource.id);
            Location resHome = Location.newInstance(res.address.geometry.lat, res.address.geometry.lng);
            res.distanceToJob = skedSetterHandler.calculateDistance(resHome, job.jobLocation);
            res.distanceToJob = res.distanceToJob == null ? 0 : res.distanceToJob;

            if (slot.pendingJobPerSlot >= slot.maxPendingJobPerSlot ||
                slot.readyJobPerSlot >= slot.maxReadyJobPerSlot ||
                (slot.pendingJobPerSlot + slot.readyJobPerSlot) >= slot.maxPendingJobPerSlot) {
                result = false;
            }
            else {
                if (suitableRes == null) {
                    suitableRes = res;
                }
                else {
                    if (suitableRes.noOfJobsInSlot >= res.noOfJobsInSlot) {
                        if (suitableRes.noOfJobsInSlot == res.noOfJobsInSlot) {
                            if (suitableRes.distanceToJob > res.distanceToJob) {
                                suitableRes = res;
                            }
                        }
                        else {
                            suitableRes = res;
                        }
                    }
                }
            }
        }

        if (suitableRes != null) {
            System.debug('suitableRes ' + suitableRes.name + ' for job ' + job.jobname);
            job.resourceId = suitableRes.id;
            suitableRes.noOfJobsInSlot++;
            skedResourceAvailabilityBase.resourceModel finalRes = suitableRes.deepClone();
            finalRes.noOfJobsInSlot = suitableRes.noOfJobsInSlot;
            map_id_avaiResAllJobs.put(suitableRes.id, finalRes);
            result = true;
            slot.pendingJobPerSlot++;
        }

        return result;
    }

    public void checkResourceAvailabilityWithJob(Allocation job, skedResourceAvailabilityBase.resourceModel res,
                                                    Map<Id, skedResourceAvailabilityBase.resourceModel> map_id_avaiResAllJobs,
                                                    skedVivintModel.Timeslot slot) {
        boolean isAvai = true;
        res.noOfPendingJob = 0;
        res.noOfReadyJob = 0;
        skedResourceAvailabilityBase.dateslotModel daySlotModel =  res.mapDateslot.get(job.jobDate);
        if (daySlotModel != null) {
            for (skedModels.Event event : daySlotModel.events) {
                if (event.isAvailable == true && isAvai) {
                    if (event.start < job.jobStart && event.finish > job.jobStart) {
                        isAvai = false;
                    }
                }
                else if (event.isAvailable == false && isAvai) {
                    if (event.start < job.jobFinish  && event.finish > job.jobStart) {
                        if (event.objectType != 'jobAllocation') {
                                isAvai = false;
                        }
                        else {
                            skedModels.joballocation ja = (skedModels.joballocation)event;
                            if (skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH.equalsIgnoreCase(ja.status)) {
                                res.noOfPendingJob++;
                                res.noOfJobsInSlot++;
                            }
                            else if (skedConstants.READY_JA_STATUS.contains(ja.status)) {
                                res.noOfReadyJob++;
                                res.noOfJobsInSlot++;
                            }
                        }
                    }
                }
            }

            if (res.noOfPendingJob >= slot.maxPendingJobPerSlot ||
                res.noOfReadyJob >= slot.maxReadyJobPerSlot ||
                ((res.noOfPendingJob + res.noOfReadyJob) >= slot.maxPendingJobPerSlot)) {
                isAvai = false;
            }

            if (isAvai) {
                isAvai = skedSetterHandler.checkTravelTimeOfResourceStraightLine(job.jobLocation, job.jobStart, job.jobFinish,
                                                                                    daySlotModel.events, res, job.velocity);
            }

            if (isAvai) {
                job.map_id_AvailableRes.put(res.id, res);
                if (!map_id_avaiResAllJobs.containsKey(res.id)) {
                    map_id_avaiResAllJobs.put(res.id, res);
                }
            }
        }
    }

    public class Allocation {
        public string jobId;
        public string jobName;
        public DateTime jobStart;
        public DateTime jobFinish;
        public string resourceId;
        public string jobStatus;
        public string timezone;
        public string jobDate;
        public Integer velocity;
        public Map<Id, skedResourceAvailabilityBase.resourceModel> map_id_waitingRes;
        public Map<Id, skedResourceAvailabilityBase.resourceModel> map_id_AvailableRes;
        public Location jobLocation;
        public skedModels.geometry jobGeometry;

        public Allocation(sked__Job__c skedJob, Map<Id, skedResourceAvailabilityBase.resourceModel> mapRes, Integer velocity) {
            this.jobId = skedJob.id;
            this.jobName = skedJob.name;
            this.jobStart = skedJob.sked__Start__c;
            this.jobFinish = skedJob.sked__Finish__c;
            this.jobStatus = skedJob.sked__Job_Status__c;
            this.timezone = skedJob.sked__Timezone__c;
            this.velocity = velocity;
            this.jobDate = skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, this.timezone);
            this.map_id_AvailableRes = new Map<Id, skedResourceAvailabilityBase.resourceModel>();
            this.jobLocation = Location.newInstance(skedJob.sked__GeoLocation__latitude__s, skedJob.sked__GeoLocation__longitude__s);
            this.jobGeometry = new skedModels.geometry(skedJob.sked__GeoLocation__latitude__s, skedJob.sked__GeoLocation__longitude__s);
            this.map_id_waitingRes = new Map<Id, skedResourceAvailabilityBase.resourceModel>();

            for (skedResourceAvailabilityBase.resourceModel res : mapRes.values()) {
                res.noOfJobsInSlot = res.noOfJobsInSlot != null ? res.noOfJobsInSlot : 0;
                this.map_id_waitingRes.put(res.id, res);
            }
        }
    }

    public Map<id, skedVivintModel.TimeSlot> checkSlotAvailableWithParallelJob(List<sked__Slot__c> skedSlots) {
        Map<id, skedVivintModel.TimeSlot> map_id_slot = getTimeSlotInformation(skedSlots);

        for (skedVivintModel.TimeSlot slot : map_id_slot.values()) {
            slot.isAvai = true;
            if (slot.pendingJobPerSlot >= slot.maxPendingJobPerSlot ||
                slot.readyJobPerSlot >= slot.maxReadyJobPerSlot ||
                (slot.pendingJobPerSlot + slot.readyJobPerSlot) >= slot.maxPendingJobPerSlot) {
                slot.isAvai = false;
            }
        }


        return map_id_slot;
    }

    public Map<id, skedVivintModel.Timeslot> getTimeSlotInformation(List<sked__Slot__c> skedSlots) {
        Map<id, skedVivintModel.Timeslot> map_id_timeslots = new Map<id, skedVivintModel.Timeslot>();

        for (sked__Slot__c skedSlot : skedSlots) {
            skedVivintModel.Timeslot slot = new skedVivintModel.Timeslot(skedSlot);
            slot.maxPendingJobPerSlot = skedSlot.sked_Number_of_Parallel_Bookings_Pending__c;
            slot.maxReadyJobPerSlot = skedSlot.sked_Number_of_Parallel_bookings_Ready__c;
            map_id_timeslots.put(skedSlot.id, slot);
        }

        Set<id> slotIds = new Set<id>(map_id_timeslots.keySet());

        String job_status_cancelled = skedConstants.JOB_STATUS_CANCELLED;
        String job_Status_completed = skedConstants.JOB_STATUS_COMPLETE;
        String job_status_pending_alocation = skedConstants.JOB_STATUS_PENDING_ALLOCATION;

        Date currentDate = System.today();

        List<sked__Job__c> skedJobs = new List<sked__Job__c>();

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
        selector.filter('sked_Slot__c IN :slotIds');
        selector.filter('sked__Job_Status__c !=: job_status_cancelled');
        selector.filter('sked__Job_Status__c !=: job_Status_completed');
        selector.filter('sked__Job_Status__c !=: job_status_pending_alocation');

        skedJobs = Database.query(selector.getQuery());

        for (sked__job__c skedJob : skedJobs) {
            if (map_id_timeslots.containsKey(skedJob.sked_Slot__c)) {
                skedVivintModel.Timeslot slot = map_id_timeslots.get(skedJob.sked_Slot__c);
                DateTime chekingDateTime = skedDateTimeUtils.getStartOfDate(currentDate, skedJob.sked__Timezone__c);
                String checkingDate = chekingDateTime.format(skedConstants.YYYY_MM_DD, skedJob.sked__Timezone__c);
                String jobDate = skedJob.sked__Start__c.format(skedConstants.YYYY_MM_DD, skedJob.sked__Timezone__c);
                if (checkingDate.equalsIgnoreCase(jobDate) &&
                        skedJob.sked_Job_Start_Minutes__c == slot.startMinutes &&
                        skedJob.sked_Job_End_Minutes__c == slot.endMinutes) {
                    if (skedJob.sked__Job_Status__c.contains('Pending')) {
                        slot.pendingJobPerSlot++;
                    }
                    else {
                        slot.readyJobPerSlot++;
                    }
                }
                map_id_timeslots.put(skedJob.sked_Slot__c, slot);
            }
        }

        return map_id_timeslots;
    }
}
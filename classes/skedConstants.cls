public class skedConstants {
    public static final string ACCCOUNT_SCORE_BLACKLIST = 'Do not schedule';
    public static final string ACCCOUNT_SCORE_WHITELIST = 'Preferred';

    public static final string AVAILABILITY_STATUS_APPROVED = 'Approved';
    public static final string AVAILABILITY_STATUS_PENDING = 'Pending';
    public static final string AVAILABILITY_STATUS_DECLINED = 'Declined';

    public static final string JOB_CANCELLATION_REASON_RESCHEDULED = 'Rescheduled';

    public static final string JOB_STATUS_CANCELLED = 'Cancelled';
    public static final string JOB_STATUS_COMPLETE = 'Complete';
    public static final string JOB_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_STATUS_IN_PROGRESS = 'In Progress';
    public static final string JOB_STATUS_PENDING_ALLOCATION = 'Pending Allocation';
    public static final string JOB_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_STATUS_QUEUED = 'Queued';
    public static final string JOB_STATUS_READY = 'Ready';
    public static final string JOB_STATUS_ON_SITE = 'On Site';
    public static final string JOB_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_CONTACT_ATTEMPTED = 'Contact Attempted';

    public static final string JOB_TYPE_DEFAULT = 'In-Home Consultation';

    public static final string JOB_DEMO_NO_SALE = 'Demo - No Sale';

    public static final string JOB_RESULT_SOLD_SCHEDULED = 'Sold: Scheduled';

    //Slot Type
    public static final string SLOT_UNALLOCATED_TYPE = 'Unallocated';
    public static final string SLOT_RESOURCE_TYPE = 'Resource';

    public static final string JOB_ALLOCATION_STATUS_COMPLETE = 'Complete';
    public static final string JOB_ALLOCATION_STATUS_CONFIRMED = 'Confirmed';
    public static final string JOB_ALLOCATION_STATUS_EN_ROUTE = 'En Route';
    public static final string JOB_ALLOCATION_STATUS_CHECKED_IN = 'Checked In';
    public static final string JOB_ALLOCATION_STATUS_DECLINED = 'Declined';
    public static final string JOB_ALLOCATION_STATUS_DELETED = 'Deleted';
    public static final string JOB_ALLOCATION_STATUS_DISPATCHED = 'Dispatched';
    public static final string JOB_ALLOCATION_STATUS_PENDING_DISPATCH = 'Pending Dispatch';
    public static final string JOB_ALLOCATION_STATUS_IN_PROGRESS = 'In Progress';

    public static final Set<String> READY_JA_STATUS = new Set<String>{JOB_ALLOCATION_STATUS_CONFIRMED, JOB_ALLOCATION_STATUS_EN_ROUTE,
                                                                       JOB_ALLOCATION_STATUS_CHECKED_IN, JOB_ALLOCATION_STATUS_DISPATCHED,
                                                                         JOB_ALLOCATION_STATUS_IN_PROGRESS};

    public static final Set<String> RES_STATUS_USE_HOME_ADDRESS = new Set<String>{JOB_ALLOCATION_STATUS_CONFIRMED,JOB_ALLOCATION_STATUS_EN_ROUTE,
                                                                        JOB_ALLOCATION_STATUS_IN_PROGRESS, JOB_ALLOCATION_STATUS_COMPLETE};

    public static final string RESOURCE_TYPE_PERSON = 'Person';
    public static final string RESOURCE_TYPE_ASSET = 'Asset';

    public static final string HOLIDAY_GLOBAL = 'global';

    //format day label on setter booking modal
    public static final string WEEKDAY_DAY_MONTH = 'EEE d MMM';
    public static final string WEEKDAY = 'EEE';
    public static final string WEEKDAY_DAY_MONTH_YEAR = 'EEEEE d MMM yyyy';
    public static final string HOUR_ONLY = 'ha';
    public static final String HOUR_MINUTES_ONLY = 'h:mma';
    public static final String YYYY_MM_DD = 'yyyy-MM-dd';
    public static final String MM_DD_YY_HH_MM = 'MM/dd/yy h:mma';
    public static final String MM_DD_YY_HH_MM_TIMEZONE = 'MM/dd/yy h:mma z';
    public static final String DATE_TIME_IN_QUERY = 'yyyy-MM-dd\'T\'hh:mm:ss\'Z\'';

    //error message when Access Id is not available
    public static final string ACCESS_ID_NOT_AVAI = 'Invalid Access Id or User is in-active';

    //error message when location is missing geo location
    public static final string LOCATION_MISSING_GEOMETRY = 'The selected address is missing geometry information';

    public static final String USER_NOT_ASSOCIATED_WITH_RES = 'The current login user is not associated with any resource';

    public static final String PROSPECT_MISSING_EMAIL = 'Contact of selected Job is missing email address';

    public static final String INVALID_JOB_ID = 'Invalid Job Id';

    public static final String INVALID_EMAIL = 'Invalid Prospect Email';

    public static final String ERROR_RESCHEDULE_COMPLETE_JOB = 'Cannot reschedule a Complete job';

    public static final String MISSING_RESCHEDULE_DAY = 'Missing Rescheduled Day';

    public static final String ASSOCIATED_RESOURCE_NOT_ACTIVE = 'The associated resource of current login user is not active';

    public static final String NO_ACTIVE_RESOURCE = 'There is not any active resource in selected regions';

    public static final String ADDRESS_MISSING_ZIPCODE = 'Zipcode is missing';

    public static final String MISSING_RESOURCE_LIST = 'Missing resource list';

    public static final String MISSING_DAY_LIST = 'Missing list of selected days';

    public static final String CLOSER_ACCESS_MANAGER_PAGE = 'You do not have enough permission to access this page';

    public static final String ZIPCODE_NO_REGION = 'This zip code is currently not associated with a Region. Please contact your administrator for assistance.';

    //Email template developer name
    public static final String CONFIRMATION_APPOINTMENT_EMAIL = 'skedConfirmationEmail';
    public static final String RESCHEDULED_APPOINTMENT_EMAIL = 'skedRescheduledAppointmentTemplate';
    public static final String REMINDER_APPOINTMENT_EMAIL = 'skedAppointmentReminderEmailTemplate';
    public static final String UNALLOCATED_EMAIL = 'skedUnallocatedEmail';

    //Email Template error
    public static final String MISSING_EMAIL_TEMPLATE = 'Missing Email Template';

    //Availability Notification Email Template
    public static final String AVAILABILITY_EMAIL_TEMPLATE = '[Resource Name] will be [Status] from [Start] to [End]';

    //Unallocated Job Notification Email Subject
    public static final String UNALLOCATED_JOBS_EMAIL_SUBJECT = 'Unallocated Jobs on ';

    //No of unallocated job of a slot error message
    public static final String SLOT_UNALLOCATED_JOB_ERROR = 'Could not create any more Pending Allocation Job for this Slot';

    //Check Primary on Queue Management Access
    public static final String QMA_DUPLICATE_PRIMARY_ERROR = 'Only one Primary Queue Management Access can be added for a region';

    //Org wide email address
    public static final String ORG_WIDE_EMAIL_ADDRESS = 'in-homeconsultation@vivint.com';
}
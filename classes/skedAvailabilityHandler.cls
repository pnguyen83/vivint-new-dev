public class skedAvailabilityHandler {
    public static void onBeforeInsert(List<sked__Availability__c> newSkedAvais) {
        updateAppoveField(newSkedAvais);
    }

    public static void onBeforeUpdate(List<sked__Availability__c> newSkedAvais,
                                        Map<id, sked__Availability__c> map_old_avais) {
        updateAppoveField(newSkedAvais);
    }

    public static void onAfterInsert(List<sked__Availability__c> newSkedAvais) {
        sendNotificationToManager(newSkedAvais, null);
    }

    public static void onAfterUpdate(List<sked__Availability__c> newSkedAvais, Map<id, sked__Availability__c> map_old_avais) {
        sendNotificationToManager(newSkedAvais, map_old_avais);
    }

    //====================================Private Functions=========================//
    private static void updateAppoveField(List<sked__Availability__c> newSkedAvais) {
        Set<String> status = new Set<String>{skedConstants.AVAILABILITY_STATUS_APPROVED, skedConstants.AVAILABILITY_STATUS_DECLINED};
        for (sked__Availability__c skedAvai : newSkedAvais) {
            if (!status.contains(skedAvai.sked__Status__c)) {
                skedAvai.sked__Status__c = skedConstants.AVAILABILITY_STATUS_APPROVED;
            }
        }
    }

    private static void sendNotificationToManager(List<sked__Availability__c> newSkedAvais, Map<id, sked__Availability__c> map_old_avais) {
        Map<String, List<String>> map_regionId_managerEmails = new Map<String, List<String>>();

        for (sked__Availability__c skedAvai : newSkedAvais) {
            map_regionId_managerEmails.put(skedAvai.sked_Resource_Region__c, new List<String>());
        }

        for (sked_Queue_Management_Access__c skedQMA : [SELECT id, sked_Region__c, sked_Resource__r.sked__Email__c
                                                        FROM sked_Queue_Management_Access__c
                                                        WHERE sked_Region__c IN :map_regionId_managerEmails.keySet()
                                                        AND sked_Resource__r.sked__Is_Active__c = true]) {
            List<String> emails = map_regionId_managerEmails.get(skedQMA.sked_Region__c);
            emails = emails != null ? emails : new List<String>();
            if (skedCommonService.validateEmail(skedQMA.sked_Resource__r.sked__Email__c)) {
                emails.add(skedQMA.sked_Resource__r.sked__Email__c);
            }
            map_regionId_managerEmails.put(skedQMA.sked_Region__c, emails);
        }
        System.debug('map_regionId_managerEmails ' + map_regionId_managerEmails);
        List<Messaging.Singleemailmessage> emails = new List<Messaging.Singleemailmessage>();
        String emailBody = skedSetting.instance.Admin.avaiNotificationTemplate;
        emailBody = String.isBlank(emailBody) ? skedConstants.AVAILABILITY_EMAIL_TEMPLATE : emailBody;

        for (sked__Availability__c skedAvai : newSkedAvais) {
            if (map_regionId_managerEmails.containsKey(skedAvai.sked_Resource_Region__c)) {
                List<String> managerEmails = map_regionId_managerEmails.get(skedAvai.sked_Resource_Region__c);

                if (!managerEmails.isEmpty()) {
                    if (map_old_avais != null && map_old_avais.containsKey(skedAvai.id)) {
                        sked__Availability__c oldAvai = map_old_avais.get(skedAvai.id);
                        if (oldAvai.sked__Is_Available__c != skedAvai.sked__Is_Available__c) {
                            Messaging.Singleemailmessage email = generateEmailBody(skedAvai, emailBody, managerEmails);
                            emails.add(email);
                        }
                    }
                    else {
                        Messaging.Singleemailmessage email = generateEmailBody(skedAvai, emailBody, managerEmails);
                        emails.add(email);
                    }
                }
            }
        }
        System.debug('emails ' + emails);
        if (!emails.isEmpty()) {
            Messaging.SendEmailResult[] result = Messaging.sendEmail(emails);
            System.debug('send email result ' + result);
        }
    }

    private static Messaging.Singleemailmessage generateEmailBody(sked__Availability__c skedAvai, String emailBody,
                                                                        List<String> emailAddresss) {
        String timezone = skedAvai.sked_Resource_Timezone__c;
        Messaging.Singleemailmessage email = new Messaging.Singleemailmessage();

        if (emailBody.containsIgnoreCase('[Resource Name]')) {
            emailBody = emailBody.replace('[Resource Name]', skedAvai.sked_Resource_Name__c);
        }

        if (emailBody.containsIgnoreCase('[Status]')) {
            String status = skedAvai.sked__Is_Available__c ? 'Available' : 'Unavailable';
            emailBody = emailBody.replace('[Status]', status);
        }

        if (emailBody.containsIgnoreCase('[Start]')) {
            String startTime = skedAvai.sked__Start__c.format(skedConstants.MM_DD_YY_HH_MM_TIMEZONE, timezone);
            emailBody = emailBody.replace('[Start]', startTime);
        }

        if (emailBody.containsIgnoreCase('[End]')) {
            String endTime = skedAvai.sked__Finish__c.format(skedConstants.MM_DD_YY_HH_MM_TIMEZONE, timezone);
            emailBody = emailBody.replace('[End]', endTime);
        }
        System.debug('emailBody ' + emailBody);
        email.setToAddresses(new List<String>(emailAddresss));
        email.setSenderDisplayName(skedAvai.sked_Resource_Name__c);
        email.setSaveAsActivity(false);

        email.setSubject(skedAvai.sked_Resource_Name__c + ' Availability Notification');
        email.setHtmlBody(emailBody);

        return email;
    }
}
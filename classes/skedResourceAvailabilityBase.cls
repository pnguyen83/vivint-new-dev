global virtual class skedResourceAvailabilityBase {

    protected skedAvailatorParams params ;

    protected Set<Date> holidays ;

    public skedResourceAvailabilityBase(skedAvailatorParams params) {
        initialize(params);
    }

    public virtual Map<Id, resourceModel> initializeResourceList() {
        Map<Id, resourceModel> mapResource = loadWorkingTime();

        for (sked__Resource__c skedResource : [SELECT Id, Name, sked__User__c, sked__Primary_Region__c, sked__GeoLocation__c, sked__User__r.FullPhotoUrl,
                                                        sked__User__r.SmallPhotoUrl, sked__Primary_Region__r.Name, sked_Maximum_Travel_radius__c,
                                                        sked__Primary_Phone__c,
                                               (SELECT Id, sked__Whitelisted__c, sked__Blacklisted__c
                                                FROM sked__Account_Resource_Scores__r
                                                WHERE sked__Account__c = :this.params.accountId),

                                               (SELECT Id, sked__Tag__c, sked__Tag__r.Name, sked__Expiry_Date__c
                                                FROM sked__ResourceTags__r
                                                WHERE (sked__Expiry_Date__c = NULL OR sked__Expiry_Date__c >= :this.params.startTime)),

                                               (SELECT Id, sked__Job__c, sked__Job__r.sked__Start__c,
                                                sked__Job__r.sked__Finish__c, sked__Job__r.sked__GeoLocation__c,
                                                sked__Job__r.sked__Type__c, sked__Job__r.sked__Timezone__c, sked__Status__c
                                                FROM sked__Job_Allocations__r
                                                WHERE sked__Status__c != :skedConstants.JOB_ALLOCATION_STATUS_DELETED
                                                AND sked__Job__r.sked__Job_Status__c != :skedConstants.JOB_STATUS_CANCELLED
                                                AND DAY_ONLY(sked__Job__r.sked__Start__c) IN :this.params.inputDates
                                                AND sked__Job__c NOT IN :this.params.excludedJobIds
                                                ORDER BY sked__Job__r.sked__Start__c ASC),

                                               (SELECT Id, sked__Start__c, sked__Finish__c, sked__Is_Available__c
                                                FROM sked__Availabilities1__r
                                                WHERE sked__Start__c < :this.params.endTime AND sked__Finish__c > :this.params.startTime
                                                AND sked__Status__c = :skedConstants.AVAILABILITY_STATUS_APPROVED
                                                ORDER BY sked__Start__c ASC),

                                               (SELECT Id, sked__Start__c, sked__End__c, sked__GeoLocation__c, sked__Timezone__c
                                                FROM sked__Activities__r
                                                WHERE DAY_ONLY(sked__Start__c) IN :this.params.inputDates
                                                ORDER BY sked__Start__c ASC)

                                               FROM sked__Resource__c
                                               WHERE Id IN :this.params.resourceIds]) {
            initializeResourceData(mapResource, skedResource);
        }

        initializeOtherResourceData(mapResource);

        return mapResource;
    }

    protected virtual void initializeResourceData(Map<Id, resourceModel> mapResource, sked__Resource__c skedResource) {
        resourceModel resource;
        if (mapResource.containsKey(skedResource.Id)) {
            resource = mapResource.get(skedResource.Id);
        }
        else {
            resource = new resourceModel();
            resource.id = skedResource.Id;
        }
        initializeResource(skedResource, resource);


        if (!mapResource.containsKey(skedResource.Id)) {
            for (Date jobDate : this.params.inputDates) {
                dateslotModel dateslot = new dateslotModel();
                dateslot.start = skedDateTimeUtils.getStartOfDate(jobDate, this.params.timezoneSidId);
                dateslot.finish = skedDateTimeUtils.addDays(dateslot.start, 1, this.params.timezoneSidId);
                dateslot.addEvent(dateslot.start, dateslot.finish, 'non-working', false);

                string key = dateslot.start.format(skedDateTimeUtils.DATE_ISO_FORMAT, this.params.timezoneSidId);
                resource.mapDateslot.put(key, dateslot);
            }
            mapResource.put(resource.id, resource);
        }
        loadResourceEvents(skedResource, resource);

        for (dateslotModel dateslot : resource.mapDateslot.values()) {
            resource.events.addAll(dateslot.events);
        }
    }

    protected virtual void initializeOtherResourceData(Map<Id, resourceModel> mapResource) {}
    /**********************************************************Private methods********************************************************************/
	protected void initialize(skedAvailatorParams inputParams) {
        Map<string, Set<Date>> mapHolidays = skedCommonService.getMapHolidays();
        this.holidays = new Set<Date>();
        Set<Date> globalHolidays = mapHolidays.get(skedConstants.HOLIDAY_GLOBAL);
        this.holidays.addAll(globalHolidays);
        if (mapHolidays.containsKey(inputParams.regionId)) {
            Set<Date> regionHolidays = mapHolidays.get(inputParams.regionId);
            this.holidays.addAll(regionHolidays);
        }

        if (inputParams.inputDates == null || inputParams.inputDates.isEmpty()) {
            if (inputParams.inputJobs != null && !inputParams.inputJobs.isEmpty()) {
                Set<Date> inputDates = new Set<Date>();
                Date startDate, endDate;
                for (skedModels.job job : inputParams.inputJobs) {
                    Date jobDate = skedDateTimeUtils.getDateFromIsoString(job.startDate);
                    inputDates.add(jobDate);
                    if (startDate == null || startDate > jobDate) {
                        startDate = jobDate;
                    }
                    if (endDate == null || endDate < jobDate) {
                        endDate = jobDate;
                    }
                }
                inputParams.startDate = startDate;
                inputParams.endDate = endDate;
                inputParams.inputDates = inputDates;
            }
        }

        this.params = inputParams;

        this.params.startTime = skedDateTimeUtils.getStartOfDate(this.params.startDate, this.params.timezoneSidId);
        this.params.endTime = skedDateTimeUtils.getStartOfDate(this.params.endDate, this.params.timezoneSidId);
        this.params.endTime = skedDateTimeUtils.addDays(this.params.endTime, 1, this.params.timezoneSidId);
    }

    protected Map<Id, resourceModel> loadWorkingTime() {
        Map<Id, resourceModel> mapResource = new Map<Id, resourceModel>();
        List<sked__Availability_Template_Resource__c> templateResourceList = [SELECT sked__Resource__c, sked__Availability_Template__c
                                                                              FROM sked__Availability_Template_Resource__c
                                                                              WHERE sked__Resource__c IN :this.params.resourceIds];
        Map<Id, Set<Id>> map_TemplateId_ResourceIds = new Map<Id, Set<Id>>();
        for (sked__Availability_Template_Resource__c atr : templateResourceList) {
            Set<Id> resourceIds;
            if (map_TemplateId_ResourceIds.containsKey(atr.sked__Availability_Template__c)) {
                resourceIds = map_TemplateId_ResourceIds.get(atr.sked__Availability_Template__c);
            }
            else {
                resourceIds = new Set<Id>();
                map_TemplateId_ResourceIds.put(atr.sked__Availability_Template__c, resourceIds);
            }
            resourceIds.add(atr.sked__Resource__c);
        }

        Map<Id, sked__Availability_Template__c> mapTemplate
            = new Map<Id, sked__Availability_Template__c>([SELECT Id,
                                                           (SELECT Id, sked__Finish_Time__c, sked__Is_Available__c, sked__Start_Time__c,
                                                                    sked__Weekday__c, sked_Number_of_Parallel_Bookings__c
                                                            FROM sked__Availability_Template_Entries__r)
                                                           FROM sked__Availability_Template__c
                                                           WHERE Id IN :map_TemplateId_ResourceIds.keySet()]);
        for (sked__Availability_Template__c skedTemplate : mapTemplate.values()) {
            Set<Id> resourceIds = map_TemplateId_ResourceIds.get(skedTemplate.Id);
            Map<string, sked__Availability_Template_Entry__c> mapEntry = new Map<string, sked__Availability_Template_Entry__c>();
            for (sked__Availability_Template_Entry__c entry : skedTemplate.sked__Availability_Template_Entries__r) {
                mapEntry.put(entry.sked__Weekday__c, entry);
            }
            resourceModel sourceResource;
            for (Id resourceId : resourceIds) {
                resourceModel resource = new resourceModel();
                resource.id = resourceId;

                if (sourceResource == null) {
                    for (Date jobDate : this.params.inputDates) {
                        dateslotModel dateslot = new dateslotModel();
                        dateslot.start = skedDateTimeUtils.getStartOfDate(jobDate, this.params.timezoneSidId);
                        dateslot.finish = skedDateTimeUtils.addDays(dateslot.start, 1, this.params.timezoneSidId);
                        dateslot.addEvent(dateslot.start, dateslot.finish, 'non-working', false);
                        string key = dateslot.start.format(skedDateTimeUtils.DATE_ISO_FORMAT, this.params.timezoneSidId);
                        resource.mapDateslot.put(key, dateslot);

                        if (this.holidays.contains(jobDate)) {
                            continue;
                        }

                        string weekday = dateslot.start.format('EEE', this.params.timezoneSidId).toUpperCase();

                        if (mapEntry.containsKey(weekday)) {
                            sked__Availability_Template_Entry__c entry = mapEntry.get(weekday);
                            integer startWorkingInMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(entry.sked__Start_Time__c);
                            integer endWorkingInMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(entry.sked__Finish_Time__c);
                            DateTime startWorkingTime = skedDateTimeUtils.addMinutes(dateslot.start, startWorkingInMinutes, this.params.timezoneSidId);
                            DateTime endWorkingTime = skedDateTimeUtils.addMinutes(dateslot.start, endWorkingInMinutes, this.params.timezoneSidId);
                            //dateslot.noOfParallelJob = entry.sked_Number_of_Parallel_Bookings__c != null ? entry.sked_Number_of_Parallel_Bookings__c : 2;
                            dateslot.addEvent(startWorkingTime, endWorkingTime, null, true);
                        }
                    }
                    sourceResource = resource;
                }
                else {
                    for (string key : sourceResource.mapDateslot.keySet()) {
                        dateslotModel dateslot = sourceResource.mapDateslot.get(key);
                        dateslotModel cloneItem = dateslot.deepClone();
                        resource.mapDateslot.put(key, cloneItem);
                    }
                }
                mapResource.put(resourceId, resource);
            }
        }
        return mapResource;
    }

    protected virtual void initializeResource(sked__Resource__c skedResource, resourceModel resource) {}

    protected virtual void loadResourceEvents(sked__Resource__c skedResource, resourceModel resource) {}

    /**********************************************************Nested classes********************************************************************/
    global virtual class resourceModel extends skedModels.resource implements Comparable {
        public transient Map<string, dateslotModel> mapDateslot;
        public transient List<skedModels.event> availability;
        public transient List<skedModels.event> events;
        public Decimal maximumTravelRadius;
        public transient boolean allowMoreJob;
        public String primaryPhone;
        public transient boolean ignoreTravelTime;
        public transient Integer noOfPendingJob;
        public transient Integer noOfReadyJob;

        public Integer noOfJobsInSlot;
        public Integer maxNoOfJob;

        public resourceModel() {
            this.allowMoreJob = false;
            this.ignoreTravelTime = false;
            this.mapDateslot = new Map<string, dateslotModel>();
            this.availability = new List<skedModels.event>();
            this.events = new List<skedModels.event>();
        }

        public resourceModel deepClone() {
            resourceModel res = new resourceModel();
            res.id = this.id;
            res.name = this.name;
            res.category = this.category;
            res.photoUrl = this.photoUrl;
            res.regionId = this.regionId;
            res.userId = this.userId;
            res.timezoneSidId = this.timezoneSidId;
            res.address = this.address;
            res.distanceToJob = this.distanceToJob;
            res.timeToJob = this.timeToJob;
            res.regionName = this.regionName;
            res.geoLocation = this.geoLocation;
            if (this.mapDateslot != null)   res.mapDateslot = new Map<string, dateslotModel>(this.mapDateslot);
            if (this.availability != null) res.availability = new List<skedModels.event>(this.availability);
            if (this.events != null)    res.events = new List<skedModels.event>(this.events);
            res.maximumTravelRadius = this.maximumTravelRadius;
            res.allowMoreJob = this.allowMoreJob;
            res.primaryPhone = this.primaryPhone;
            res.ignoreTravelTime = this.ignoreTravelTime;
            res.noOfJobsInSlot = 0;
            res.noOfPendingJob = this.noOfPendingJob;
            res.noOfReadyJob = this.noOfReadyJob;

            return res;
        }

        public virtual Integer compareTo(Object compareTo) {
            resourceModel compareToRecord = (resourceModel)compareTo;
            Integer returnValue = 0;

            if (distanceToJob > compareToRecord.distanceToJob) {
                returnValue = 1;
            }
            else if (distanceToJob < compareToRecord.distanceToJob) {
                returnValue = -1;
            }
            return returnValue;
        }
    }

    global class dateslotModel {
        public DateTime start ;
        public DateTime finish ;
        public List<skedModels.event> events ;
//        public decimal noOfParallelJob ;
        public skedModels.event firstJobOfDay;

        public dateslotModel() {
            this.events = new List<skedModels.event>();
        }

        public dateslotModel deepClone() {
            dateslotModel result = new dateslotModel();
            result.start = this.start;
            result.finish = this.finish;
//            result.noOfParallelJob = this.noOfParallelJob;
            for (skedModels.event event : this.events) {
                skedModels.event cloneItem = event.clone();
                result.events.add(cloneItem);
            }
            return result;
        }

        public skedModels.event addEvent(DateTime startTime, DateTime endTime, string objectType, boolean isAvailable) {
            return addEvent(startTime, endTime, objectType, isAvailable, null);
        }

        public skedModels.event addEvent(DateTime startTime, DateTime endTime, string objectType, boolean isAvailable, string relatedId) {
            if (isAvailable == true) {
                addAvailableBlock(startTime, endTime);
            }
            else {
                skedModels.event newEvent = new skedModels.event();
                newEvent.start = startTime;
                newEvent.finish = endTime;
                newEvent.objectType = objectType;
                newEvent.isAvailable = false;
                this.events.add(newEvent);
                return newEvent;
            }
            return null;
        }

        private void addAvailableBlock(DateTime startTime, DateTime endTime) {
            List<skedModels.event> newEvents = new List<skedModels.event>();
            Set<DateTime> removedEvents = new Set<DateTime>();

            for (skedModels.event eventItem : this.events) {
                eventItem.isAvailable = true;
                if (eventItem.start < startTime && startTime < eventItem.finish) {
                    if (endTime < eventItem.finish) {
                        skedModels.event newEvent = new skedModels.event();
                        newEvent.start = endTime;
                        newEvent.finish = eventItem.finish;
                        newEvent.objectType = eventItem.objectType;
                        newEvent.isAvailable = true;
                        newEvents.add(newEvent);
                    }
                    eventItem.finish = startTime;
                }
                else if (startTime <= eventItem.start) {
                    if (endTime >= eventItem.finish) {
                        removedEvents.add(eventItem.start);
                    }
                    else if (eventItem.start < endTime && endTime < eventItem.finish) {
                        eventItem.start = endTime;
                    }
                }
            }

            for (integer i = this.events.size() - 1; i >= 0; i--) {
                skedModels.event eventItem = this.events.get(i);
                if (removedEvents.contains(eventItem.start)) {
                    this.events.remove(i);
                }
            }


            this.events.addAll(newEvents);
        }
    }

}
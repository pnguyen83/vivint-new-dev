public virtual class skedObjectSelector {

    public SObjectType objectType;
    private Integer limitCount;
    private Integer offsetCount;
    private list<String> filters;
    private String filterLogic;
    public List<String> sortFields;
    public Set<String> fields;

    private Map<Schema.ChildRelationship, skedObjectSelector> mapChildSelectors;
    private Set<String> parentFields;

    public skedObjectSelector(Schema.sObjectType objectType){
        //queryBuilder    = initBuilder(objectName);
        this.objectType     = objectType;
        if(fieldMap.containsKey(objectType)){
            fields = fieldMap.get(objectType);
        }else{
            fields = new Set<String>{'Id'};
        }
        mapChildSelectors   = new Map<Schema.ChildRelationship, skedObjectSelector>();
        filters        = new list<String>();
        sortFields              = new List<String>();
        parentFields    = new Set<String>();
    }

    public static skedObjectSelector newInstance(Schema.sObjectType objectType){
        return new skedObjectSelector(objectType);
    }

    public skedObjectSelector parentQuery(String fieldName){
        parentFields.add(fieldName.toLowerCase());
        return this;
    }

    public skedObjectSelector subQuery(String relationshipName){
        Schema.ChildRelationship cr = getChildRelationship(relationshipName);
        if(cr == null) return null;
        if(mapChildSelectors.containsKey( cr )){
            return mapChildSelectors.get( cr );
        }
        skedObjectSelector childSelector = new skedObjectSelector( cr.getChildSObject() );
        mapChildSelectors.put( cr, childSelector );
        return childSelector;
    }

    public skedObjectSelector subQueries(List<String> relationshipNames){
        for(String relationshipName : relationshipNames){
            subQuery(relationshipName);
        }
        return this;
    }

    public skedObjectSelector filter(String whereClause){
        filters.add( '(' + whereClause + ')');
        return this;
    }

    public skedObjectSelector filters(List<String> whereClauses){
        for(String whereClause : whereClauses){
            filter(whereClause);
        }
        return this;
    }

    public skedObjectSelector sort(String sortField){
        sortFields.add(sortField);
        return this;
    }

    public skedObjectSelector filterLogic(String filterLogic){
        this.filterLogic = filterLogic;
        return this;
    }

    public String getQuery(){
        String query = toQueryBuilder().getQuery();
        System.debug(query);
        return query;
    }

    public List<sObject> toSObjects(){
        return Database.query( toQueryBuilder().getQuery() );
    }

    public skedObjectQueryBuilder toQueryBuilder(){
        //Main query
        skedObjectQueryBuilder mainQuery = toQueryBuilder(this, this.objectType.getDescribe().getName());

        //Sub query
        for(Schema.ChildRelationship cr : mapChildSelectors.keySet()){
            skedObjectSelector childSelector = mapChildSelectors.get( cr );
            skedObjectQueryBuilder childQuery = toQueryBuilder(childSelector, cr.getRelationshipName());
            mainQuery.addChildQuery( cr.getRelationshipName(), childQuery );
        }

        return mainQuery;
    }

    private skedObjectQueryBuilder toQueryBuilder(skedObjectSelector selector, String apiName){
        //Change fields to lower case to avoid querying duplicate fields
        String fieldNames = String.join(new List<String>(selector.fields),',');
        selector.fields.clear();
        for(String fieldName : fieldNames.split(',')){
            selector.fields.add( fieldName.toLowerCase() );
        }
        //add parent object fields
        Map <String, Schema.SObjectField> fieldMap = selector.objectType.getDescribe().fields.getMap();

        for(String fieldName : selector.parentFields){
            if(!fieldMap.containsKey( fieldName.toLowerCase() )) continue;

            Schema.DescribeFieldResult fieldDescribe = fieldMap.get( fieldName.toLowerCase() ).getDescribe();
            List<Schema.SObjectType> objTypes = fieldDescribe.getReferenceTo();
            if(objTypes == null || objTypes.isEmpty()) continue;

            selector.fields.add( fieldDescribe.getName().toLowerCase() );

            String relationshipName = fieldDescribe.getRelationshipName().toLowerCase();
            skedObjectSelector parentSelector = new skedObjectSelector( objTypes.get(0) );
            for(String f : parentSelector.fields){
                selector.fields.add( relationshipName + '.' + f.toLowerCase() );
            }
        }
        skedObjectQueryBuilder queryBuilder = new skedObjectQueryBuilder(apiName);
        queryBuilder.addFields(selector.fields);
        //Set where clause
        if(!selector.filters.isEmpty()){
            if(selector.filters.size()==1){
                queryBuilder.whereClause = ' WHERE ' + selector.filters.get(0);
            }else{
                if(String.isBlank( selector.filterLogic )){
                    queryBuilder.whereClause = ' WHERE ' + String.join( selector.filters, ' AND ' );
                }else{
                    queryBuilder.whereClause = ' WHERE ' + String.format( selector.filterLogic, selector.filters );
                }
            }
        }
        //Sorting
        if(!selector.sortFields.isEmpty()){
            queryBuilder.extraClause = 'ORDER BY ' + String.join(selector.sortFields, ',');
        }
        return queryBuilder;
    }

    public static String test(){
        skedObjectSelector selector = new skedObjectSelector(sked__Job__c.sObjectType);
        selector.parentQuery('sked__Contact__c');
         return selector.getQuery();
    }

    /**
     * Get the ChildRelationship from the Table for the relationship name passed in.
    **/
    private Schema.ChildRelationship getChildRelationship(String relationshipName){
        for (Schema.ChildRelationship childRow : objectType.getDescribe().getChildRelationships()){
            if (childRow.getRelationshipName() == relationshipName){
                return childRow;
            }
        }
        return null;
    }

    public Map<Schema.sObjectType,Set<String>> fieldMap = new Map<Schema.sObjectType, Set<String>>{
        Account.sObjectType => new Set<String>{
            'Id', 'Name', 'BillingCity', 'BillingState', 'ShippingStreet', 'ShippingCity', 'ShippingState','ShippingPostalCode','ShippingCountry'
        },
        Contact.sObjectType => new Set<String>{
            'Id', 'Name', 'Email', 'Title', 'Birthdate', 'MobilePhone', 'PhotoUrl','Account.Id','Account.Name'
        },
        sked__Job__c.sObjectType => new Set<String>{
            'Id', 'Name', 'sked__Type__c', 'sked__Address__c', 'sked__Start__c', 'sked__Job_Allocation_Count__c',
            'sked__Account__c', 'sked__Finish__c', 'sked__Job_Status__c', 'sked__Timezone__c', 'sked__Location__c', 'sked__Contact__c',
            'sked__Duration__c', 'sked__Notes_Comments__c', 'sked__Region__c', 'sked__Region__r.Id', 'sked__Region__r.Name',
            'sked__Region__r.sked__Timezone__c', 'sked__GeoLocation__latitude__s', 'sked__GeoLocation__longitude__s',
            'sked__Description__c', 'sked__Abort_Reason__c', 'sked_accepts_Vivint_Policy__c', 'ssked_Prospect_Additional_information__c',
            'sked_Prospect_has_speed_internet__c', 'sked_Prospect_owns_Home_or_Business__c', 'sked_Prospect_interested_in_Smart_Home__c',
            'sked_Prospect_Rep_ID__c', 'sked_Vivint_Account_Number__c', 'sked_Area_Interested_In_Smart_Home__c',
            'sked_Prospect_First_Name__c', 'sked_Prospect_Last_Name__c', 'sked_Customer_Email__c', 'sked_Customer_Phone__c',
            'sked_Setter_Name__c', 'sked_Setter_Location__r.Name', 'sked_Setter_Location__c', 'sked_Will_a_decision_maker_be_present__c',
            'sked_Do_you_have_security_system__c', 'sked_How_long_have_you_had_your_system__c', 'sked_Is_your_system_being_monitored__c',
            'sked_Prospect_Address__c', 'sked__Contact__r.firstname', 'sked__Contact__r.lastname', 'sked_Contact_Attempted_Time__c',
            'sked_Job_End_Minutes__c', 'sked_Job_Start_Minutes__c', 'sked_Demo_Result__c', 'Job_Created_Desc__c', 'sked_Slot__c'
        },
        sked__Job_Allocation__c.sObjectType => new Set<String>{
            'Id', 'Name', 'sked__Resource__c', 'sked__Resource__r.Id', 'sked__Resource__r.Name', 'sked__Status__c',
            'sked__Resource__r.sked__Primary_Region__c', 'sked__Resource__r.sked__Primary_Region__r.sked__Timezone__c',
            'sked__Resource__r.sked__Category__c', 'sked__Resource__r.sked__GeoLocation__c',
            'sked__Resource__r.sked__User__c', 'sked__Resource__r.sked__User__r.SmallPhotoUrl',
            'sked__Job__c','sked__Job__r.Name', 'sked__Job__r.sked__Duration__c', 'sked__Job__r.sked__Start__c',
            'sked__Job__r.sked__Finish__c', 'sked__Job__r.sked__Contact__c', 'sked__Job__r.sked__Address__c',
            'sked__Job__r.sked_Customer_Phone__c', 'sked__Job__r.sked_Contact_Attempted_Time__c',
            'sked__Job__r.sked_Area_Interested_In_Smart_Home__c', 'sked__Job__r.sked_Setter_Location__c',
            'sked__Job__r.sked_Setter_Location__r.Name', 'sked__Job__r.sked_Customer_Email__c',
            'sked__Job__r.sked__Start__c', 'sked__Job__r.sked__Finish__c', 'sked__Job__r.sked__Job_Status__c',
            'sked__Job__r.sked_Job_Start_Minutes__c', 'sked__Job__r.sked__GeoLocation__latitude__s',
            'sked__Job__r.sked__GeoLocation__longitude__s', 'sked__Job__r.sked__Contact__r.FirstName',
            'sked__Job__r.sked__Contact__r.LastName', 'sked__Job__r.sked__Timezone__c',
            'sked__Job__r.sked_Job_End_Minutes__c', 'sked__Job__r.sked__Account__r.name', 'sked__Job__r.sked_accepts_Vivint_Policy__c',
            'sked__Job__r.ssked_Prospect_Additional_information__c', 'sked__Job__r.sked_Prospect_has_speed_internet__c',
            'sked__Job__r.sked_Prospect_owns_Home_or_Business__c', 'sked__Job__r.sked_Prospect_Rep_ID__c',
            'sked__Job__r.sked_Vivint_Account_Number__c', 'sked__Job__r.sked_Prospect_interested_in_Smart_Home__c',
            'sked__Job__r.sked_Prospect_First_Name__c', 'sked__Job__r.sked_Prospect_Last_Name__c', 'sked__Job__r.sked_Setter_Name__c',
            'sked__Job__r.sked_Will_a_decision_maker_be_present__c',
            'sked__Job__r.sked_Do_you_have_security_system__c', 'sked__Job__r.sked_How_long_have_you_had_your_system__c',
            'sked__Job__r.sked_Is_your_system_being_monitored__c', 'sked__Job__r.sked_Prospect_Address__c',
            'sked__Job__r.sked__type__c', 'sked__Job__r.sked_Demo_Result__c',
            'sked__Job__r.sked__Abort_Reason__c', 'sked__Job__r.Job_Created_Desc__c', 'sked__Job__r.sked_Slot__c',
            'sked__Job__r.sked__Region__c'
        },
        sked__Resource__c.sObjectType => new Set<String>{
            'Id', 'Name', 'sked__Primary_Region__c', 'sked__Primary_Region__r.Name', 'sked__Category__c',
            'sked__User__c', 'sked__User__r.SmallPhotoUrl', 'sked__Primary_Region__r.sked__Timezone__c',
            'sked__Is_Active__c'
        },
        sked__Availability__c.sObjectType => new Set<String>{
            'Id', 'Name', 'sked__Is_Available__c', 'sked__Start__c', 'sked__Finish__c', 'sked__Type__c', 'sked__Status__c'
        },
        sked__Slot__c.sObjectType => new Set<String>{
            'Id', 'Name', 'sked_Active_End_Date__c', 'sked_Active_Start_Date__c', 'sked_Bookable_days_in_advance__c', 'sked_Booking_Type__c',
            'sked_Day__c', 'sked_End_Time__c', 'sked_Market__c', 'sked_Number_of_Parallel_Bookings_Pending__c', 'sked_Number_of_Parallel_bookings_Ready__c',
            'sked_Start_Time__c', 'sked_Market__r.Name', 'sked_Market__r.sked__Timezone__c'
        },
        sked__Region__c.sObjectType => new Set<String> {
            'Id', 'Name', 'sked__Timezone__c'
        },
        EmailTemplate.sObjectType => new Set<String> {
            'Id', 'Subject', 'HtmlValue', 'Body', 'DeveloperName'
        },
        sked_Queue_Management_Access__c.sObjectType => new Set<String> {
            'id', 'sked_Resource__c', 'sked_Resource__r.sked__Mobile_Phone__c', 'sked_Resource__r.sked__Email__c', 'sked_Primary__c', 'sked_Region__c'
        }
    };
}
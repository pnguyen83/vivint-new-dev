public class skedAreaManagementQueueHandler {
    public static skedRemoteResultModel getJobsInRegion(skedVivintModel.AreaManagementFilter filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();
        List<skedVivintModel.VivintJA> jas = new List<skedVivintModel.VivintJA>();

        try {
            filter.fromJobTab = true;
            if (String.isNotBlank(filter.searchText)) {
                jas = searchJobByJobName(filter);
            }
            else {
                jas = getJAsInRegion(filter);
            }

            result.data = jas;
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getConfigData() {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            Set<String> regionIds = skedCommonService.checkUserPermissionSetFromUserId();
            if (regionIds.isEmpty()) {
                result.success = false;
                result.errorMessage = skedConstants.CLOSER_ACCESS_MANAGER_PAGE;
                result.message = skedConstants.CLOSER_ACCESS_MANAGER_PAGE;
                return result;
            }

            result.data = new skedVivintModel.AreaManagementConfig();
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel cancelJob(skedVivintModel.VivintJob job) {
        skedRemoteResultModel result = new skedRemoteResultModel();
        SavePoint sp = Database.setSavePoint();

        try {
            cancelSelectedJob(job);
        }
        catch (Exception ex) {
            Database.rollback(sp);
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getResourcesToReassign(skedVivintModel.JobAvaiModel filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = checkResourcesToReassign(filter);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel confirmResourceAssignment(skedVivintModel.Assignment data) {
        skedRemoteResultModel result = new skedRemoteResultModel();
        SavePoint sp = Database.setSavePoint();

        try {
            result.data = confirmAssignment(data);
        }
        catch (Exception ex) {
            Database.rollback(sp);
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getAllResourcesInRegions(skedVivintModel.AreaManagementFilter filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = getAllResourceInRegion(filter);
        }
        catch(Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getResAndJobInRegions(skedVivintModel.AreaManagementFilter filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = getResourceAndJobInRegions(filter);

        }
        catch(Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getResourceForReschedule(skedVivintModel.AreaManagementFilter filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = getResourceDataForReschedule(filter);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel rescheduleJob (skedVivintModel.RescheduleJob data) {
        skedRemoteResultModel result = new skedRemoteResultModel();
        SavePoint sp = Database.setSavePoint();

        try {
            saveRescheduleJob(data);
        }
        catch (Exception ex) {
            Database.rollback(sp);
            result.getError(ex);
        }

        return result;
    }

    public static skedRemoteResultModel getUnAllocatedJobsForSchedule(skedVivintModel.AreaManagementFilter filter) {
        skedRemoteResultModel result = new skedRemoteResultModel();

        try {
            result.data = getUnallocatedJobInRegionForScheduleTab(filter);
        }
        catch (Exception ex) {
            result.getError(ex);
        }

        return result;
    }

    //===========================================Private Functions===================//
    private static List<skedVivintModel.VivintJA> getJAsInRegion (skedVivintModel.AreaManagementFilter filter) {
        List<skedVivintModel.VivintJA> vivintJAs = new List<skedVivintModel.VivintJA>();
        Map<id, skedVivintModel.VivintJA> map_jobid_jas = new Map<id, skedVivintModel.VivintJA>();
        String timezone = filter.timezone != null ? filter.timezone : skedCommonService.getTimezoneFromUserId();

        if (filter.isFromAppointmentQueue == null || filter.isFromAppointmentQueue == false) {
            checkAndGetRegionOfAreaManager(filter);
        }

        Set<String> jobStatus = filter.status != null ? new Set<String>(filter.status) : new Set<String>();
        Set<String> closerIds = filter.closers != null ? new Set<String>(filter.closers) : new Set<String>();
        if (filter.closers == null && filter.resourceList != null) {
            for (skedModels.resource res : filter.resourceList) {
                closerIds.add(res.id);
            }
        }

        skedModels.resource currentRes = skedCommonService.getResourceFromUserId(UserInfo.getUserId());
        if (filter.getMyJobs != null && filter.getMyJobs) {
            closerIds.add(currentRes.id);
        }
        else if (filter.getMyJobs != null && filter.getMyJobs){
            closerIds.remove(currentRes.id);
        }

        Date startDate;
        Date endDate;

        if (filter.dayList != null) {
            for (String day : filter.dayList) {
                Date selectedDate = Date.valueOf(day);
                if (startDate == null || startDate > selectedDate) {
                    startDate = selectedDate;
                    filter.startDate = day;
                }

                if (endDate == null || endDate < selectedDate) {
                    endDate = selectedDate;
                    filter.endDate = day;
                }
            }
        }

        if (filter.startDate != null && startDate == null) {
            startDate = Date.valueOf(filter.startDate);
        }

        if (filter.endDate != null && endDate == null) {
            endDate = Date.valueOf(filter.endDate);
        }


        DateTime startOfTime = skedDateTimeUtils.getStartOfDate(startDate, timezone);
        DateTime endOfTime = skedDateTimeUtils.getEndOfDate(endDate, timezone);
        filter.startOfTime = startOfTime;
        filter.endOfTime = endOfTime;
        String ja_declined_status = skedConstants.JOB_ALLOCATION_STATUS_DECLINED;

        List<String> regionIds = new List<String>(filter.regions);

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job__c.sObjectType).filter('sked__Region__c IN :regionIds');
        selector.filter('sked__Start__c <= :endOfTime');
        selector.filter('sked__Finish__c >=: startOfTime');
        selector.subQuery('sked__Job_Allocations__r').filter('sked__Status__c != :ja_declined_status');

        List<sked__job__c> skedJobs = Database.query(selector.getQuery());

        for (sked__Job__c skedJob : skedJobs) {
            boolean isJobValid = true;

            if (isJobValid && !jobStatus.isEmpty()) {
                if (!jobStatus.contains(skedJob.sked__Job_Status__c)) {
                    isJobValid = false;
                }
            }
            if (isJobValid) {
                if (skedJob.sked__Job_Allocations__r != null && !skedJob.sked__Job_Allocations__r.isEmpty()) {
                    for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
                        boolean isValid = true;
                        if (isValid && !closerIds.isEmpty()) {
                            if (!closerIds.contains(skedJA.sked__Resource__c)) {
                                isValid = false;
                            }
                        }

                        if (isValid) {
                            String jobTimezone = skedJob.sked__Timezone__c;
                            skedVivintModel.VivintJA jaModel = generateVivintJaFromJa(skedJA, jobTimezone);
                            jaModel.jaCount = skedJob.sked__Job_Allocation_Count__c;
                            jaModel = skedCommonService.updateContactAttemptedJob(jaModel, jobTimezone, skedJob.sked_Contact_Attempted_Time__c);

                            if (!map_jobid_jas.containsKey(skedJA.sked__Job__c)) {
                                map_jobid_jas.put(skedJA.sked__Job__c, jaModel);
                            }
                            else {
                                skedVivintModel.VivintJA ja = map_jobid_jas.get(skedJA.sked__Job__c);
                                if (ja.status != skedConstants.JOB_ALLOCATION_STATUS_DELETED &&
                                            ja.status != skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH) {
                                    continue;
                                }
                                map_jobid_jas.put(skedJA.sked__Job__c, jaModel);
                            }
                        }
                    }
                }
                else {
                    skedVivintModel.VivintJA ja = new skedVivintModel.VivintJA(skedJob, null);
                    ja.jaCount = skedJob.sked__Job_Allocation_Count__c;
                    ja = skedCommonService.updateContactAttemptedJob(ja, skedJob.sked__Timezone__c, skedJob.sked_Contact_Attempted_Time__c);
                    map_jobid_jas.put(skedJob.id, ja);
                }
            }

        }

        if (!map_jobid_jas.isEmpty()) {
            for (skedVivintModel.VivintJA ja : map_jobid_jas.values()) {
                if (filter.isFromAppointmentQueue != null && filter.isFromAppointmentQueue) {
                    if (ja.status == skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH) {
                        vivintJAs.add(ja);
                    }
                }
                else {
                    vivintJAs.add(ja);
                }
            }

        }

        return vivintJAs;
    }

    private static List<skedVivintModel.VivintJA> searchJobByJobName(skedVivintModel.AreaManagementFilter filter) {
        List<skedVivintModel.VivintJA> vivintJAs = new List<skedVivintModel.VivintJA>();
        String myResourceId = skedCommonService.getResourceFromUserId(UserInfo.getUserId()).id;
        Set<String> selectedResIds = new Set<String>(filter.closers);

        if (filter.getMyJobs) {
            selectedResIds.add(myResourceId);
        }
        else {
            selectedResIds.remove(myResourceId);
        }

        Integer noOfJob = skedSetting.instance.Admin.noOfJobToDisplay;
        String jobName = '%' + filter.searchText.replace(' ','%') + '%';
        filter.timezone = skedCommonService.getTimezoneFromUserId();
        Set<String> myRegionIds = skedCommonService.getRegionIdFromUserId(filter);

        String ja_declined_status = skedConstants.JOB_ALLOCATION_STATUS_DECLINED;

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
        selector.filter('Name LIKE :jobName OR sked__Contact__r.Name LIKE :jobName');
        selector.filter('sked__Region__c IN: myRegionIds');
        selector.subQuery('sked__Job_Allocations__r').filter('sked__Status__c != :ja_declined_status');
        List<sked__Job__c> skedJobs = Database.query(selector.getQuery());

        for (sked__Job__c skedJob : skedJobs) {
            skedVivintModel.VivintJA ja = filterJaOfJob(skedJob);
            ja = skedCommonService.updateContactAttemptedJob(ja, filter.timezone, skedJob.sked_Contact_Attempted_Time__c);
            vivintJAs.add(ja);
        }

        System.debug('vivintJAs ' + JSON.serialize(vivintJAs));
        return vivintJAs;
    }

    private static void checkAndGetRegionOfAreaManager(skedVivintModel.AreaManagementFilter filter) {
        Set<String> setManagerRegionIds = skedCommonService.getRegionIdFromUserId(filter);

        if (!setManagerRegionIds.isEmpty()) {
            filter.regions = new List<String>(setManagerRegionIds);
        }
        else {
            filter.regions = new List<String>();
        }
    }

    private static boolean checkSearchStringWithSearchTarget(String value, String target) {
        boolean result = false;
        if (target.containsIgnoreCase(value)){
            result = true;
        }
        else {
            result = false;
        }

        return result;
    }

    private static void  cancelSelectedJob(skedVivintModel.VivintJob job) {
        List<sObject> objects = new List<sObject>();
        String jobId = job.id;
        String ja_complete_status = skedConstants.JOB_ALLOCATION_STATUS_COMPLETE;
        String ja_delete_status = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
        String ja_declined_status = skedConstants.JOB_ALLOCATION_STATUS_DECLINED;
        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
        selector.filter('Id =:jobId');
        selector.subQuery('sked__Job_Allocations__r').filter('sked__Status__c != :ja_complete_status')
                                                        .filter('sked__Status__c != :ja_delete_status')
                                                        .filter('sked__Status__c != :ja_declined_status');

        List<sked__Job__c> skedJobs = Database.query(selector.getQuery());

        if (skedJobs == null || skedJobs.isEmpty()) {
            throw new skedException(skedConstants.INVALID_JOB_ID);
        }

        for (sked__Job__c skedJob : skedJobs) {
            skedJob.sked__Job_Status__c = skedConstants.JOB_STATUS_CANCELLED;
            skedJob.sked__Abort_Reason__c = job.reason;
            objects.add(skedJob);

            if (skedJob.sked__Job_Allocations__r != null) {
                for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
                    skedJA.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
                    objects.add(skedJA);
                }
            }
        }

        if (!objects.isEmpty()) {
            objects.sort();
            update objects;
        }
    }

    private static skedVivintModel.AssignResourceData checkResourcesToReassign(skedVivintModel.JobAvaiModel filter) {
        Map<Id, skedResourceAvailabilityBase.resourceModel> mapResources = skedCommonService.getAvaiResourcesOfJob(filter);
        skedVivintModel.AssignResourceData data = new skedVivintModel.AssignResourceData();
        Set<Id> setSelectedResIDs = new Set<Id>();

        if (filter.job.sked__Job_Allocations__r != null) {
            for (sked__Job_Allocation__c skedJA : filter.job.sked__Job_Allocations__r) {
                setSelectedResIDs.add(skedJA.sked__Resource__c);
            }
        }

        Location jobLocation = Location.newInstance(filter.job.sked__GeoLocation__latitude__s, filter.job.sked__GeoLocation__longitude__s);
        Integer velocity = skedSetting.instance.Admin.velocity;

        for (skedResourceAvailabilityBase.resourceModel res : mapResources.values()) {
            System.debug('0 check res ' + res.name);
            skedResourceAvailabilityBase.dateslotModel daySlotModel =  res.mapDateslot.get(filter.startDate);
            if (!setSelectedResIDs.isEmpty() && setSelectedResIDs.contains(res.id)) {
                data.selectedResources.add(res);
                continue;
            }

            boolean isAvai = skedCommonService.checkResourceWithTimePeriod(res, daySlotModel,
                                                        true, filter.job.sked__Start__c, filter.job.sked__Finish__c, null);
            System.debug('1 check res ' + res.name + ' isAvai ' + isAvai);
            if (isAvai) {
                isAvai = skedSetterHandler.checkResourceMaxTravelRadius(res, jobLocation);
                System.debug('2 check res ' + res.name + ' isAvai ' + isAvai);

                if (isAvai) {
                    isAvai = skedSetterHandler.checkTravelTimeOfResourceStraightLine(jobLocation, filter.job.sked__Start__c, filter.job.sked__Finish__c, daySlotModel.events,
                                                            res, velocity);
                    System.debug('3 check res ' + res.name + ' isAvai ' + isAvai);
                }
            }

            if (isAvai) {
                data.avaiResources.add(res);
            }
            else {
                data.unAvaiResources.add(res);
            }
        }

        return data;

    }

    private static List<sked__Job_Allocation__c> confirmAssignment(skedVivintModel.Assignment data) {
        List<sked__Job_Allocation__c> newSkedJas = new List<sked__Job_Allocation__c>();
        List<sked__Job_Allocation__c> oldSkedJas = new List<sked__Job_Allocation__c>();
        Map<String, sked__Job_Allocation__c> map_resId_ja = new Map<String, sked__Job_Allocation__c>();

        String jobId = data.jobId;
        String ja_delete_status = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
        String ja_declined_status = skedConstants.JOB_ALLOCATION_STATUS_DECLINED;
        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job_Allocation__c.sObjectType);
        selector.filter('sked__Status__c != :ja_declined_status');
        selector.filter('sked__Status__c != :ja_delete_status');
        selector.filter('sked__Job__c = :jobId');
        List<sked__Job_Allocation__c> skedJAs = Database.query(selector.getQuery());

        for (sked__Job_Allocation__c skedJA : skedJAs) {
            map_resId_ja.put(skedJA.sked__Resource__c, skedJA);
        }

        for (String resID : data.resourceIDs) {
            if (map_resId_ja.containsKey(resID)) {
                map_resId_ja.remove(resId);
                continue;
            }
            else {
                sked__Job_Allocation__c newJA = new sked__Job_Allocation__c(
                    sked__Job__c = data.jobId,
                    sked__Resource__c = resID,
                    sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
                );
                newSkedJas.add(newJA);
            }
        }

        if (!map_resId_ja.isEmpty()) {
            for (sked__Job_Allocation__c skedJA : map_resId_ja.values()) {
                skedJA.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
                oldSkedJas.add(skedJA);
            }
        }

        if (!newSkedJas.isEmpty()) {
            insert newSkedJas;
        }

        if (!oldSkedJas.isEmpty()) {
            update oldSkedJas;
        }

        return newSkedJas;
    }

    private static List<skedModels.resource> getAllResourceInRegion(skedVivintModel.AreaManagementFilter filter) {
        List<skedModels.resource> resources = new List<skedModels.resource>();
        checkAndGetRegionOfAreaManager(filter);
        Map<String, List<skedModels.resource>> map_regionId_resources = skedCommonService.getResourcesInRegions(filter.regions);

        for (List<skedModels.resource> regRes : map_regionId_resources.values()) {
            resources.addAll(regRes);
        }

        return resources;
    }

    private static List<skedVivintModel.AreaManagerRescheduleData> getResourceAndJobInRegions(skedVivintModel.AreaManagementFilter filter) {
        if (filter.resourceList == null || filter.resourceList.isEmpty()) {
            throw new skedException(skedConstants.MISSING_RESOURCE_LIST);
        }

        if (filter.dayList == null || filter.dayList.isEmpty()) {
            throw new skedException(skedConstants.MISSING_DAY_LIST);
        }

        List<skedVivintModel.AreaManagerRescheduleData> result = new List<skedVivintModel.AreaManagerRescheduleData>();
        Map<id, skedModels.resource> map_id_resource = new Map<id, skedModels.resource>();

        List<skedModels.resource> resources = new List<skedModels.resource>(filter.resourceList);
        Map<id, List<skedVivintModel.VivintJA>> map_id_ja = new Map<id, List<skedVivintModel.VivintJA>>();

        String timezone = skedCommonService.getTimezoneFromUserId();

        checkAndGetRegionOfAreaManager(filter);

        for (skedModels.resource res : resources) {
            map_id_resource.put(res.id, res);
        }

        List<skedVivintModel.VivintJA> jobAllocations = getJAsInRegion(filter);

        for (skedVivintModel.VivintJA ja : jobAllocations) {
            System.debug('1 job ' + ja.jobName);
            if (ja.res != null) {
                System.debug('2 job ' + ja.jobName);
                List<skedVivintModel.VivintJA> lstJAs = map_id_ja.get(ja.res.id);
                lstJAs = lstJAs == null ? new List<skedVivintModel.VivintJA>() : lstJAs;

                if (ja.jobStatus != skedConstants.JOB_STATUS_CANCELLED && ja.status == skedConstants.JOB_ALLOCATION_STATUS_DELETED) {
                   System.debug('3 job ' + ja.jobName);
                    continue;
                }
                System.debug('4 job ' + ja.jobName);

                lstJAs.add(ja);
                map_id_ja.put(ja.res.id, lstJAs);
            }
        }

        List<skedVivintModel.TimeSlot> basedLstTimeSlots = skedCommonService.getListTimeSlotInListOfDates(filter.dayList, filter.regions);

        for (skedModels.resource res : resources) {
            skedVivintModel.AreaManagerRescheduleData data = new skedVivintModel.AreaManagerRescheduleData(res);
            List<skedVivintModel.VivintJA> lstJAs = map_id_ja.get(res.Id);
            lstJAs = lstJAs == null ? new List<skedVivintModel.VivintJA>() : lstJAs;

            for (String day : filter.dayList) {
                Date selectedDate = Date.valueOf(day);
                DateTime startOfSelectedDay = skedDateTimeUtils.getStartOfDate(selectedDate, timezone);
                String weekDay = startOfSelectedDay.format(skedConstants.WEEKDAY, timezone);
                List<skedVivintModel.TimeSlot> lstTimeSlots = new List<skedVivintModel.TimeSlot>(basedLstTimeSlots);

                lstTimeSlots.sort();

                for (skedVivintModel.TimeSlot baseSlot : lstTimeSlots) {
                    if (baseSlot.weekDay != null && weekDay.equalsIgnoreCase(baseSlot.weekDay) &&
                            baseSlot.bookingType.equalsIgnoreCase(skedConstants.SLOT_RESOURCE_TYPE)) {
                        if ((baseSlot.slotStartDate == null || baseSlot.slotStartDate <= selectedDate) &&
                            (baseSlot.slotEndDate == null || baseSlot.slotEndDate >= selectedDate)) {
                            skedVivintModel.TimeSlot slot = baseSlot.deepClone();
                            DateTime startTime = skedDateTimeUtils.addMinutes(startOfSelectedDay, slot.startMinutes, timezone);
                            DateTime endTime = skedDateTimeUtils.addMinutes(startOfSelectedDay, slot.endMinutes, timezone);
                            slot.dateString = day;

                            for (skedVivintModel.VivintJA ja : lstJAs) {
                                if (ja.res.id == res.id) {
                                    if (startTime <= ja.jobFinish && endTime >= ja.jobStart){
                                        if ((slot.startMinutes == ja.jobStartMinutes && slot.endMinutes == ja.jobEndMinutes) ||
                                            ja.timeslotId == slot.id) {
                                            slot.jobAllocations.add(ja);
                                        }
                                    }
                                }
                            }

                            data.timeSlots.add(slot);
                        }
                    }
                }
            }

            result.add(data);
        }

        return result;
    }

    private static List<skedVivintModel.AreaManagerRescheduleData> getResourceDataForReschedule(skedVivintModel.AreaManagementFilter filter) {
        List<skedVivintModel.AreaManagerRescheduleData> result = new List<skedVivintModel.AreaManagerRescheduleData>();
        checkAndGetRegionOfAreaManager(filter);
        String timezone = filter.selectedJob.timezone != null ? filter.selectedJob.timezone : skedCommonService.getTimezoneFromUserId();
        String dateString = filter.dayList.get(0);//only one day is sent back from UI
        Date selectedDate = Date.valueOf(dateString);
        DateTime startOfGrid = skedDateTimeUtils.getStartOfDate(selectedDate, timezone);
        String selectedWeekDay = startOfGrid.format(skedConstants.WEEKDAY, timezone).tolowerCase();
        skedVivintModel.JobAvaiModel jobData = new skedVivintModel.JobAvaiModel();
        jobData.regions = new List<String>{filter.selectedJob.regionId};
        jobData.jobId = filter.selectedJob.jobId;
        jobData.selectedDate = dateString;

        Map<Id, skedResourceAvailabilityBase.resourceModel> mapResources = skedCommonService.getAvaiResourcesOfJob(jobData);

        List<skedVivintModel.TimeSlot> lstTimeSlots = skedCommonService.getListTimeSlotInDefinedTimeRange(selectedDate, selectedDate, jobData.regions);

        //set jobLocation for timeslot
        for (skedVivintModel.TimeSlot slot : lstTimeSlots) {
            slot.jobLocation = Location.newInstance(filter.selectedJob.geometry.lat, filter.selectedJob.geometry.lng);
            slot.jobId = jobData.jobId;
        }

        Integer velocity = skedSetting.instance.Admin.velocity;
        boolean ignoreTravelTimeFirstJob = skedSetting.instance.Admin.ignoreTravelTimeFirstJob;

        lstTimeSlots.sort();

        for (skedResourceAvailabilityBase.resourceModel resource : mapResources.values()) {
            System.debug('verify res ' + resource.name + ' id ' + resource.id);
            skedVivintModel.AreaManagerRescheduleData data = new skedVivintModel.AreaManagerRescheduleData();
            data.resource = resource.deepClone();
            data.timeSlots = new List<skedVivintModel.TimeSlot>();
            Map<Id, skedResourceAvailabilityBase.resourceModel> mapResource = new Map<Id, skedResourceAvailabilityBase.resourceModel>{resource.id => resource};

            skedVivintModel.TimeSlotCheckingModel checkingData = new skedVivintModel.TimeSlotCheckingModel(lstTimeSlots,
                                                                                new Set<String>(), timezone, startOfGrid, mapResource,
                                                                                dateString, null, velocity, ignoreTravelTimeFirstJob);

            for (skedVivintModel.TimeSlot defaultSlot : lstTimeSlots) {
                if (selectedWeekDay.equalsIgnoreCase(defaultSlot.weekDay) &&
                        defaultSlot.bookingType != skedConstants.SLOT_UNALLOCATED_TYPE &&
                        defaultSlot.market.id == filter.selectedJob.regionId) {
                    checkingData.checkingSlot = defaultSlot.deepClone();
                    skedVivintModel.TimeSlot slot = skedSetterHandler.checkAvaiResOfTimeSlot(checkingData);
                    Map<id, skedResourceAvailabilityBase.resourceModel> map_id_res = new Map<id, skedResourceAvailabilityBase.resourceModel>();
                    for (skedResourceAvailabilityBase.resourceModel res : slot.lstRes) {
                        System.debug('2 verify res ' + res.name + ' id ' + res.id);
                        map_id_res.put(res.id, res);
                    }

                    if (map_id_res.containsKey(resource.id)) {
                        slot.isAvai = true;
                    }
                    else {
                        slot.isAvai = false;
                    }

                    data.timeSlots.add(slot);
                }
            }

            result.add(data);
        }


        return result;
    }

    private static void saveRescheduleJob(skedVivintModel.RescheduleJob data) {
        String ja_declined_status = skedConstants.JOB_ALLOCATION_STATUS_DECLINED;
        String ja_delete_status = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
        String ja_complete_status = skedConstants.JOB_ALLOCATION_STATUS_COMPLETE;
        String jobId = data.jobId;

        skedObjectSelector selector = skedObjectSelector.newInstance(sked__Job__c.sObjectType);
        selector.filter('Id = :jobId');
        selector.subQuery('sked__Job_Allocations__r').filter('sked__Status__c !=: ja_complete_status')
                                                        .filter('sked__Status__c !=: ja_declined_status')
                                                        .filter('sked__Status__c !=: ja_delete_status');

        List<sked__Job__c> skedJobs = Database.query(selector.getQuery());

        if (skedJobs == null || skedJobs.isEmpty()) {
            throw new skedException(skedConstants.INVALID_JOB_ID);
        }

        sked__Job__c skedJob = skedJobs.get(0);
        String timezone = skedJob.sked__Timezone__c;

        if (skedJob.sked__Job_Status__c == skedConstants.JOB_STATUS_COMPLETE) {
            throw new skedException(skedConstants.ERROR_RESCHEDULE_COMPLETE_JOB);
        }

        if (String.isBlank(skedJob.sked_Customer_Email__c)) {
            throw new skedException(skedConstants.PROSPECT_MISSING_EMAIL);
        }

        Date selectedDate = Date.valueOf(data.selectedDate);
        DateTime startOfSelectedDay = skedDateTimeUtils.getStartOfDate(selectedDate, timezone);
        Integer startMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(data.startTime);
        Integer endMinutes = skedDateTimeUtils.convertTimeNumberToMinutes(data.endTime);
        DateTime newJobStart = skedDateTimeUtils.addMinutes(startOfSelectedDay, startMinutes, timezone);
        DateTime newJobEnd = skedDateTimeUtils.addMinutes(startOfSelectedDay, endMinutes, timezone);

        skedJob.sked__Start__c = newJobStart;
        skedJob.sked__Finish__c = newJobEnd;
        skedJob.sked__Duration__c = skedDateTimeUtils.getDifferenteMinutes(newJobStart, newJobEnd);
        skedJob.sked_Job_Start_Minutes__c = startMinutes;
        skedJob.sked_Job_End_Minutes__c = endMinutes;

        List<sObject> updateJobandJas = new List<sObject>{skedJob};
        for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
            skedJA.sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_DELETED;
            updateJobandJas.add(skedJA);
        }

        update updateJobandJas;

        sked__Job_Allocation__c newSkedJA = new sked__Job_Allocation__c(
            sked__Job__c = skedJob.id,
            sked__Resource__c = data.resourceId,
            sked__Status__c = skedConstants.JOB_ALLOCATION_STATUS_PENDING_DISPATCH
        );

        insert newSkedJA;
    }

    private static skedVivintModel.VivintJA generateVivintJaFromJa (sked__Job_Allocation__c skedJA, String timezone) {
        skedVivintModel.VivintJA jaModel = new skedVivintModel.VivintJA(skedJA, timezone);
        jaModel.jobFinish = skedJA.sked__Job__r.sked__Finish__c;
        jaModel.timezone = skedJA.sked__Job__r.sked__Timezone__c;
        jaModel.geometry = new skedModels.geometry(skedJA.sked__Job__r.sked__GeoLocation__latitude__s,
                                                    skedJA.sked__Job__r.sked__GeoLocation__longitude__s);
        jaModel.jobType = skedJA.sked__Job__r.sked__Type__c;

        jaModel.jobContact = new skedVivintModel.VivintContact(skedJA.sked__Job__r);
        jaModel.jobContact.setInfo = skedJA.sked__Job__r.Job_Created_Desc__c;
        jaModel.jobContact.demoResult = skedJA.sked__Job__r.sked_Demo_Result__c;
        jaModel.reason = skedJA.sked__Job__r.sked__Abort_Reason__c;

        return jaModel;
    }

    private static skedVivintModel.VivintJA filterJaOfJob(sked__Job__c skedJob) {
        sked__Job_Allocation__c tempJA;
        if (!skedJob.sked__Job_Allocations__r.isEmpty() && skedJob.sked__Job_Status__c != skedConstants.JOB_STATUS_PENDING_ALLOCATION) {
            for (sked__Job_Allocation__c skedJA : skedJob.sked__Job_Allocations__r) {
                tempJA = skedJA;
                if (tempJA.sked__Status__c != skedConstants.JOB_ALLOCATION_STATUS_DELETED) {
                    break;
                }
            }
        }
        skedVivintModel.VivintJA jaModel = new skedVivintModel.VivintJA(skedJob, tempJA);

        return jaModel;
    }

    private static List<skedVivintModel.AreaManagerRescheduleData> getUnallocatedJobInRegionForScheduleTab(skedVivintModel.AreaManagementFilter filter) {
        skedVivintModel.AreaManagerRescheduleData result = new skedVivintModel.AreaManagerRescheduleData();
        Map<id, skedVivintModel.VivintJA> map_jobId_ja = new Map<id, skedVivintModel.VivintJA>();

        String timezone = skedCommonService.getTimezoneFromUserId();
        checkAndGetRegionOfAreaManager(filter);
        List<skedVivintModel.VivintJA> jas = getJAsInRegion(filter);
        for (skedVivintModel.VivintJA ja : jas) {
            if ((ja.jaCount == null || ja.jaCount == 0) &&
                (ja.jobStatus == null || ja.jobStatus != skedConstants.JOB_STATUS_CANCELLED)) {
                map_jobId_ja.put(ja.jobId, ja);
            }
        }

        List<skedVivintModel.TimeSlot> basedLstTimeSlots = skedCommonService.getListTimeSlotInListOfDates(filter.dayList, filter.regions);

        for (String day : filter.dayList) {
            Date selectedDate = Date.valueOf(day);
            System.debug('selectedDate ' + selectedDate);
            DateTime startOfSelectedDay = skedDateTimeUtils.getStartOfDate(selectedDate, timezone);
            String weekDay = startOfSelectedDay.format(skedConstants.WEEKDAY, timezone);
            List<skedVivintModel.TimeSlot> lstTimeSlots = new List<skedVivintModel.TimeSlot>(basedLstTimeSlots);

            lstTimeSlots.sort();

            for (skedVivintModel.TimeSlot baseSlot : lstTimeSlots) {
                if (baseSlot.weekDay != null && weekDay.equalsIgnoreCase(baseSlot.weekDay)) {
                    if ((baseSlot.slotStartDate == null || baseSlot.slotStartDate <= selectedDate) &&
                        (baseSlot.slotEndDate == null || baseSlot.slotEndDate >= selectedDate)) {
                        skedVivintModel.TimeSlot slot = baseSlot.deepClone();
                        slot.dateString = day;

                        for (skedVivintModel.VivintJA ja : map_jobId_ja.values()) {
                            System.debug('ja.timezone ' + ja.timezone);
                            String jobDay = ja.jobStart.format(skedConstants.YYYY_MM_DD, ja.timezone);
                            if (slot.market.id == ja.regionId) {
                                if (jobDay.equalsIgnoreCase(slot.dateString) && ja.jobStartMinutes == slot.startMinutes &&
                                    ja.jobEndMinutes == slot.endMinutes) {
                                    system.debug('2 check job ' + ja.jobName);
                                    slot.jobAllocations.add(ja);
                                }
                            }

                        }

                        result.timeSlots.add(slot);
                    }
                }
            }
        }

        return new List<skedVivintModel.AreaManagerRescheduleData>{result};
    }
}
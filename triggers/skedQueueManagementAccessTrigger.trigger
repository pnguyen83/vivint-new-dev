trigger skedQueueManagementAccessTrigger on sked_Queue_Management_Access__c (before insert, before update) {
    if (trigger.isBefore) {
        if (trigger.isInsert) {
            skedQueueManageAccessHandler.onBeforeInsert(trigger.new);
        }
        else if (trigger.isUpdate) {
            skedQueueManageAccessHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
        }
    }
}
trigger skedJobTrigger on sked__Job__c (after update, before insert, before update) {
    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            skedJobHandler.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }

    if (trigger.isBefore) {
        if (trigger.isInsert) {
            skedJobHandler.onBeforeInsert(trigger.new);
        }
        if (trigger.isUpdate) {
            skedJobHandler.onBeforeUpdate(trigger.new);
        }
    }
}